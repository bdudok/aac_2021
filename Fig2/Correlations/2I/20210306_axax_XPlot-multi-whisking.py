from ImportFirst import *
from Scores import Scores
from scipy import stats
from Alignment import Mouse
from CommonFunc import restrict_selection, load_face
from multiprocessing import Queue, freeze_support
from AnalysisClasses.Xplot_scan import Xplot_Worker

path = 'X:/Barna/axax/'
savepath = path + '_figure_plots/'
output_path = '_Xplot_scan_whisking/'
eyepath = 'X:/Barna/axax/eyes/'
fine_full = False

offest_secs = [0]
window_secs = [1]
if fine_full:
    offest_secs = numpy.arange(-10, 10.1, 0.1)
    window_secs = numpy.arange(0.1, 10.1, 0.1)

# read session info
s_info_fn = '_included_sessions.csv'
with open(path + s_info_fn, 'r') as f:
    session_info = pandas.read_csv(f)
# filter for experiment: ripples
filtered_info = restrict_selection(session_info, 'Ripples', 'yes')
filtered_info = restrict_selection(filtered_info, 'tv', 'off')
filtered_info = restrict_selection(filtered_info, 'Channels', 'dual')

#get list of included sessions
ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pflist = incl_sessions['Prefix']

# cache all mouse objects
mlist = filtered_info['Animal'].unique()
mdir = 'Alignment'
mice = {}
for mouse_id in filtered_info['Animal'].unique():
    mice[mouse_id] = Mouse(path + mdir, 'Axax-' + str(mouse_id))
os.chdir(path)

suffix = '_avg_AXAX-PC.npy'
roi_tag = '1'  # cells for sorting - PV in this case
red_tag = '2'  # All cells inc PCs
window_w = int(10 * fps)
gap = 60
pf_calc_param = 'nnd'
pf_calc_bins = 25

os.chdir(path)
store_bins = {}

nworker = 0
ncores = 4

if __name__ == '__main__':
    freeze_support()
    request_queue = Queue()

    for prefix in pflist:
        have_stops = False
        have_PCs = False
        # prefix = session['SessionTag'].strip()
        # if os.path.exists(path + output_path + '/running/' + prefix + 'Xplot_sorted_bins.npy'):
        #     continue

        session_id = int(''.join([x for x in prefix if x.isdigit()]))
        mouse_id = session_info.loc[session_info['Tag'] == prefix]['Animal'].iloc[0]
        mouse = mice[mouse_id]
        # add axax cells
        a = Scores(prefix, tag=roi_tag, no_tdml=True, norip=True)
        axax_indices = [c for c, cid in mouse.pair_with_polygons(a).items()]
        # add red cells
        red = Scores(prefix, tag=red_tag, no_tdml=True, norip=True)  # ripples are read to test if their numbers are fine
        pairs = mouse.pair_with_polygons(red, contain_only=True)
        incl_axax = list(pairs.keys())
        qc = red.qc()
        redc = []
        for c in range(red.ca.cells):
            if c not in incl_axax:
                if qc[c]:
                    redc.append(c)

        print('computing traces')
        axax_tr = a.ca.rel[axax_indices].mean(axis=0)
        red_tr = numpy.nanmean(red.ca.rel[redc], axis=0)
        # speed_tr = numpy.copy(a.pos.smspd)
        face_tr = load_face(a, eyepath, prefix)[:len(axax_tr)]

        # inputs for the compute function
        window_list = [int(fps * x) for x in window_secs]
        offset_list = [int(fps * x) for x in offest_secs]
        cellgroup_arrays = (red_tr, axax_tr, face_tr)
        cellgroup_names = ('Other', 'Axax', 'whisking')
        exclude_settings = ('all', 'exclude_movement', 'only_movement', 'running', )
        bins = 25
        name_of_source = 'whisking'
        name_of_targets = ('Axax', 'Other')
        # save_example = {'window': [15], 'offset': [100]}
        save_example = {'window': window_list, 'offset': offset_list}

        args = (path, a, output_path, window_list, offset_list, cellgroup_names, cellgroup_arrays,
                name_of_source, name_of_targets, exclude_settings, save_example, bins)
        kwargs = {}

        if nworker < ncores:
            Xplot_Worker(request_queue).start()
            nworker += 1
        request_queue.put((args, kwargs))

