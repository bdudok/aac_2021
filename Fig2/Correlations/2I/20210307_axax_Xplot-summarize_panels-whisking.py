from ImportFirst import *
from scipy.ndimage import gaussian_filter
from scipy import stats
from sklearn import metrics
from CommonFunc import strip_ax

path = 'X:/Barna/axax/'
savepath = path + '_figure_plots/'
fig_path = path + '_figure_plots/'
output_path = '_Xplot_scan_whisking/'
ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pf_list = incl_sessions['Prefix']
df = pf_list

# these must match values in previous script
cellgroup_names = ('Other', 'Axax', 'whisking')
offest_secs = numpy.arange(-10, 10.1, 0.1)
window_secs = numpy.arange(0.1, 10.1, 0.1)
window_list = [int(fps * x) for x in window_secs]
offset_list = [int(fps * x) for x in offest_secs]
bins = 25
unity = numpy.linspace(0, 1, bins)
neg_unity = numpy.linspace(1, 0, bins)
max_error = metrics.mean_squared_error(numpy.ones(bins) * 0.5, unity)

plt.rcParams['font.size'] = 8
plt.rcParams['font.sans-serif'] = 'Arial'

source_cells = 'whisking'
target_cells = 'Other'
for ai, excl_setting in enumerate(('all', 'exclude_movement', 'only_movement', 'running', )):
    savepath = f'_Xplot_scan_whisking/{excl_setting}/'
    os.chdir(path)
    prefix_indices = {}
    sorted_bins = numpy.empty((len(df), len(window_list), len(offset_list), len(cellgroup_names), bins))
    sorted_bins[:] = numpy.nan

    # load sortings
    counter = 0
    for n_entry, session in enumerate(pf_list):  # df.iterrows():
        prefix = session  # ['SessionTag'].strip()
        prefix_indices[prefix] = n_entry
        # if session['DrugName'] == 'CNO':
        #     continue
        sorted_bins[counter] = numpy.load(
            path + savepath + prefix + f'Xplot_sorted_bins by {source_cells} norm False.npy')
        counter += 1
    sorted_bins = sorted_bins[:counter]
    # assert False

    # compute matrix by window and offset
    slope_matrix = numpy.empty((len(window_list), len(offset_list)))
    r_matrix = numpy.empty((len(window_list), len(offset_list)))
    p_matrix = numpy.empty((len(window_list), len(offset_list)))
    for wi in range(len(window_list)):
        for oi in range(len(offset_list)):
            sort_Y = numpy.nanmean(sorted_bins[:, wi, oi, cellgroup_names.index(source_cells), :], axis=0)
            target_Y = numpy.nanmean(sorted_bins[:, wi, oi, cellgroup_names.index(target_cells), :], axis=0)
            sort_wh = numpy.logical_not(numpy.isnan(sort_Y))
            target_wh = numpy.logical_not(numpy.isnan(target_Y))
            incl = numpy.logical_and(sort_wh, target_wh)
            statres = stats.pearsonr(sort_Y[incl], target_Y[incl])
            r_matrix[wi, oi] = statres[0]
            p_matrix[wi, oi] = statres[1]
            sort_null_error = metrics.mean_squared_error(sort_Y[sort_wh], unity[sort_wh])
            sort_worst_error = metrics.mean_squared_error(unity, numpy.ones(bins) * sort_Y[sort_wh].mean())
            sort_score = max(0, 1 - sort_null_error / sort_worst_error)
            # 2, score the target. worst: the line is flat
            # null: unity or neg unity
            is_pos = target_Y[-1] > target_Y[0]
            ctrl = (neg_unity, unity)[is_pos]
            target_null_error = metrics.mean_squared_error(target_Y[target_wh], ctrl[target_wh])
            target_worst_error = metrics.mean_squared_error(ctrl, numpy.ones(bins) * target_Y[target_wh].mean())
            target_score = max(0, 1 - target_null_error / target_worst_error) * (-1, 1)[is_pos]
            slope_matrix[wi, oi] = sort_score * target_score
    # find optimum after gaussian filtering R matrix
    gf = gaussian_filter(slope_matrix, 3, mode='nearest')

    fig, ax = plt.subplots(nrows=2, ncols=2, gridspec_kw={'width_ratios': [3, 1], 'height_ratios': [1, 3]},
                           sharex='col', sharey='row')
    strip_ax(ax[0, 1])
    ca = ax[1, 0]
    mappable = ca.imshow(slope_matrix.transpose(), vmin=-0.25, vmax=0.25, cmap='seismic', aspect='auto',
                         interpolation='lanczos')
    # ca.set_title('sorted by' + source_cells)
    x_val = numpy.arange(0, 11, 2)
    ca.set_xticks(x_val * 10)
    ca.set_xticklabels(x_val)
    ca.set_xlabel('Window width (s)')
    y_val = numpy.arange(-10, 11, 5)
    ca.set_yticks(y_val * 10 + 10 * 10)
    ca.set_yticklabels(y_val)
    ca.set_xlim(0, len(window_list) + 1)
    ca.set_ylim(0, len(offset_list) + 1)
    ca.set_ylabel('Offset (s)')
    ca.set_aspect(len(window_list) / len(offset_list))
    # plot max curve

    opt = numpy.unravel_index(numpy.nanargmax(numpy.abs(gf)), slope_matrix.shape)
    # ca.scatter(*opt, marker='x', color='black')
    ca.axhline(opt[1], color='black', linestyle=':')
    ca.axvline(opt[0], color='black', linestyle=':')
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)

    # plot cross sections
    ca = ax[0, 0]
    Y = gf[:, opt[1]]  # all windows with bestoffset
    cmap = matplotlib.cm.get_cmap('seismic')
    norm = matplotlib.colors.Normalize(vmin=-0.25, vmax=0.25)
    ca.scatter(numpy.arange(len(Y)), Y, c=cmap(norm(Y)), edgecolor='none', s=3)
    ca.set_aspect('auto')
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)

    ca = ax[1, 1]
    Y = gf[opt[0], :]
    ca.scatter(Y, numpy.arange(len(Y)), c=cmap(norm(Y)), edgecolor='none', s=3)
    ca.set_aspect('auto')
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)


    fig.set_size_inches(2, 2)
    plt.tight_layout()
    for ext in ('.png', '.svg'):
        fig.savefig(f'{savepath}_Xplot-{target_cells}-whisking-heatmap-0307-sides-{excl_setting}' + ext, dpi=600)
    print(excl_setting)
    print(opt)
    print(f'Max corr: res:{window_secs[opt[0]]:.1f}, offs:{offest_secs[opt[1]]:.1f}, score:{gf[opt[0], opt[1]]:.2f}')
    print(f'Pearson: r={r_matrix[opt[0], opt[1]]:.2f}, p={p_matrix[opt[0], opt[1]]:.4f}')
    # if excl_setting == 'exclude_movement':
    #     assert False
    # plot batman
    opt = [9, 100]
    # opt = (94, 101) #match corr
    fig2, ca = plt.subplots(nrows=1, ncols=1)
    # ca = ax[1, ai]
    for i, color in zip((source_cells, target_cells), ('black', 'orange')):
        i = cellgroup_names.index(i)
        color = get_color(color)
        data = sorted_bins[:, opt[0], opt[1], i]
        mid = numpy.nanmean(data, axis=0)
        n = numpy.count_nonzero(numpy.logical_not(numpy.isnan(data)), axis=0)
        sem = numpy.nanstd(data, axis=0) / numpy.sqrt(n)
        lower = mid - sem
        upper = mid + sem
        ca.plot(mid, color=color)
        ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color, label=cellgroup_names[i])
    ca.set_xlabel('Fraction of Time')
    ca.set_xticks([0, bins - 1])
    ca.set_xticklabels([0, 1])
    # ca.set_ylim([0.1, 0.9])
    ca.set_xlim([0, bins - 1])
    ca.set_aspect(bins / 0.8)
    ca.set_ylabel('Normalized DF/F')
    # ca.legend()
    # ca.set_title(f'Window {window_secs[opt[0]]:.1f} s, offset {offest_secs[opt[1]]:.1f} s')
    # strip_ax(ax[-1, -1])
    # fig.colorbar(mappable, cax=ax[0, -1], shrink=0.4, ticks=[-0.5, 0, 0.5])
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)
    fig2.set_size_inches(2, 2)
    plt.tight_layout()
    #
    for ext in ('.png', '.svg'):
        fig2.savefig(f'{savepath}_negcorr_{target_cells}-XPlot-whisking-0307-1scorr-{excl_setting}' + ext, dpi=600)
#
    # # compute session-wise slopes
    # opts = numpy.empty((2, counter))
    # for i in range(counter):
    #     slope_matrix = numpy.empty((len(window_list), len(offset_list)))
    #     for wi in range(len(window_list)):
    #         for oi in range(len(offset_list)):
    #             sort_Y = sorted_bins[i, wi, oi, cellgroup_names.index(source_cells), :]
    #             target_Y = sorted_bins[i, wi, oi, cellgroup_names.index(target_cells), :]
    #             incl = numpy.logical_and(~numpy.isnan(sort_Y), ~numpy.isnan(sort_Y))
    #             sort_null_error = metrics.mean_squared_error(sort_Y[incl], unity[incl],)
    #             sort_worst_error = metrics.mean_squared_error(unity[incl], numpy.ones(incl.sum()) * sort_Y[incl].mean())
    #             sort_score = max(0, 1 - sort_null_error / sort_worst_error)
    #             # 2, score the target. worst: the line is flat
    #             # null: unity or neg unity
    #             is_pos = target_Y[-1] > target_Y[0]
    #             ctrl = (neg_unity, unity)[is_pos]
    #             target_null_error = metrics.mean_squared_error(target_Y[incl], ctrl[incl])
    #             target_worst_error = metrics.mean_squared_error(ctrl[incl], numpy.ones(incl.sum()) * target_Y[incl].mean())
    #             target_score = max(0, 1 - target_null_error / target_worst_error) * (-1, 1)[is_pos]
    #             slope_matrix[wi, oi] = sort_score * target_score
    #     gf = gaussian_filter(slope_matrix, 3, mode='nearest')
    #     opt = numpy.unravel_index(numpy.argmax(numpy.abs(gf)), slope_matrix.shape)
    #     opts[0, i] = window_secs[opt[0]]
    #     opts[1, i] = offest_secs[opt[1]]
    #
    # print(
    #     f'n = {counter} sessions, window = {opts[0].mean()}+-{numpy.std(opts[0])}, offset = {opts[1].mean()}+-{numpy.std(opts[1])}')
