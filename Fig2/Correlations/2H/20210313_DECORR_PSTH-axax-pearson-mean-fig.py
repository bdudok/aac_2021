from ImportFirst import *
from Scores import Scores
from Alignment import Mouse
from PSTH import PSTH
from EventMasks import running, all_ripples_immobility, whisking_v3
from CommonFunc import restrict_selection, get_sex_axax

'''avg trace of identified cells'''

path = 'X:/Barna/axax/'  # location of output folder. actual ImagingSession assets are not copied
psth_tags = ('run', 'whisking', 'swr')
pull_functions = {'run': running, 'whisking': whisking_v3, 'swr': all_ripples_immobility}
savepath = path + '_figure_plots/'

# read session info file
s_info_fn = '_included_sessions.csv'
with open(path + s_info_fn, 'r') as f:
    session_info = pandas.read_csv(f)
# filter for experiment: ripples
filtered_info = restrict_selection(session_info, 'Ripples', 'yes')
filtered_info = restrict_selection(filtered_info, 'tv', 'off')
filtered_info = restrict_selection(filtered_info, 'Channels', 'dual')
# pflist = filtered_info['Tag'].unique()

#ripple info
ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pflist = incl_sessions['Prefix']


# cache all mouse objects
mlist = filtered_info['Animal'].unique()
mdir = 'Alignment'
mice = {}
for mouse_id in filtered_info['Animal'].unique():
    mice[mouse_id] = Mouse(path + mdir, 'Axax-' + str(mouse_id))
os.chdir(path)
fps = 15.6
window_w = int(10 * fps)
param_key = 'rel'

psth_fname = '20210309-decorr-3AAC'
e = PSTH(savepath, psth_fname)  # this need not to be unique for the 3 groups
grp_names = ('CellType', 'Prefix', 'CellID', 'Animal', 'Sex')

wdir = path
ch = 0

# put AACs in PSTH
if not e.locked:
    for prefix in pflist:
        session_id = int(''.join([x for x in prefix if x.isdigit()]))
        mouse_id = session_info.loc[session_info['Tag'] == prefix]['Animal'].iloc[0]
        sex = get_sex_axax(mouse_id)
        mouse = mice[mouse_id]
        # add axax cells
        roi_tag = '1'
        a = Scores(prefix, tag=roi_tag, no_tdml=True, norip=True)
        axax_indices = [c for c, cid in mouse.pair_with_polygons(a).items()]
        if len(axax_indices) < 3:
            continue
        for c, cid in mouse.pair_with_polygons(a).items():
            grouping = ('Axax', prefix, mouse.id + '-' + cid, mouse_id, sex)
            e.add_items(prefix, roi_tag, [c], grouping=grouping, group_names=grp_names, path=wdir)
    e.save_items()

e.eyepath = 'X:/Barna/axax/eyes/'
e.set_session_kwargs(ripple_tag='curated')
e.ready()


# plot PSTH
plt.rcParams['font.size'] = 8
plt.rcParams['font.sans-serif'] = 'Arial'
fig, ax = plt.subplots(nrows=1, ncols=3, sharex=True, sharey=False)#, gridspec_kw={'height_ratios': [1,1,2]})
seconds = numpy.arange(-10, 11, 5)
xtk = (seconds * fps + window_w)
# pull
trig_colors = {'run':'grey', 'whisking':'sunset6', 'swr':'swrgreen'}
trig_params = {'run':'speed', 'whisking':'face', 'swr':'rpw'}

for col, psth_tag in enumerate(psth_tags):
    for ca in ax:
        ca.axvline(window_w, color='black', alpha=0.8, linestyle=':', zorder=0)
        ca.axhline(0, color='black', alpha=0.8, linestyle=':', zorder=0)
        ca.spines['right'].set_visible(False)
        ca.spines['top'].set_visible(False)
    ca = ax[col]
    for pull_tag, color_name, zorder in zip(('2d_mean', ), ('orange', ), (2,)):
    # for pull_tag, color_name in zip(('2d_mean', ), ('CMYKgreen', )):
        color = get_color(color_name)
        e.set_pull_params(param_key='pcorr-'+pull_tag, window_w=window_w, uses_ripples=(psth_tag == 'swr'))
        e.set_pull_function_kwargs(mod_kw=pull_tag, suffix='_pearson_pairwise.npy')
        mf_kwargs = {}
        if psth_tag == 'whisking':
            mf_kwargs['min_l'] = 8
        e.set_triggers(pull_functions[psth_tag], psth_tag, **mf_kwargs)
        e.pull_traces()
        e.summary()
        mid, lower, upper = e.return_mean(group_criteria=None, group_by='CellID', avg_cells=False,
                                          mid_mode='mean', spread_mode='SEM', multiply=1)
        # if psth_tag == 'running':
        baseline_shift = 0
        # else:
        # baseline_shift = numpy.nanmean(mid[window_w - int(3*fps):window_w])
        ca.plot(mid-baseline_shift, color=color, label='', zorder=zorder)
        ca.fill_between(range(len(mid)), lower-baseline_shift, upper-baseline_shift, alpha=0.4, color=color,
                        zorder=zorder)
    ca.set_xlabel('Time (s)')
    # ca.set_ylabel(psth_tag)
    # #plot trig param
    # ca = ax[0, col]
    # e.set_pull_params(param_key=trig_params[psth_tag])
    # e.set_pull_function_kwargs(None)
    # e.pull_traces()
    # mid, lower, upper = e.return_mean(group_criteria=None, group_by='CellID', avg_cells=False,
    #                                   mid_mode='mean', spread_mode='SD', multiply=1)
    # color = get_color(trig_colors[psth_tag])
    # ca.plot(mid, color=color, )
    # ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color)

for ca in ax:
    ca.set_xticks(xtk)
    ca.set_xticklabels(seconds)
ax[2].set_ylim(*ax[1].get_ylim())


fig.set_size_inches(2.5, 1.8)
plt.tight_layout()
fn = psth_tag + '-cor-resp-0310-pearson-mean-compact'
fig.savefig(savepath + fn + '.png', dpi=300)
fig.savefig(savepath + fn + '.svg')

seconds = numpy.arange(-3, 4, 3)
xtk = (seconds * fps + window_w)
ca.set_xticks(xtk)
ca.set_xticklabels(seconds)
ca.set_xlim(-3.1*fps+window_w, 3.1*fps+window_w)
plt.tight_layout()
fn = fn + '-zoom'
fig.savefig(savepath + fn + '.png', dpi=300)
fig.savefig(savepath + fn + '.svg')
