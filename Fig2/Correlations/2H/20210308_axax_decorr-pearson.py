from ImportFirst import *
from Scores import Scores
from scipy import stats
from Alignment import Mouse
from CommonFunc import restrict_selection
from sklearn.metrics import mutual_info_score
# from multiprocessing import Queue, freeze_support


path = 'X:/Barna/axax/'
savepath = path + '_figure_plots/'
output_path = '_decorrelation/'

# window_secs = numpy.arange(0.5, 10.1, 0.5)
y_bins = 25

# read session info
s_info_fn = '_included_sessions.csv'
with open(path + s_info_fn, 'r') as f:
    session_info = pandas.read_csv(f)
# filter for experiment: ripples
filtered_info = restrict_selection(session_info, 'Ripples', 'yes')
filtered_info = restrict_selection(filtered_info, 'tv', 'off')
filtered_info = restrict_selection(filtered_info, 'Channels', 'dual')

# get list of included sessions
ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pflist = incl_sessions['Prefix']

# cache all mouse objects
mlist = filtered_info['Animal'].unique()
mdir = 'Alignment'
mice = {}
for mouse_id in filtered_info['Animal'].unique():
    mice[mouse_id] = Mouse(path + mdir, 'Axax-' + str(mouse_id))
os.chdir(path)

roi_tag = '1'
suffix = '_pearson_pairwise.npy'

os.chdir(path)

def discretize_trace(y, bins=25):
    y = numpy.copy(y)
    y -= numpy.nanpercentile(y, 1)
    y = numpy.maximum(0, y)
    y /= numpy.nanpercentile(y, 99) / bins
    y = numpy.minimum(bins-1, y)
    return y.astype(numpy.uint8)

rng = numpy.random.default_rng()
for prefix in pflist:
    session_id = int(''.join([x for x in prefix if x.isdigit()]))
    mouse_id = session_info.loc[session_info['Tag'] == prefix]['Animal'].iloc[0]
    mouse = mice[mouse_id]
    # add axax cells
    a = Scores(prefix, tag=roi_tag, no_tdml=True, norip=True)
    axax_indices = [c for c, cid in mouse.pair_with_polygons(a).items()]
    if len(axax_indices) < 3:
        continue
    # axax_tr = a.ca.rel[axax_indices].mean(axis=0)

    '''in a rolling window, compute MI of single AAC trace and average aac trace
    control: samples are picked from random AACs - quantify "excess information" in each individual AAC'''

    # re-compute all AACs and p[opulation trace into bins for MI
    # pop_y = discretize_trace(axax_tr)
    # cell_y = discretize_trace(a.ca.rel[axax_indices])

    w = 1  # sec
    emia_fn = path + output_path + prefix + str(w) + suffix
    if os.path.exists(emia_fn):
        emi = numpy.load(emia_fn)
    else:
        print('Computing MI array:', prefix)
        incl_range = 100, a.ca.frames - 100
        # incl_range = 6000, 7500
        mi_array = numpy.empty((len(axax_indices), len(axax_indices), a.ca.frames))
        mi_array[:] = numpy.nan
        for t in range(*incl_range):
            # if not t%100:
            #     print(f'{int(100*t/a.ca.frames)}%')
            t0 = max(0, int(t - w * fps))
            t1 = min(a.ca.frames, int(t + w * fps))
            t_slice = slice(t0, t1)
            for i in range(len(axax_indices)):
                for j in range(i, len(axax_indices)):
                    if i == j:
                        continue
                    y1 = a.ca.rel[axax_indices[i], t_slice]
                    y2 = a.ca.rel[axax_indices[j], t_slice]
                    mi_array[i, j, t] = stats.pearsonr(y1, y2)[0]
        numpy.save(emia_fn, mi_array)
    # assert False


