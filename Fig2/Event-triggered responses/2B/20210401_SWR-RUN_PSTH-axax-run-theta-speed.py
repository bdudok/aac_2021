from ImportFirst import *
from Scores import Scores
from Alignment import Mouse
from PSTH import PSTH
from EventMasks import all_ripples_immobility, running, StartResponse, whisking
from CommonFunc import restrict_selection, get_sex_axax

'''avg trace of identified cells'''

path = 'X:/Barna/axax/'  # location of output folder. actual ImagingSession assets are not copied
psth_tag = ('run', 'swr', 'run-clean')[2]
pull_functions = {'run': StartResponse, 'swr': all_ripples_immobility, 'run-clean': running}
savepath = path + '_figure_plots/'

# read session info file
s_info_fn = '_included_sessions.csv'
with open(path + s_info_fn, 'r') as f:
    session_info = pandas.read_csv(f)
# filter for experiment: ripples
filtered_info = restrict_selection(session_info, 'Ripples', 'yes')
filtered_info = restrict_selection(filtered_info, 'tv', 'off')
filtered_info = restrict_selection(filtered_info, 'Channels', 'dual')
# pflist = filtered_info['Tag'].unique()

#ripple info
ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pflist = incl_sessions['Prefix']

# cache all mouse objects
mlist = filtered_info['Animal'].unique()
mdir = 'Alignment'
mice = {}
for mouse_id in filtered_info['Animal'].unique():
    mice[mouse_id] = Mouse(path + mdir, 'Axax-' + str(mouse_id))
os.chdir(path)
fps = 15.6
window_w = int(10 * fps)
param_key = 'rel'

psth_fname = 're20210224-curated'
e = PSTH(savepath, psth_fname)  # this need not to be unique for the 3 groups
grp_names = ('CellType', 'Prefix', 'CellID', 'Animal', 'Sex')

wdir = path
ch = 0
# put cells in PSTH
if not e.locked:
    for prefix in pflist:
        session_id = int(''.join([x for x in prefix if x.isdigit()]))
        mouse_id = session_info.loc[session_info['Tag'] == prefix]['Animal'].iloc[0]
        sex = get_sex_axax(mouse_id)
        mouse = mice[mouse_id]
        # add axax cells
        roi_tag = '1'
        a = Scores(prefix, tag=roi_tag, no_tdml=True, norip=True)
        for c, cid in mouse.pair_with_polygons(a).items():
            grouping = ('Axax', prefix, mouse.id + '-' + cid, mouse_id, sex)
            e.add_items(prefix, roi_tag, [c], grouping=grouping, group_names=grp_names, path=wdir)
        # add red cells
        roi_tag = '2'
        a = Scores(prefix, tag='2', no_tdml=True, norip=True)  # ripples are read to test if their numbers are fine
        # with open(savepath + '_SWR-count.txt', 'a') as f:
        #     f.write(f'{prefix}\t{len(a.ripples.events)}\n')
        # exclude red cells that overlap with tagged axax cells
        pairs = mouse.pair_with_polygons(a, contain_only=True)
        incl_axax = list(pairs.keys())
        nnd = numpy.nan_to_num(a.ca.nnd) > 0
        ppm = numpy.nan_to_num(a.ca.peaks) > 0
        for c in range(a.ca.cells):
            if c not in incl_axax:
                snr = numpy.nanpercentile(a.ca.ntr[c], 99)
                qc_pass = numpy.any(nnd[c]) and numpy.any(ppm[c]) and snr > 1
                if qc_pass:
                    grouping = ('Red', prefix, prefix + '-' + str(c), mouse_id, sex)
                    e.add_items(prefix, roi_tag, [c], grouping=grouping, group_names=grp_names, path=wdir)
    e.save_items()

e.ready()
e.set_pull_params(param_key=param_key, window_w=window_w, uses_ripples=(psth_tag == 'swr'))

# plot PSTH
plt.rcParams['font.size'] = 8
plt.rcParams['font.sans-serif'] = 'Arial'
fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=False, gridspec_kw={'height_ratios': [1,1,2]})
seconds = numpy.arange(-10, 11, 5)
xtk = (seconds * fps + window_w)
# pull
psth = e
psth.set_triggers(pull_functions[psth_tag], psth_tag)
psth.pull_traces()
psth.summary()

for ca in ax:
    ca.axvline(window_w, color='black', alpha=0.8, linestyle=':')
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)
# 2) plot cells
ca = ax[2]
ca.axhline(0, color='black', alpha=0.8, linestyle=':')
for ctype, color_name in zip(('Axax', 'Red'), ('orange', 'mountains1')):
    color = get_color(color_name)
    group_criteria = (('CellType', ctype),)
    mid, lower, upper = e.return_mean(group_criteria=group_criteria, group_by='CellID', avg_cells=True,
                                      mid_mode='mean', spread_mode='SEM', multiply=100)
    ca.plot(mid, color=color, label='')
    ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color)

ca.set_xticks(xtk)
ca.set_xticklabels(seconds)
ca.set_xlabel('Time (s)')
ca.set_ylabel('DF/F (%)')

# '''save excel output for stats and plots. important: don't change pull param before saving this!'''
# resp_before = {'run-clean': 10, 'swr': 3, 'stop': 10}
# resp_after = {'run-clean': 5, 'swr': 0.1, 'stop': 5}
# ctypes = ('Axax', 'Red')
# cell_no = 0
# op = pandas.DataFrame()
# before_slice = slice(*[int(window_w + x * fps) for x in (-resp_before[psth_tag], 0)])
# after_slice = slice(*[int(window_w + x * fps) for x in (0, resp_after[psth_tag])])
# for _, item in e.group_df.iterrows():
#     ctype = item['CellType']
#     cid = item['CellID']
#     group_criteria = (('CellType', ctype), ('CellID', cid),)
#     data = e.get_data(group_criteria=group_criteria, avg_cells=True)
#     mid = numpy.nanmean(data, axis=0) * 100
#     before = numpy.nanmean(mid[before_slice])
#     after = numpy.nanmean(mid[after_slice])
#     op = op.append(
#         pandas.DataFrame({'CellType': ctype, 'CellID': cid, 'Animal': f'BD{item["Animal"]}',
#                           'Sex': item["Sex"], 'Event': psth_tag,
#                           'Before[DF/F]': before, 'After[DF/F]': after, 'Response': after - before},
#                          index=[cell_no]))
#     cell_no += 1
#
# op.to_excel(savepath + 'Axax-RunResp-0224-curated.xlsx')


# plot speed
psth.set_pull_params(param_key='speed', uses_ripples=False)
psth.pull_traces()
mid, lower, upper = e.return_mean(group_criteria=None, group_by='CellID', avg_cells=False,
                                  mid_mode='mean', spread_mode='SD', multiply=1)
ca = ax[0]
color = get_color('black')
ca.plot(mid, color=color, )
ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color)
ca.set_ylabel('Speed (cm/s)')
# ca.set_xlim(-5.1*fps+window_w, 5.1*fps+window_w)

#plot theta
psth.set_pull_params(param_key='tpw', uses_ripples=True)
psth.pull_traces()
mid, lower, upper = e.return_mean(group_criteria=None, group_by='CellID', avg_cells=False,
                                  mid_mode='mean', spread_mode='SD', multiply=100)
ca = ax[1]
color = get_color('thetablue')
ca.plot(mid, color=color, )
ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color)
ca.set_ylabel('Tpw (AU)')

fig.set_size_inches(1.5, 2.2)
plt.tight_layout()
fn = psth_tag + '-resp-by-type-0404-theta-6-9'
fig.savefig(savepath + fn + '.png', dpi=300)
fig.savefig(savepath + fn + '.svg')
