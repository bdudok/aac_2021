from ImportFirst import *
from Scores import Scores
from Alignment import Mouse
from PSTH import PSTH
from EventMasks import all_ripples_immobility, running, StartResponse, whisking
from CommonFunc import restrict_selection, get_sex_axax, ewma
from scipy import stats

'''load supersampled response array, look up layer from Alignments'''

path = 'X:/Barna/axax/'  # location of output folder. actual ImagingSession assets are not copied
savepath = path + '_figure_plots/'
array_path = path + '_supersample_arrays/'

# read session info file
s_info_fn = '_included_sessions.csv'
with open(path + s_info_fn, 'r') as f:
    session_info = pandas.read_csv(f)
# filter for experiment: ripples
filtered_info = restrict_selection(session_info, 'Ripples', 'yes')
filtered_info = restrict_selection(filtered_info, 'tv', 'off')
filtered_info = restrict_selection(filtered_info, 'Channels', 'dual')
# pflist = filtered_info['Tag'].unique()

# ripple info
ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pflist = incl_sessions['Prefix']

# cache all mouse objects
mlist = filtered_info['Animal'].unique()
mdir = 'Alignment'
mice = {}
for mouse_id in filtered_info['Animal'].unique():
    mice[mouse_id] = Mouse(path + mdir, 'Axax-' + str(mouse_id))
os.chdir(path)


edges = numpy.linspace(-1, 1, 101)
eval_col = 1  # 1:df/f, 2:diff

op = pandas.DataFrame()
n_c = 0
for prefix in pflist:
    session_id = int(''.join([x for x in prefix if x.isdigit()]))
    mouse_id = session_info.loc[session_info['Tag'] == prefix]['Animal'].iloc[0]
    sex = get_sex_axax(mouse_id)
    mouse = mice[mouse_id]
    # add axax cells
    roi_tag = '1'
    a = Scores(prefix, tag=roi_tag, no_tdml=True, norip=True)

    for c, cid in mouse.pair_with_polygons(a).items():
        cell_lookup = mouse.id + '-' + cid
        layer = mouse.get_layer_values(cid)
        X = numpy.load(array_path + prefix + f'_c{c}_supersample_2.npy')
        C = numpy.load(array_path + prefix + f'_c{c}_supersample_2_control.npy')
        # count number of responses above 95 percentile of control
        obs = []
        exp = []
        for Y, resp in zip((X, C), (obs, exp)):
            for event in range(Y.shape[1]):
                incl = numpy.where(numpy.logical_and(Y[0, event] <= 0.1, Y[0, event] > -0.05))[0]
                baseline = numpy.where(numpy.logical_and(Y[0, event] < -0.1, Y[0, event] > -0.95))[0]
                if len(incl) > 0:
                    resp.append(numpy.max(Y[eval_col, event, incl]) - Y[eval_col, event, baseline].mean())
        # binstat = stats.binned_statistic(Y[0].flatten(), Y[eval_col].flatten(), 'mean', bins=edges)
        rrat = numpy.mean(numpy.logical_and(numpy.array(obs) > 0, numpy.array(obs) > numpy.percentile(exp, 95)))
        op = op.append(
            pandas.DataFrame({'CID': cell_lookup, 'layer': layer, 'Resp': numpy.mean(obs), 'Control': numpy.mean(exp),
                              'SigRespR': rrat, 'Active': rrat > .05, 'Prefix': prefix}, index=[n_c]))
        n_c += 1

op.to_excel(savepath + 'Axax-RippleResp-supersample-0722-100msmax-resprat-layers+-100_DFF.xlsx')
