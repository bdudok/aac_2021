from ImportFirst import *
from ImagingSession import ImagingSession
from EventMasks import all_ripples_immobility
from EyeTracking import clean_whisker_map

path = 'X:/Barna/axax/'
savepath = path + '_figure_plots/'
array_path = path + '_supersample_arrays/'
prefix = 'axax_124_342'
roi_tag = '1'
os.chdir(path)

window_w = 16

ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pflist = incl_sessions['Prefix']

for prefix in pflist:
    a = ImagingSession(prefix, tag=roi_tag, no_tdml=True, ch=0, ripple_tag='curated')
    events, masks = all_ripples_immobility(a, w=window_w)

    # event_delays
    event_masks = []  # the frames to include, as well as the time of each sample after ripple onset (ms)
    event_delays = []
    timestamps = a.ripples.frame_array
    #get control timestamps



    for e in a.ripples.events:
        sample = e.p[0]
        frame = a.sampletoframe(sample)
        if frame in events:
            start = numpy.searchsorted(timestamps, frame)
            n = numpy.searchsorted(timestamps[start:], frame + 1)
            delay = ((sample - start) / n) / fps
            event_masks.append(numpy.arange(frame - window_w, frame + window_w))
            event_delays.append(delay)

    rel = a.ca.rel
    diff = a.getparam('diff')

    # pull times
    line_time = 1 / 8000
    msize = window_w * 2
    for c in range(a.ca.cells):
        cm = a.rois.polys.data[c].mean(axis=0)
        c_d = cm[1] * line_time
        Y = numpy.empty((3, len(event_masks),  msize))  # time, intensity, differential
        n = 0
        for mask, e_d in zip(event_masks, event_delays):
            Y[0, n] = numpy.arange(- window_w, window_w) / fps + (c_d - e_d)
            Y[1, n] = rel[c, mask]
            Y[2, n] = diff[c, mask]
            n += 1
        numpy.save(array_path+prefix+f'_c{c}_supersample_2.npy', Y)

