from ImportFirst import *
from Scores import Scores
from Alignment import Mouse
from PSTH import PSTH
from EventMasks import all_ripples_immobility, running, StartResponse, whisking, whisking_v2, whisking_v3
from CommonFunc import restrict_selection, get_sex_axax, load_face

'''avg trace of identified cells. this version aligns on avg of -1 sec and focuses on +- 3 sec'''

path = 'X:/Barna/axax/'  # location of output folder. actual ImagingSession assets are not copied
# psth_tag = 'whiskingv1'
# pull_functions = {'whiskingv1': whisking}
psth_tag = 'whiskingv3'
pull_functions = {'whiskingv3': whisking_v3}
savepath = path + '_figure_plots/'

# read session info file
s_info_fn = '_included_sessions.csv'
with open(path + s_info_fn, 'r') as f:
    session_info = pandas.read_csv(f)
# filter for experiment: ripples
filtered_info = restrict_selection(session_info, 'Ripples', 'yes')
filtered_info = restrict_selection(filtered_info, 'tv', 'off')
filtered_info = restrict_selection(filtered_info, 'Channels', 'dual')
# pflist = filtered_info['Tag'].unique()

#ripple info
ripple_info = pandas.read_excel(savepath + 'manual ripple selection status.xlsx')
incl_sessions = ripple_info.loc[ripple_info['status'] != 'exclude']
pflist = incl_sessions['Prefix']

# #test if we have face for all:
# os.chdir(path)
# for prefix in pflist:
#     a = Scores(prefix, tag='1', no_tdml=True, norip=True)
#     # try:
#     line = load_face(a, path + 'eyes/', prefix)
#     # except:
#     #     print('Face not loaded for', prefix)
#     #     continue
#     plt.figure()
#     plt.plot(line)
#     plt.suptitle(prefix)
#     plt.savefig(path + 'eyes/' + prefix + '_facepreview.png')
#     plt.close()
#     print(prefix, 'OK')
# assert False


# cache all mouse objects
mlist = filtered_info['Animal'].unique()
mdir = 'Alignment'
mice = {}
for mouse_id in filtered_info['Animal'].unique():
    mice[mouse_id] = Mouse(path + mdir, 'Axax-' + str(mouse_id))
os.chdir(path)
fps = 15.6
window_w = int(10 * fps)
param_key = 'rel'

psth_fname = 're20210227-whisking_v1'
e = PSTH(savepath, psth_fname)  # this need not to be unique for the 3 groups
grp_names = ('CellType', 'Prefix', 'CellID', 'Animal', 'Sex')
e.eyepath = 'X:/Barna/axax/eyes/'

wdir = path
ch = 0

e.ready()
e.set_pull_params(param_key=param_key, window_w=window_w, uses_ripples=False)
e.set_session_kwargs(ripple_tag='curated')

# plot PSTH
plt.rcParams['font.size'] = 8
plt.rcParams['font.sans-serif'] = 'Arial'
fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=False, gridspec_kw={'height_ratios': [1, 1, 1]})
seconds = numpy.arange(-10, 11, 1)
xtk = (seconds * fps + window_w)
# pull
psth = e
psth.set_triggers(pull_functions[psth_tag], psth_tag)
# psth.pull_traces()
print('psth setup complete')
for ca in ax:
    ca.axvline(window_w, color='black', alpha=0.8, linestyle=':')
    # ca.axhline(0, color='black', alpha=0.8, linestyle=':')
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)

ca = ax[2]
# for ctype, color_name in zip(('Axax', 'Red'), ('orange', 'mountains1')):
#     color = get_color(color_name)
#     group_criteria = (('CellType', ctype),)
#     mid, lower, upper = e.return_mean(group_criteria=group_criteria, group_by='CellID', avg_cells=True,
#                                       mid_mode='mean', spread_mode='SEM', multiply=100)
#     baseline_shift = numpy.nanmean(mid[window_w-int(fps):window_w])
#     ca.plot(mid-baseline_shift, color=color, label='')
#     ca.fill_between(range(len(mid)), lower-baseline_shift, upper-baseline_shift, alpha=0.4, color=color)

ca.set_xticks(xtk)
ca.set_xticklabels(seconds)
# ca.set_ylim(-1, 7)
ca.set_xlabel('Time (s)')
# ca.set_ylabel('DF/F (%)')


# plot theta
psth.set_pull_params(param_key='tpw', uses_ripples=True)
print('calling pull theta')
psth.pull_traces()
print('Theta complete')
mid, lower, upper = e.return_mean(group_criteria=None, group_by='CellID', avg_cells=False,
                                  mid_mode='mean', spread_mode='SD', multiply=100)
ca = ax[1]
color = get_color('thetablue')
ca.plot(mid, color=color, )
ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color)
ca.set_ylabel('Tpw (AU)')

# plot gamma
psth.set_pull_params(param_key='gpw', gamma_band='b')
psth.pull_traces()
mid, lower, upper = e.return_mean(group_criteria=None, group_by='CellID', avg_cells=False,
                                  mid_mode='mean', spread_mode='SD', multiply=100)
ca = ax[2]
color = get_color('thetablue')
ca.plot(mid, color=color, )
ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color)
# ca.set_ylim(0, 500)
ca.set_ylabel('Gpw (AU)')

# plot 4hz
psth.set_pull_params(param_key='gpw-slo', gamma_band='slo')
psth.pull_traces()
mid, lower, upper = e.return_mean(group_criteria=None, group_by='CellID', avg_cells=False,
                                  mid_mode='mean', spread_mode='SD', multiply=100)
ca = ax[0]
color = get_color('thetablue')
ca.plot(mid, color=color, )
ca.fill_between(range(len(mid)), lower, upper, alpha=0.4, color=color)
# ca.set_ylim(0, 500)
ca.set_ylabel('4Hz (AU)')

ca.set_xlim(-3.1*fps+window_w, 3.1*fps+window_w)

fig.set_size_inches(1.5, 2.2)
plt.tight_layout()
fn = psth_tag + '-resp-by-type-0404-re-v3-thetagamma-4hz'
fig.savefig(savepath + fn + '.png', dpi=300)
fig.savefig(savepath + fn + '.svg')
