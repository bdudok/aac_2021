from LoadPolys import LoadImage
from Quad import SplitQuad
import numpy
import os
import cv2


def exportstop(path, prefix, mode='stop', channel='Green', stimlen=15):
    if path is not None:
        os.chdir(path)
    try:
        pos = SplitQuad(prefix)
        image = LoadImage(prefix, explicit_need_data=True)
    except:
        return -1
    mov = pos.gapless
    span = 100, len(pos.pos) - 100
    duration = 50
    gap = 150
    # collect stops
    starts, stops = pos.startstop(duration=duration, gap=gap, span=span)
    im = image.data
    stoprun_image = numpy.zeros(image.info['sz'])
    frames = image.nframes
    if channel == 'Green':
        ch = 0
    elif channel == 'Red':
        ch = 1
        if ch not in image.channels:
            print('Channel not found:', channel)
            return -1
    else:
        print('Channel unexpected:', channel)
        return -1
    if mode == 'stop':
        suf = '_StopActivity.tif'
        for start, stop in zip(starts, stops):
            stoprun_image[:, :] += im[stop:min(frames, stop + 100), :, :, ch].mean(axis=0) \
                                   - im[start:stop, :, :, ch].mean(axis=0)
    elif mode == 'opto':
        suf = '_OptoActivity.tif'
        opto = numpy.load(prefix + '_opto.npy')
        starts = numpy.where(numpy.diff(opto.astype('byte')) > 0)[0]
        for start in starts:
            stoprun_image[:, :] += im[start - 100:start, :, :, ch].mean(axis=0) - im[start:start + stimlen, :, :,
                                                                                  ch].mean(axis=0)
    elif mode == 'run':
        suf = '_RunActivity.tif'
        for i in range(len(starts)):
            start = starts[i]
            stop = stops[i]
            if i == 0:
                t0 = 100
            else:
                t0 = stops[i - 1]
            l = min(start - t0, stop - start)
            stoprun_image[:, :] += im[start - l:start, :, :, ch].mean(axis=0) - im[start:stop, :, :, ch].mean(axis=0)
    # positive values as red, negs as green
    stoprun_rgb = numpy.zeros((*stoprun_image.shape, 3), dtype='uint8')
    neg = numpy.zeros(stoprun_image.shape)
    nw = numpy.where(stoprun_image < 0)
    neg[nw] -= stoprun_image[nw]
    pos = numpy.zeros(stoprun_image.shape)
    pw = numpy.where(stoprun_image > 0)
    pos[pw] += stoprun_image[pw]
    r = max(numpy.percentile(neg, 99), numpy.percentile(pos, 99))
    stoprun_rgb[:, :, 2] = numpy.minimum(neg / r, 1) * 255
    stoprun_rgb[:, :, 1] = numpy.minimum(pos / r, 1) * 255
    pfolder = './/'
    if not os.path.exists(pfolder):
        os.mkdir(pfolder)
    cv2.imwrite(pfolder + prefix + suf, stoprun_rgb)
    print('Stop score export finished for', prefix)


if __name__ == '__main__':
    os.chdir('Z://cckdlx//')
    with open('_prefix-list-bd80.txt', 'r') as f:
        lines = f.readlines()
    for l in lines:
        prefix = l.strip()
        exportstop(None, prefix)
