from datetime import datetime
from skimage.feature import register_translation
from skimage import transform
from multiprocessing import Process  # , Queue

import numpy
import os
import scipy
import shutil
import sima
import time

from LoadPolys import LoadImage


# class RigidTransform:
#     def __init__(self, prefix, granularity):
#         self.scratch = 'C://MotionCorrect//'
#         self.prefix = prefix
#         w = (int(660 / granularity) + 1) * granularity  # trim blank edges
#         trim = int((796 - w) / 2)
#         im = LoadImage(prefix)
#         self.data = im.data[:, :, trim:-trim, 0]
#         self.result = numpy.empty(self.data.shape)
#         itn = 100
#         pic = numpy.zeros(d.shape[1:3])
#         for i in numpy.random.choice(self.data.shape[0], itn):
#             pic += self.data[i, :, :, 0]
#         pic /= itn
#         self.target = pic
#
#     def pack_data(self, i):
#         return i, self.data[i]
#
#     def unpack_data(self, data):
#         i, d = data
#         self.result[i] = data
#
#
# class RigidWorker(Process):
#     def __init__(self, queue, res_queue, target):
#         super().__init__()
#         self.queue = queue
#         self.res_queue = res_queue
#         self.target = target
#
#     def run(self):
#         for d in iter(self.queue.get, None):
#             r = register_translation(d, self.target)
#             return transform.warp(d, transform.EuclideanTransform(translation=r[0]))


class CleanupWorker(Process):
    def __init__(self, cleanup_queue):
        super().__init__()
        self.queue = cleanup_queue
        self.transforms = {}

    def run(self):
        for fn in iter(self.queue.get, None):
            fn += '//'
            # print('Cleanup called for', fn)
            if os.path.exists(fn):
                while True:
                    time.sleep(1)
                    try:
                        shutil.rmtree(fn)
                        break
                    except:
                        pass

class Worker(Process):
    def __init__(self, queue, cleanup_queue):
        super(Worker, self).__init__()
        self.queue = queue
        self.cleanup_queue = cleanup_queue

    def run(self):
        for din in iter(self.queue.get, None):
            # scratch folder:
            # scratch = 'C://MotionCorrect//'
            path, scratch, prefix, granularity, channels_used, rigid_steps, max_displacement, opto_mode, ignore_sat = din
            os.chdir(path)
            w = (int(660 / granularity) + 1) * granularity  # trim blank edges
            trim = int((796 - w) / 2)
            mc_approach = sima.motion.HiddenMarkov2D(granularity=('row', granularity), max_displacement=[20, 30],
                                                     verbose=False)
            fn = scratch + prefix + '.mcds.sima'
            if os.path.exists(fn):
                print(datetime.now(), 'Open mcds, continuing aborted job', prefix)
                mcds = sima.ImagingDataset.load(fn)
                # (1000, 1, 502, 543, 1) - last is channel
            else:
                im = LoadImage(prefix, explicit_need_data=True)
                print(datetime.now(), 'Open sbx, found', im.found)
                # (16453, 512, 796, 1)
                if channels_used == 'Red':
                    if im.data.shape[-1] > 1:
                        channels_used = [1]
                    else:
                        channels_used = [0]
                elif channels_used == 'Both':
                    channels_used = None
                elif type(channels_used) == list:
                    pass
                else:
                    channels_used = [0]
                #perform corrections as needed
                if im.found == 'nonrigid':
                    print('Nonrigid file found, skipping.', prefix)
                    return
                elif im.found == 'rigid':
                    print('Rigid file found, performing nonrigid step...')
                    sds = sima.Sequence.create('ndarray', numpy.expand_dims(im.data[:, :, trim:-trim, :], axis=1))
                else:
                    if rigid_steps > 0:
                        print(f'Rigid step: Refining reference...')
                        t0 = datetime.now()
                        ref_len = max(100, im.nframes // 50)
                        ch = channels_used[0]
                        if not opto_mode: #for now, separate code for no-crop displacement estimation scenarios
                            xtrim = int(min(100, im.data.shape[1] * 0.2))
                            ytrim = int(min(200, im.data.shape[2] * 0.2))
                            tforms = {}
                            #perform multiple steps only on ref frames to improve reference image, then all frames only once
                            rdat = numpy.copy(im.data[numpy.random.choice(im.nframes, ref_len),
                                              xtrim:-xtrim, ytrim:-ytrim, ch]).astype('float')
                            mdslice = slice(max_displacement, -max_displacement)
                            for k in range(rigid_steps):
                                reference = numpy.mean(rdat, axis=0)[mdslice, mdslice].astype('float')
                                reference = reference / reference.max()
                                displacements = numpy.zeros((ref_len, 2))
                                for i in range(ref_len):
                                    p = rdat[i, mdslice, mdslice]
                                    displacements[i] = register_translation(p/p.max(), reference,
                                                                            upsample_factor=2)[0]
                                displacements = numpy.minimum(displacements, max_displacement)
                                displacements = numpy.maximum(displacements, -max_displacement)
                                for i, r in enumerate(displacements):
                                    tr = r[0] * (max_displacement + 1) + r[1]
                                    if tr not in tforms:
                                        tforms[tr] = transform.SimilarityTransform(translation=r[::-1])
                                    rdat[i] = transform.warp(rdat[i], tforms[tr], order=1).astype('float')
                            print('Transforming image...')
                            tforms = {}
                            rigid = numpy.empty(im.data.shape, im.data.dtype)
                            reference = numpy.mean(rdat, axis=0)[mdslice, mdslice]
                            x_slice = slice(xtrim + max_displacement, - xtrim - max_displacement)
                            y_slice = slice(ytrim + max_displacement, - ytrim - max_displacement)
                            zp = numpy.empty(im.nframes)
                            zp_threshold = 1500
                            nz_frames = numpy.zeros(im.nframes, 'bool')
                            for i in range(im.nframes):
                                zp[i] = numpy.count_nonzero(im.data[i] == 0)
                                p = im.data[i, x_slice, y_slice, ch]
                                r = register_translation(p / p.max(), reference)[0]
                                r = numpy.minimum(max_displacement, numpy.maximum(-max_displacement, r))
                                tr = r[0] * (max_displacement + 1) + r[1]
                                if tr not in tforms:
                                    tforms[tr] = transform.SimilarityTransform(translation=r[::-1])
                                for c in range(im.data.shape[-1]):
                                    if zp[i] < zp_threshold or i == 0 or i == im.nframes - 1 or ignore_sat:
                                        rigid[i, ..., c] = transform.warp(im.data[i, ..., c], tforms[tr], order=1,
                                                                 preserve_range=True).astype(im.data.dtype)
                                    else:
                                        rigid[i, ..., c] = ((im.data[i - 1, ..., c].astype('float') +
                                                             im.data[i + 1, ..., c].astype('float'))
                                                            / 2).astype(im.data.dtype)
                                        nz_frames[i] = True
                            # nz_frames = zp > zp_threshold
                            if numpy.any(nz_frames):
                                print('!!! Frames with zero values detected, frames may have been dropped.'
                                      ' Bad frames were replaced:', list(numpy.where(nz_frames)[0]))
                        else: #opto mode
                            print('Motion correction rigid step running in opto mode...')
                            bbox = numpy.array((68, 723, max(opto_mode, 262), 496)).reshape((2, 2))
                            x_slice, y_slice = slice(bbox[0, 0], bbox[0, 1]), slice(bbox[1, 0], bbox[1, 1])
                            input_data = im.data
                            opto_frames = numpy.load(prefix + '_opto.npy')
                            incl_frames = numpy.where(numpy.logical_not(opto_frames))[0]
                            rdat = numpy.empty((ref_len, *numpy.diff(bbox, axis=1).flat[::-1]), dtype=im.data.dtype)
                            from pystackreg import StackReg
                            sr = StackReg(StackReg.TRANSLATION)
                            for k in range(rigid_steps):
                                print(f'Transforming image {k + 1}/{rigid_steps}...')
                                for i, f in enumerate(numpy.random.choice(incl_frames, ref_len, replace=False)):
                                    rdat[i] = input_data[f, y_slice, x_slice, ch]
                                reference = numpy.mean(rdat, axis=0)
                                rigid = numpy.empty(im.data.shape, im.data.dtype)
                                for i in range(im.nframes):
                                    # estimate displacement on cropped region with stackreg
                                    tform = sr.register(reference, input_data[i, y_slice, x_slice, ch])
                                    for c in range(im.data.shape[-1]):
                                        rigid[i, ..., c] = transform.warp(input_data[i, ..., c], tform, order=1,
                                                                          preserve_range=True).astype(im.data.dtype)
                        speed = (datetime.now() - t0).seconds / im.nframes
                        sds = sima.Sequence.create('ndarray', numpy.expand_dims(rigid[:, :, trim:-trim, :], axis=1))
                        print(f'Rigid steps completed in {speed} seconds / frame')
                    elif rigid_steps == 0:
                        sds = sima.Sequence.create('ndarray', numpy.expand_dims(im.data[:, :, trim:-trim, :], axis=1))
                if im.found != 'nonrigid':
                    print(datetime.now(), 'Call motion correct: HMM;', prefix)
                    mcds = mc_approach.correct([sds], fn, correction_channels=channels_used)
                    print(datetime.now(), 'Motion correct returned', prefix)
                # del sds

            ret = mcds.sequences[0]
            h, w, nch = ret.shape[2:]
            # find crop size
            x0 = int((796 - w) / 2)
            x1 = x0 + w
            y0 = int((512 - h) / 2)
            y1 = y0 + h
            print(datetime.now(), 'Writing output', prefix)
            fid = open(scratch + prefix + '_nonrigid.tmp', 'w')
            for frame in range(ret.shape[0]):
                curframe = numpy.array(ret[frame, 0, :, :, :]).astype('uint16')
                for column in range(512):
                    row = numpy.empty((796, nch), dtype='uint16')
                    if column < y0 or column >= y1:
                        row[:, :] = 65535
                    else:
                        row[:x0, :] = 65535
                        line = curframe[0, 0, column - y0, :, :]
                        # interpolate if empty line
                        if line[398, 0] == 0:
                            if line.max() == 0 and column > y0 and column < y1 - 1:
                                line = numpy.array([curframe[0, 0, column - y0 - 1, :, :],
                                                    curframe[0, 0, column - y0 + 1, :, :]]).mean(axis=0)
                        row[x0:x1, :] = line
                        row[x1:, :] = 65535
                    row.flatten().tofile(fid)
            fid.close()
            shutil.move(scratch + prefix + '_nonrigid.tmp', prefix + '_nonrigid.sbx')
            if not os.path.exists(prefix + '_nonrigid.mat'):
                shutil.copy(prefix + '.mat', prefix + '_nonrigid.mat')
            # clean up temporary files
            mcds = None
            im = None

            ##Create align file - removed, only needed for sbx pulling
            print(datetime.now(), prefix, 'Done.')
            for temp in [scratch + prefix + '.mcds.sima', path + prefix + '_rigid.sbx']:
                self.cleanup_queue.put(temp)


# if __name__ == '__main__':
#     os.chdir(path)
#     request_queue = Queue()
#     nworker=0
#
#     if files == None:
#         files = []
#         for f in os.listdir('.'):
#             if f.endswith('.sbx'):
#                 if 'rigid' in f:
#                     continue
#                 if os.path.exists(f.replace('.sbx','_nonrigid.sbx')):
#                     continue
#                 if os.path.getsize('ir_041_200.sbx') > sizelimit:
#                     files.append(f[:-4])
#     for t in files:
#         if nworker<ncpu:
#             Worker(request_queue).start()
#             nworker+=1
#         request_queue.put((t, mc_approach, trim))


'''
d.shape
(1, 796, 512, 16453)
d.dtype
dtype('uint16')
'''

# import cv2
# class play_eye:
#     def __init__(self, M, raw, scaling):
#         eye=M
#         step = 1
#         self.frame = 0
#         cv2.namedWindow('Movie')
#         cv2.namedWindow('Raw')
#         nframes = len(eye)
#         tblen = 512
#         self.factor = tblen / nframes
#         cv2.createTrackbar('Frame', 'Movie', 0, tblen, self.tbonChange)
#         while self.frame < nframes:
#             if cv2.getWindowProperty('Movie', 0) < 0:
#                 break
#             im = 255 - (eye[self.frame]*scaling / 256).transpose().astype('uint8').copy()
#             cv2.putText(im, str(self.frame), (0, 40),
#                         fontFace=cv2.FONT_HERSHEY_DUPLEX, fontScale=1, color=128)
#             cv2.imshow('Movie', im)
#             cv2.imshow('Raw', 255 - (raw[self.frame] * scaling / 256).transpose().astype('uint8'))
#             if cv2.waitKey(30) & 0xFF == ord('q'):
#                 break
#             self.frame += step
#             cv2.setTrackbarPos('Frame', 'Movie', int(self.frame * self.factor))
#         cv2.destroyAllWindows()
#
#     def tbonChange(self, v):
#         self.frame = int(v / self.factor)