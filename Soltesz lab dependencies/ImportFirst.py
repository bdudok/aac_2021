import matplotlib
matplotlib.use('TkAgg')
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import os
import numpy
import matplotlib.pyplot as plt
import pandas
from MyColorLib import get_color

fps = 15.6
speed_calib = 21.468