import ImportFirst
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button
import os
import numpy
from sbxreader import loadmat


class Opts:
    def __init__(self, fs):
        self.fs = fs
        self.coarse = 0
        self.fine = 0
        self.scale = 0
        self.m = 1
        self.b = 0


def update(*args):
    global opts, lines, test_regions, rec_channels
    opts.coarse = s_coarse.val
    opts.fine = s_fine.val
    opts.scale = s_scale.val
    opts.m = 1 - 0.000003 * opts.scale
    opts.b = opts.fs * opts.coarse + 100 * opts.fine
    for i, reg in enumerate(test_regions):
        for j, ch in enumerate(rec_channels):
            lines[j, i].set_ydata(get_trace(j, reg))


def scroll_callback(event):
    global sliders
    add = 0
    if event.button == 'up':
        add = 1
    elif event.button == 'down':
        add = -1
    ax = None
    for i, x in enumerate(sliders):
        if event.inaxes == x.ax:
            ax = i
    if ax is not None and add != 0:
        ax = sliders[ax]
        ax.set_val(ax.val + add)


def get_trace(j, reg):
    global c_add, opts, rec_channels
    x = numpy.arange(*reg) * opts.m + opts.b
    # don't forget to add correction for different sample rates if record fs != scanbox fs
    return c_add[rec_channels[j]][x.astype('int')]

def save(event):
    global c_add, opts, trace, target, b_save
    b_save.label.set_text('Saving...')
    x = numpy.arange(len(trace)) * opts.m + opts.b
    cut_end = False
    if x[-1] > len(c_add[0]):
        cut_end = numpy.where(x > len(c_add[0]))[0][0]
        x = x[:cut_end]
    a = numpy.empty((len(c_add), len(trace)))
    for i, c in enumerate(c_add):
        a[i, :len(x)] = c[x.astype('int')]
    if cut_end:
        a[:, cut_end:] = 0
    prefix = target[:-6]
    a.tofile(prefix+'.morephys')
    b_save.label.set_text('Done...')


# input:
path = 'S://Barna//ihka//'
fdict = {
    # 'ihka_3_103_401': 'rec_data_2018_Dec_17___17_30_34',
    # 'ihka_3_103_500': 'rec_data_2018_Dec_18___14_37_41',
    # 'ihka_3_101_502': 'rec_data_2018_Dec_18___15_25_05',
    # 'ihka_3_103_600': 'rec_data_2018_Dec_19___13_52_55',
    # 'ihka_3_101_700': 'rec_data_2018_Dec_20___09_59_13',
    # 'ihka_3_103_701': 'rec_data_2018_Dec_20___10_43_15',
    # 'ihka_3_101_800': 'rec_data_2018_Dec_27___13_01_53',
    # 'ihka_3_103_800': 'rec_data_2018_Dec_27___13_49_51',
    # 'JF_104_100': 'rec_data_2019_Jan_17___16_23_19',
    'JF_104_222': 'rec_data_2019_Jan_18___14_51_33'}#,
    # 'JF_104_667': 'rec_data_2019_Jan_19___11_37_38'},
    # 'JF_101_666': 'rec_data_2019_Jan_19___10_47_33'},
    # 'JF_103_333': 'rec_data_2019_Jan_18___15_47_14'}

# parameters
fs = 10000  # sample rate of scanbox
match_channel = 0  # channel that is found in both files
add_channels = (1,)  # channels to save aligned

os.chdir(path)
for target, ephys in fdict.items():
    if not target.endswith('.ephys'):
        target += '.ephys'
    print(target, ephys)
    # read trace
    a = numpy.fromfile(target, dtype='float32')
    b = numpy.reshape(a, (int(len(a) / 2), 2))
    trace = b[:, 1]

    # read record file
    a = loadmat(ephys)
    r_rate = a['fs']
    rec_channels = [match_channel, *add_channels]
    c_add = [a['s'][:, c] for c in rec_channels]

    # set up plot to review match
    l = len(trace)
    test_regions = [[int(l * x), int(l * x + fs)] for x in (0.1, 0.5, 0.9)]
    test_regions = [[0, 10 * fs], *test_regions]

    hkw = {'height_ratios': [1, *[10] * (len(add_channels) + 1)]}
    fig, ax = plt.subplots(nrows=len(add_channels) + 2, ncols=len(test_regions), gridspec_kw=hkw)
    # remove all ticks and labels:
    for ca in ax.flatten():
        ca.set_xticks([])
        ca.set_yticks([])
        ca.set_xticklabels([])
        ca.set_yticklabels([])

    # set up sliders
    opts = Opts(fs)
    s_coarse = Slider(ax[0, 0], 'Coarse', -60, 60, valinit=0, valfmt='%1.0f')
    s_fine = Slider(ax[0, 1], 'Fine', -100, 100, valinit=0, valfmt='%1.0f')
    s_scale = Slider(ax[0, 2], 'Scale', -100, 100, valinit=0, valfmt='%1.0f')
    b_save = Button(ax[0, 3], 'Save')
    b_save.on_clicked(save)
    sliders = [s_coarse, s_fine, s_scale]
    for s in sliders:
        s.on_changed(update)

    whl = fig.canvas.mpl_connect('scroll_event', scroll_callback)

    # plot reference trace:
    lines = numpy.ndarray((len(add_channels) + 1, len(test_regions)), dtype='object')
    add_colors = ('#ff9f48', '#8c27ff', '#4affa0')
    for i, reg in enumerate(test_regions):
        ax[1, i].plot(trace[reg[0]:reg[1]], color='#0080ff', alpha=0.8)
        # plot match channels and keep reference to allow mod:
        for j, ch in enumerate(rec_channels):
            lines[j, i] = ax[1 + j, i].plot(get_trace(j, reg), color=add_colors[j], alpha=0.8)[0]

    # add fancies
    for i, text in enumerate(('First 10 secs', ' 1 sec at 10% of recording', '1s @50 %', '1s @90%')):
        ax[1, i].set_title(text)
    ax[1,0].set_ylabel('Ref. channels')
    for i, c in enumerate(add_channels):
        ax[i+2, 0].set_ylabel(f'Channel {c}')
    plt.show(block=True)

#testing
# prefix = target[:-6]
# res = numpy.fromfile(prefix+'.morephys', dtype='float64')
# res_trace = numpy.reshape(res, (len(c_add), len(trace)))
# span = slice(1000000,11000000)
# # span = slice(0, 1000000)
# fig, ax = plt.subplots(3, sharex=True)
# ax[0].plot(trace[:], color='black', label='scanbox')
# # ax[1].plot(c_add[0][span], color='blue', label='RecorderC1')
# # ax[2].plot(c_add[1][span], color='red', label='RecorderC2')
# ax[1].plot(res_trace[0, :], color='blue', label='RecorderC1')
# ax[2].plot(res_trace[1, :], color='red', label='RecorderC2')
# for cax in ax:
#     cax.legend(loc='upper right')
