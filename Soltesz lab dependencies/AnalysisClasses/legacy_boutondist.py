import fnmatch
from scipy.interpolate import UnivariateSpline as spline
import scipy
import numpy
from scipy.optimize import minimize
from collections import Counter
# import PIL.Image
import copy

class boutons(object):
    def __init__(self, img):
        self.tag = img
        self.ob = self.fitborder(self.getborder(img.replace('results.txt', 'oriselection.txt')))
        self.rb = self.fitborder(self.getborder(img.replace('results.txt', 'radselection.txt')))
        bd = self.getboutons(img)
        self.bloc = bd[0]
        self.blayer = bd[1]
        self.n = bd[2]

    def __getitem__(self, i):
        return self.bloc[:, i]

    def fitborder(self, border):
        x = border[0]
        y = border[1]
        t = numpy.zeros(x.shape)
        t[1:] = numpy.sqrt((x[1:] - x[:-1]) ** 2 + (y[1:] - y[:-1]) ** 2)
        t = numpy.cumsum(t)
        t /= t[-1]
        nt = numpy.linspace(0, 1, 2048)
        '''
         #updated to work with current scipy. old was:
         scipy.interpolate.spline(xk, yk, xnew, order=3, kind='smoothest', conds=None)
         which readily returned fit y for xnew
         new is:
         scipy.interpolate.UnivariateSpline
         takes x, y, returns a function that can be called with new x to obtain fit y.
        '''
        x2 = spline(t, x)(nt)
        y2 = spline(t, y)(nt)
        #        plt.plot(x2, y2, label='dist_spline')
        #        assert False
        return [x2, y2]

    def calcdists(self):
        ods = self.bldist(self.ob)
        self.ods = ods
        rds = self.bldist(self.rb)
        self.rds = rds
        absdists = []
        reldists = []
        for i in range(self.n):
            p = self[i]
            od = ods[0][i]
            oloc = ods[1][:, i]
            rd = rds[0][i]
            rloc = rds[1][:, i]
            ld = self.dist2p(oloc, rloc)
            if self.blayer[i] == 0:
                d = (od + ld * 0.5) * -1
            elif self.blayer[i] == 1:
                if od >= rd:
                    d = ld * 0.5 - rd
                else:
                    d = (ld * 0.5 - od) * -1
            else:
                d = rd + ld * 0.5
            absdists.append(d)
            reldists.append(d / ld)
        self.absdists = absdists
        self.reldists = reldists
        return absdists, reldists

    def dist2p(self, a, b):
        x = a[0] - b[0]
        y = a[1] - b[1]
        return numpy.sqrt(x ** 2 + y ** 2)

    def getborder(self, f):
        with open(f) as f:
            lines = f.readlines()
        bcoords = numpy.empty((2, len(lines) - 1), dtype='int16')
        for i in range(1, len(lines)):
            l = lines[i].split('\t')
            bcoords[:, i - 1] = [int(l[1]), int(l[2])]
        return bcoords

    def getboutons(self, f):
        with open(f.replace('.txt', '.csv')) as f: ###replace for hh current use
            lines = f.readlines()
        bcoords = numpy.empty((2, len(lines) - 1), dtype='int16')
        layers = []
        for i in range(1, len(lines)):
            l = lines[i].split(',') ###csv for hh current use, original tabsep
            bcoords[:, i - 1] = [int(l[4]), int(l[5])]
            layers.append(int(l[3]))
        return bcoords, layers, len(layers)

    def bldist(self, border):
        dists = []
        points = numpy.empty((2, self.n), dtype='int16')
        for p in range(self.n):
            x = self[p][0]
            y = self[p][1]
            mind = 99999
            for i in range(len(border[0])):
                dx = x - border[0][i]
                dy = y - border[1][i]
                d = numpy.sqrt(dx * dx + dy * dy)
                if d < mind:
                    mind = d
                    mini = i
            dists.append(mind)
            points[:, p] = [int(border[0][mini]), int(border[1][mini])]
        return dists, points

    def distf(self, x):
        dx = self.point[0] - x
        dy = self.point[1] - int(self.f(x))
        return numpy.sqrt(dx * dx + dy * dy)

    def hdists(self, soma):
        self.point = soma
        x = soma[0]
        y = soma[1]
        mind = 99999
        border = self.rb
        for i in range(len(border[0])):
            dx = x - border[0][i]
            dy = y - border[1][i]
            d = numpy.sqrt(dx * dx + dy * dy)
            if d < mind:
                mind = d
                mini = i
        p2 = [int(border[0][mini]), int(border[1][mini])]
        self.m = float(soma[1] - p2[1]) / (soma[0] - p2[0])
        self.b = soma[1] - self.m * soma[0]
        self.f = self.line
        dists = []
        points = numpy.empty((2, self.n), dtype='int16')
        for i in range(self.n):
            self.point = self[i]
            minx = minimize(self.distf, self.point[0], method='Nelder-Mead').x
            dists.append(self.distf(minx))
            points[:, i] = [int(minx), int(self.f(minx))]
        self.hdists = dists
        return dists, points

    def line(self, x):
        return self.m * x + self.b


class cell(object):
    def __init__(self, c):
        self.tag = c
        self.getsoma(c + '_soma.txt')
        self.bsets = []
        for img in self.getflist(c):
            try:
                self.bsets.append(boutons(img))
            except:
                print
                'Reading data failed for ' + img
        for bset in self.bsets:
            try:
                bset.calcdists()
                bset.hdists(self.soma)
            except:
                print
                'Calculations failed for ' + bset.tag

    def getsoma(self, c):
        with open(c) as f:
            lines = f.readlines()
        l = lines[1].split('\t')
        self.soma = [int(l[2]), int(l[3])]
        self.calib = float(l[4])

    def getflist(self, c):
        global tflist
        fl = []
        key = '*' + c + '_results.txt'
        for df in tflist:
            if fnmatch.fnmatch(df, key):
                fl.append(df)
        return fl

    def bdi(self):
        try:
            rd = []
            for bset in self.bsets:
                rd.extend(bset.reldists)
            rd = numpy.array(rd, dtype=float).round(1)
            mode = self.mode(rd).round(1)
            iqr = abs(numpy.subtract(*numpy.percentile(rd, [75, 25])))
            bdi = 0.5 / (abs(mode) * iqr)
            return bdi
        except:
            print
            'BDI calculation failed for ' + self.tag
            return None

    def mode(self, data):
        mean = data.mean()
        data = Counter(data)
        m = data.most_common()
        if m[0][1] > m[1][1]:
            return m[0][0]
        else:
            if abs(m[0][0] - mean) < abs(m[1][0] - mean):
                return m[0][0]
            else:
                return m[1][0]

    def bdens(self):
        bl = []
        for bset in self.bsets:
            for i in range(bset.n):
                bl.append(list(bset[i]))
        try:
            tree = scipy.spatial.cKDTree(bl)
            ds = []
            for p in bl:
                nl = tree.query_ball_point(p, 100)
                mind = 100
                for i in nl:
                    p2 = bl[i]
                    if not p is p2:
                        d = self.dist2p(p, p2)
                        if d < mind:
                            mind = d
                ds.append(mind)
            self.bnndists = ds
            return ds
        except:
            print
            'Density calculation failed for ' + self.tag
            return None

    def dist2p(self, a, b):
        x = a[0] - b[0]
        y = a[1] - b[1]
        return numpy.sqrt(x ** 2 + y ** 2)

    def heatmap(self):
        c = self.calib
        pim = numpy.zeros((128, 128), dtype=numpy.float)
        bl = []
        for bset in self.bsets:
            for i in range(bset.n):
                bl.append([bset.hdists[i], bset.absdists[i]])
        for p in bl:
            x = min(int((p[0] * c) / 5), 127)
            y = min(int((p[1] * c) / 5) + 64, 127)
            pim[y, x] += 1
        self.hmapim = copy.deepcopy(pim)
        pim *= 255 / numpy.amax(pim)
        #        pim[64,64:]=128
        return PIL.Image.fromarray(numpy.array(pim, dtype=numpy.uint8))

