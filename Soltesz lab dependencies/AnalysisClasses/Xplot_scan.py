from ImportFirst import *
from multiprocessing import Process
from scipy import stats

'''a worker that can be called to compute the X plot param scan on a single session'''


class Xplot_Worker(Process):
    def __init__(self, queue):
        super(Xplot_Worker, self).__init__()
        self.queue = queue

    def run(self):
        for args, kwargs in iter(self.queue.get, None):
            Xplot_compute(*args, **kwargs)


def Xplot_compute(path, session, output_path, window_list, offset_list, cellgroup_names, cellgroup_arrays,
                  name_of_source, name_of_targets, exclude_settings, save_example, bins, norm_inputs=True):
    '''
    :param session:ImagingSession
    :param window_list: window sizes (in frames) to use
    :param offset_list: offsets of source trace (in frames)
    :param cellgroup_names: e.g.('All cells', 'PV cells', 'Place cells'). Sorted bins will be computed for all
    :param cellgroup_arrays: list of the raw inputs, corresponding to cellgroup_names
    :param name_of_source: the value in cellgroup_names which is sorted
    :param name_of_targets: list of values cellgroup_names where correlation with source is computed
    :param exclude_settings: list of options, implemented: ('all', 'exclude_movement', 'only_movement')
    :param save_example: save the sorted resampled traces with these settings {'window': [15], 'offset': [0]}
    :param bins: number of bins for the averaging, default is 25
    :return: saves the sorted bins array, sahpe (len(window_list), len(offset_list), len(cellgroup_arrays), bins))
    '''
    a = session
    prefix = a.prefix
    print(prefix, ': Worker called')
    n_shuff = 30
    norm_post = False
    for excl_setting in exclude_settings:  # 'only_stop', 'exclude_stop',
        savepath = f'{output_path}/{excl_setting}/'
        resampled_sorted_path = savepath + 'resampled_traces/'
        for n_p in (savepath, resampled_sorted_path):
            if not os.path.exists(n_p):
                os.mkdir(n_p)
        remaining_indices = numpy.ones(a.ca.frames, dtype='bool')
        if excl_setting == 'exclude_movement':  # any movement and the 10 secs after
            for i in range(a.ca.frames):
                if numpy.any(a.pos.gapless[int(max(0, i - 10 * fps)):i]):
                    remaining_indices[i] = False
        elif excl_setting == 'only_movement':  # inverse of exclude_movement
            remaining_indices = numpy.zeros(a.ca.frames, dtype='bool')
            for i in range(a.ca.frames):
                if numpy.any(a.pos.gapless[int(max(0, i - 10 * fps)):i]):
                    remaining_indices[i] = True
        elif excl_setting == 'running':  # exclude gpaless bu tnot stop
            remaining_indices = numpy.zeros(a.ca.frames, dtype='bool')
            for i in range(a.ca.frames):
                if a.pos.speed[i] > 0.1:
                    remaining_indices[i] = True
        elif excl_setting == 'all':
            pass
        else:
            print('Exclusion setting not implemented: ', excl_setting)
            return -1

        input_traces = {}
        excl_indices = numpy.logical_not(remaining_indices)
        for y, g_n in zip(cellgroup_arrays, cellgroup_names):
            Y = numpy.copy(y)
            Y[excl_indices] = numpy.nan
            if norm_inputs:
                norm(Y)
            input_traces[g_n] = Y

        # PLOT SORTED in time bins

        input_param = input_traces[name_of_source]
        sort_param = numpy.empty(input_param.shape)
        sorted_traces = {}
        sorted_bins = numpy.empty((len(window_list), len(offset_list), len(cellgroup_arrays), bins))
        sorted_bins[:] = numpy.nan
        Rvals = numpy.empty((len(window_list), len(offset_list), len(cellgroup_arrays), 2))  # last dim:actual, shuffled
        Rvals[:] = numpy.nan
        resampled_traces = {}
        for wi, window in enumerate(window_list):
            if len(input_param) / window < 5:
                continue
            target = numpy.empty(int(len(input_param) / window))
            target[:] = numpy.nan
            for g_n in name_of_targets:
                for i in range(len(target)):
                    target[i] = numpy.nanmean(input_traces[g_n][i * window:(i + 1) * window])
                resampled_traces[g_n] = numpy.copy(target)
            for oi, offset in enumerate(offset_list):
                save_this = window in save_example['window'] and offset in save_example['offset']
                if offset > 0:
                    sort_param[:offset] = numpy.nan
                    sort_param[offset:] = input_param[:-offset]
                elif offset < 0:
                    sort_param[offset:] = numpy.nan
                    sort_param[:offset] = input_param[-offset:]
                elif offset == 0:
                    sort_param[:] = input_param
                source = numpy.empty(int(len(input_param) / window))
                source[:] = numpy.nan
                for i in range(len(source)):
                    source[i] = numpy.nanmean(sort_param[i * window:(i + 1) * window])
                # get indices sorted, but exclude indices that point to nan values
                indices = [x for x in numpy.argsort(source) if not numpy.isnan(source[x])]
                sorting = numpy.array(indices)
                Y_source = numpy.copy(source[sorting])
                if norm_post:
                    norm(Y_source)
                sorted_traces[name_of_source] = Y_source
                #removed this, creates 10s thousands of unnecessary files for all settings. if need example, fix it.
                # if save_this:
                #     numpy.save(path + resampled_sorted_path + f'{prefix}_w{window}_o{offset}_{name_of_source}.npy',
                #                Y_source)
                # compute the correlation of the target trace, including shuffles for control
                skip_bin_mean = True
                if not (~numpy.isnan(Y_source)).sum() > 3:
                    continue
                for g_n in name_of_targets:
                    Y_target = numpy.copy(resampled_traces[g_n][sorting])
                    incl = numpy.logical_and(~numpy.isnan(Y_source), ~numpy.isnan(Y_target))
                    if not incl.sum() > 3:
                        continue
                    skip_bin_mean = False
                    if norm_post:
                        norm(Y_target)
                    sorted_traces[g_n] = Y_target
                    if save_this:
                        numpy.save(path + resampled_sorted_path + f'{prefix}_w{window}_o{offset}_{g_n}.npy', Y_target)
                    try:
                        Rvals[wi, oi, cellgroup_names.index(g_n), 0] = stats.spearmanr(Y_source,
                                                                                       Y_target, nan_policy='omit')[0]
                    except:
                        print(wi, oi, len(Y_source), len(Y_target), (~numpy.isnan(Y_source)).sum(),
                              (~numpy.isnan(Y_target)).sum())
                        raise ValueError('correlation compute failed')
                    Y_shuff = numpy.copy(Y_target)
                    R_shuff = numpy.empty(n_shuff)
                    for i in range(n_shuff):
                        numpy.random.shuffle(Y_shuff)
                        R_shuff[i] = stats.spearmanr(Y_source, Y_shuff, nan_policy='omit')[0]
                    Rvals[wi, oi, cellgroup_names.index(g_n), 1] = R_shuff.mean()
                # recalculate mean in bins
                if skip_bin_mean:
                    continue
                for k, g_n in enumerate(cellgroup_names):
                    y = input_traces[g_n]
                    for i in range(bins):
                        binslice = slice(int(i * len(sorting) / bins), int((i + 1) * len(sorting) / bins))
                        incl_periods = sorting[binslice]
                        current_bin = numpy.empty(len(incl_periods))
                        for j, t in enumerate(incl_periods):
                            current_bin[j] = numpy.nanmean(y[t * window: (t + 1) * window])
                        sorted_bins[wi, oi, k, i] = numpy.nanmean(current_bin)
                # binning of bins - faster, could yield different result, I don't understand why in 'All' setting
                # binslices = [0] * bins
                # for i in range(bins):
                #     binslices[i] = slice(int(i * len(sorting) / bins), int((i + 1) * len(sorting) / bins))
                # for k, g_n in enumerate(cellgroup_names):
                #     for i, current_slice in enumerate(binslices):
                #         sorted_bins[wi, oi, k, i] = numpy.nanmean(sorted_traces[g_n][current_slice])
            print(f'{prefix}: {100 * wi / len(window_list):.0f}% ({excl_setting})')
        numpy.save(path + savepath + prefix + f'Xplot_Rvals by {name_of_source} norm {norm_post}.npy', Rvals)
        numpy.save(path + savepath + prefix + f'Xplot_sorted_bins by {name_of_source} norm {norm_post}.npy',
                   sorted_bins)


def norm(y):
    y -= numpy.nanmin(y)
    y /= numpy.nanmax(y)
