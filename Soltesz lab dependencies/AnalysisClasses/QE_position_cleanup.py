import numpy
from Batch_Utils import outlier_indices
from ImportFirst import fps

def cleanup_pos(pos_in):
    pos = numpy.copy(pos_in)
    pos -= pos[100]
    pos[:100] = 0
    # clean up speed
    speed = numpy.empty(pos.shape)
    speed[:] = numpy.nan
    speed[1:] = numpy.diff(pos)
    # exclude outliers
    moving_index = numpy.where(speed > 1)
    ol_index = outlier_indices(speed[moving_index], thresh=6)
    speed[moving_index[0][ol_index]] = numpy.nan
    speed[numpy.abs(speed) > 50] = numpy.nan
    # transform back to pos
    pos_cln = numpy.nancumsum(speed)
    return pos_cln, speed * fps