import numpy
import tkinter
from tkinter.filedialog import askopenfilenames

root = tkinter.Tk()
fn = askopenfilenames(parent=root,title='Choose a file')
fn = root.tk.splitlist(fn)
for f in fn:
    with open(f, 'r') as tf:
        lines = tf.readlines()
    ea = numpy.zeros((len(lines), 2))
    try:
        for i, l in enumerate(lines):
            ea[i, :] = [float(x) * 10000 for x in l.strip().split('\t')]
        ea.tofile(f.replace('.txt', '.ripples'))
        print(f, 'saved.')
    except:
        print('Could not convert', f)
root.destroy()
