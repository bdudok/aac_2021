import numpy

from ImportFirst import *
from sklearn import cluster
from EyeTracking import clean_movement_map, clean_whisker_map, parse_triggers
from SCA_reader import get_spikes


# define functions to detect events for pulling mean traces. keep each version for future reuse and reference
# functions should take the session and window,
# and return the trigger list and pull mask (indices or none for each event and Dt)
def single_ripples_immobility(a, w):
    events_used = []
    event_t = []
    for event in a.ripples.events:
        ripple_frame = a.sampletoframe(event.p[0])
        if 100 < ripple_frame < a.ca.frames - 100:
            if not a.pos.gapless[ripple_frame]:
                events_used.append(event)
                event_t.append(numpy.mean(event.p))  # used for clustering only
    event_t = numpy.array(event_t)
    clustering = cluster.DBSCAN(eps=4000, min_samples=2).fit(event_t.reshape(-1, 1))
    single_ripple_indices = numpy.where(clustering.labels_ < 0)[0]
    decay_w = 6
    gap = 15
    mask = numpy.zeros((len(single_ripple_indices), 2 * w))
    events = numpy.empty(len(single_ripple_indices), dtype=numpy.int64)
    mask[:] = numpy.nan
    trim = min(100, w)
    for ri, rj in enumerate(single_ripple_indices):
        # trim end to start of next ripple
        current_frame = a.sampletoframe(events_used[rj].p[0])
        if rj < len(events_used) - 1:
            last_frame = min(current_frame + w, a.sampletoframe(events_used[rj + 1].p[0]) - decay_w)
        else:
            last_frame = current_frame + w
        if rj > 0:
            first_frame = max(current_frame - w, a.sampletoframe(events_used[rj - 1].p[0]) + decay_w)
        else:
            first_frame = current_frame - w
        i0 = max(trim, first_frame)
        im = current_frame - i0
        i1 = min(last_frame, a.ca.frames - trim)
        m = a.pos.gapless[i0:i1]
        # check if mouse was still all the time during the pre period and find last movement if not.
        if numpy.any(m[:im - gap]):
            w0 = gap + numpy.argmax(m[:im - gap][::-1])
        else:
            w0 = im
        # find first movement frame after event
        if numpy.any(m[im + gap:]):
            w1 = numpy.argmax(m[im + gap:]) + gap
        else:
            w1 = i1 - current_frame
        # set actual indices
        mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
        events[ri] = current_frame
    return events, mask


def single_ripples_clean_history(a, w):  # only ripples where there was no other ripple or running in the 5 secs before
    events_used = []
    event_t = []
    decay_w = 6
    gap = 15
    fps = 15.6
    gap_len = 5
    fs = 10000
    for event in a.ripples.events:
        ripple_frame = a.sampletoframe(event.p[0])
        if 100 < ripple_frame < a.ca.frames - 100:
            if not a.pos.gapless[ripple_frame]:
                events_used.append(event)
                event_t.append(numpy.mean(event.p))  # used for clustering only
    event_t = numpy.array(event_t)
    single_ripple_indices = []
    for ti, t1 in enumerate(event_t):
        if ti == 0:
            t0 = 100
        else:
            t0 = event_t[ti - 1]
        if t1 - t0 > gap_len * fs:
            t_frame = a.sampletoframe(int(t1))
            if not numpy.any(a.pos.gapless[t_frame - int(gap_len * fps):t_frame]):
                single_ripple_indices.append(ti)
    mask = numpy.zeros((len(single_ripple_indices), 2 * w))
    events = numpy.empty(len(single_ripple_indices), dtype=numpy.int64)
    mask[:] = numpy.nan
    trim = min(100, w)
    for ri, rj in enumerate(single_ripple_indices):
        # trim end to start of next ripple
        current_frame = a.sampletoframe(events_used[rj].p[0])
        if rj < len(events_used) - 1:
            last_frame = min(current_frame + w, a.sampletoframe(events_used[rj + 1].p[0]) - decay_w)
        else:
            last_frame = current_frame + w
        if rj > 0:
            first_frame = max(current_frame - w, a.sampletoframe(events_used[rj - 1].p[0]) + decay_w)
        else:
            first_frame = current_frame - w
        i0 = max(trim, first_frame)
        im = current_frame - i0
        i1 = min(last_frame, a.ca.frames - trim)
        m = a.pos.gapless[i0:i1]
        # check if mouse was still all the time during the pre period and find last movement if not.
        if numpy.any(m[:im - gap]):
            w0 = gap + numpy.argmax(m[:im - gap][::-1])
        else:
            w0 = im
        # find first movement frame after event
        if numpy.any(m[im + gap:]):
            w1 = numpy.argmax(m[im + gap:]) + gap
        else:
            w1 = i1 - current_frame
        # set actual indices
        mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
        events[ri] = current_frame
    return events, mask


def running(a, w, loc_type=0, gap=None):
    'loc 0 for starts, 1 for stops, 2 for both'
    gap = 15
    starts, stops = a.startstop(ret_loc='actual', gap=gap)
    startstop_mask = numpy.zeros((2, len(starts), 2 * w))
    startstop_mask[:] = numpy.nan
    trim = min(100, w)
    events = numpy.empty((2, len(starts)), dtype=numpy.int64)
    for i, t in enumerate(starts):
        # determine lookup window (for cases where pre or post would be outside of session)
        # have to change this with stop loc=actual, because gapless will be moving after stop. modifying lookup window with gap
        i0 = max(trim, t - w)
        im = t - i0
        i1 = min(a.ca.frames - trim, t + w)
        m = a.pos.gapless[i0:i1]
        # check if mouse was still all the time during the pre period and find last movement if not.
        if numpy.any(m[:im - gap]):
            w0 = gap + numpy.argmax(m[:im - gap][::-1])
        else:
            w0 = im
        # find last movement frame after start
        if numpy.all(m[im:]):
            w1 = i1 - t
        else:
            w1 = numpy.argmin(m[im:]) - gap
        # set actual indices
        startstop_mask[0, i, w - w0:w + w1] = numpy.arange(t - w0, t + w1)
        events[0, i] = t
    # same logic for stops, but conditions reverted:
    for i, t in enumerate(stops):
        # determine lookup window (for cases where pre or post would be outside of session)
        i0 = max(10, t - w)
        im = t - i0
        i1 = min(a.ca.frames - 10, t + w)
        m = a.pos.gapless[i0:i1]
        # check if mouse was running all the time during the pre period and find first movement if not.
        if numpy.all(m[:im]):
            w0 = im
        else:
            w0 = numpy.argmin(m[:im][::-1]) - gap
        # find first movement frame after stop
        if numpy.any(m[im + gap:]):
            w1 = numpy.argmax(m[im + gap:]) + gap
        else:
            w1 = i1 - t
        # set actual indices
        startstop_mask[1, i, w - w0:w + w1] = numpy.arange(t - w0, t + w1)
        events[1, i] = t
    if loc_type == 2:
        return events, startstop_mask
    elif loc_type in (0, 1):
        return events[loc_type], startstop_mask[loc_type]


def whisking_v2(a, w, decay=16, eye_path=None):
    '''this is an updated version for better inclusion of separate non-locomotory events'''
    face = clean_movement_map(a.prefix, a, eye_path=eye_path)
    frame_indices = numpy.where(face > 1)[0]
    return nonoverlap_from_list(a, w, frame_indices, eps=decay, exclude_move=False)


def whisking_v3(a, w, decay=16, min_l=None):
    '''this, compared to v2, zooms in on whisker pad'''
    face = clean_whisker_map(a.prefix, a)
    frame_indices = numpy.where(face > 1)[0]
    return nonoverlap_from_list(a, w, frame_indices, eps=decay, exclude_move=False, min_n=min_l)


def whisking_v4(a, w, decay=16, min_l=None):
    '''as v3, but returns max of cluster instead of beginning. helps with more active mice'''
    face = clean_whisker_map(a.prefix, a)
    frame_indices = numpy.where(numpy.logical_and(face > 1, numpy.logical_not(a.pos.gapless)))[0]
    return nonoverlap_from_list(a, w, frame_indices, eps=decay, exclude_move=False, min_n=min_l,
                                clustloc='max', trace=face)


def trigstim(a, w, stimlen=45, snifflen=15, run_type='immo', imm_before=16, imm_after=48):
    '''parse the text file for timestamps and return as events
    run_type: 'immo', 'run' or 'all; 'sniff' or 'nosniff' for whisk during immobile
    'immo_ext': stable before, after. 'immo_norm': pick random frames instead
    '''
    frame_indices = parse_triggers(a.prefix)
    if frame_indices is None or len(frame_indices) < 1:
        raise ValueError('Stim frames not found')
    if run_type == 'immo':
        frame_indices = [x for x in frame_indices if numpy.all(a.pos.gapless[x:x + stimlen] == 0)]
    elif 'immo_ext' in run_type:
        frame_indices = [x for x in frame_indices if numpy.all(a.pos.gapless[x - imm_before:x + imm_after] == 0)]
    elif 'immo_norm' in run_type:
        xs = numpy.arange(100, a.ca.frames-100)
        frame_pool = [x for x in xs if numpy.all(a.pos.gapless[x - imm_before:x + imm_after] == 0)]
        frame_indices = numpy.random.choice(frame_pool, len(frame_indices), replace=False)
    elif run_type == 'run':
        frame_indices = [x for x in frame_indices if numpy.any(a.pos.gapless[x:x + stimlen])]
    if 'sniff' in run_type:
        if run_type == 'sniff' or run_type == 'nosniff':
            frame_indices = [x for x in frame_indices if numpy.all(a.pos.gapless[x:x + stimlen] == 0)]
        face = clean_whisker_map(a.prefix, a)
        if 'nosniff' in run_type:
            frame_indices = [x for x in frame_indices if numpy.all(face[x:x + snifflen] < 1)]
        else:
            frame_indices = [x for x in frame_indices if numpy.any(face[x:x + snifflen] > 1)]
    return nonoverlap_from_list(a, w, frame_indices)


def whisking(a, w):
    face = clean_movement_map(a.prefix, a)
    # peak detection - simple: take over sd 1 frames, and cluster
    frame_indices = numpy.where(face > 1)[0]
    clustering = cluster.DBSCAN(eps=15, min_samples=1).fit(frame_indices.reshape(-1, 1))
    labels = clustering.labels_
    events_used = []
    event_t = []
    decay_w = 6
    gap = decay_w
    mask = numpy.zeros((labels.max(), 2 * w))
    mask[:] = numpy.nan
    trim = min(100, w)
    good_lines = []
    min_keep = 45
    for cid in range(labels.max()):
        x = frame_indices[numpy.where(labels == cid)[0]]
        events_used.append(cid)
        current_frame = x.min()
        event_t.append(current_frame)
        i0 = max(trim, current_frame - w)
        im = current_frame - i0
        i1 = min(current_frame + w, a.ca.frames - trim)
        # make combined movement or face trace for exclusions
        m_a = a.pos.gapless[i0:i1]
        m_b = face[i0:i1] > 1
        m = numpy.logical_or(m_a, m_b)
        # check if mouse was still all the time during the pre period and find last movement if not.
        if numpy.any(m[:im - gap]):
            w0 = gap + numpy.argmax(m[:im - gap][::-1])
        else:
            w0 = im
        # find first movement frame after event
        if numpy.any(m[x.max() - i0 + gap:]):
            w1 = numpy.argmax(m[x.max() - i0 + gap:]) + gap
        else:
            w1 = i1 - current_frame
        # filter for good lines
        if all([w0 > min_keep, w1 > min_keep]):
            good_lines.append(cid)
        # set actual indices
        mask[cid, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
    return numpy.array(event_t, dtype=numpy.int64)[good_lines], mask[good_lines]


def SWR_In(a, w):
    return all_ripples_immobility(a, w, when='In')


def SWR_Out(a, w):
    return all_ripples_immobility(a, w, when='Out')


def all_ripples_immobility(a, w, when='In'):
    '''After adding all single ripples, add ripple clusters by cluster
    There was a version which could look at "ripple out" - contained bug, removed. if needed, implement it using
    event.p[1] for the times, and last members of clusters
    '''
    if when == 'Out':
        print('Ripple out not implemented, see function description')
        assert False
    event_t = []
    for event in a.ripples.events:
        ripple_frame = a.sampletoframe(event.p[0])
        if 100 + w < ripple_frame < a.ca.frames - 100 - w:
            if not a.pos.gapless[ripple_frame]:
                event_t.append(ripple_frame)  # used for clustering only
    event_t = numpy.array(sorted(event_t))
    clustering = cluster.DBSCAN(eps=int(4 * fps), min_samples=2).fit(event_t.reshape(-1, 1))
    single_ripple_indices = numpy.where(clustering.labels_ < 0)[0]
    filtered_event_t = [event_t[ri] for ri in single_ripple_indices]
    for rci in range(clustering.labels_.max() + 1):
        current_cluster = numpy.where(clustering.labels_ == rci)[0]
        filtered_event_t.append(event_t[current_cluster[0]])
    event_t = sorted(filtered_event_t)
    decay_w = 6
    gap = 15
    event_number = len(event_t)
    mask = numpy.zeros((event_number, 2 * w))
    events = numpy.empty(event_number, dtype=numpy.int64)
    mask[:] = numpy.nan
    trim = min(100, w)
    for ri, current_frame in enumerate(event_t):
        if ri < event_number - 1:
            last_frame = min(current_frame + w, event_t[ri + 1] - decay_w)
        else:
            last_frame = current_frame + w
        if ri > 0:
            first_frame = max(current_frame - w, event_t[ri - 1] + decay_w)
        else:
            first_frame = current_frame - w
        i0 = max(trim, first_frame)
        im = current_frame - i0
        i1 = min(last_frame, a.ca.frames - trim)
        try:
            m = a.pos.gapless[i0:i1]
        except:
            print(ri, current_frame, w, event_number)
            assert False
        # check if mouse was still all the time during the pre period and find last movement if not.
        if numpy.any(m[:im - gap]):
            w0 = gap + numpy.argmax(m[:im - gap][::-1])
        else:
            w0 = im
        # find first movement frame after event
        if numpy.any(m[im + gap:]):
            w1 = numpy.argmax(m[im + gap:]) + gap
        else:
            w1 = i1 - current_frame
        # set actual indices. in case of overlap,skip this.
        if w0 > w:
            continue
        try:
            mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
        except:
            print(ri, current_frame, w, w0, w1, mask.shape)
            assert False
        events[ri] = current_frame

    return events, mask


def vis_stim(a, w, which=0):
    '''for run, stim-run and stim-no run in grating experiment (0-1-2 respectively)'''
    # get frames when light was turned on
    grating_stims = [a.timetoframe(time) for pin, time, on in a.bdat.other_events if pin == 11 and on]
    grating_stims = [x for x in grating_stims if 200 < x < a.ca.frames - 200]
    starts = a.startstop(ret_loc='actual')[0]
    starts = [x for x in starts if 200 < x < a.ca.frames - 200]

    # create events based on stim and split whether mouse started running
    run_treshold = 0.01
    fps = 15.6
    run_stims, imm_stims, start_sel = [], [], []
    for t in grating_stims:
        if abs(a.pos.relpos[t] - a.pos.relpos[int(t + 10 * fps)]) > run_treshold:
            run_stims.append(t)
        else:
            imm_stims.append(t)
    for start in starts:
        if numpy.abs(numpy.array(grating_stims) - start).min() > fps * 10:
            start_sel.append(start)
    trigs = [start_sel, run_stims, imm_stims][which]
    mask = numpy.zeros((len(trigs), 2 * w))
    events = numpy.empty(len(trigs), dtype=numpy.int64)
    mask[:] = numpy.nan
    trim = min(100, w)
    for ri, rj in enumerate(trigs):
        # trim end to start of next ripple
        current_frame = rj
        i0 = max(trim, rj - w)
        im = current_frame - i0
        i1 = min(rj + w, a.ca.frames - trim)
        w0 = im
        w1 = i1 - current_frame
        # set actual indices
        mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
        events[ri] = current_frame
    return events, mask


def immo_sync_event(a, w, pull_session=None, thr=None):
    if pull_session is None:
        pull_session = a  # to allow detection in single channel
    peaks = [x[0] for x in pull_session.sync_events(data_only=True, thr=thr)]
    event_t = []
    decay_w = 6
    gap = decay_w
    mask = numpy.zeros((len(peaks), 2 * w))
    mask[:] = numpy.nan
    trim = min(100, w)
    for cid, current_frame in enumerate(peaks):
        event_t.append(current_frame)
        i0 = max(trim, current_frame - w)
        im = current_frame - i0
        i1 = min(current_frame + w, a.ca.frames - trim)
        m = a.pos.gapless[i0:i1]
        # check if mouse was still all the time during the pre period and find last movement if not.
        if numpy.any(m[:im - gap]):
            w0 = gap + numpy.argmax(m[:im - gap][::-1])
        else:
            w0 = im
        # find first movement frame after event
        if numpy.any(m[im + gap:]):
            w1 = numpy.argmax(m[im + gap:]) + gap
        else:
            w1 = i1 - current_frame
        # set actual indices
        mask[cid, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
    return numpy.array(event_t, dtype=numpy.int64), mask


def masks_from_list(a, w, event_list):
    '''return all masks from a custom event list'''
    events = numpy.array(event_list, dtype=numpy.int64)
    mask = numpy.empty((len(events), 2 * w))
    mask[:] = numpy.nan
    trim = min(100, w)
    for ri, rj in enumerate(events):
        current_frame = rj
        i0 = max(trim, rj - w)
        im = current_frame - i0
        i1 = min(rj + w, a.ca.frames - trim)
        w0 = im
        w1 = i1 - current_frame
        # set actual indices
        mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
    return events, mask


def nonoverlap_from_list(a, w, event_list, decay=6, eps=16, exclude_move=False, min_n=None,
                         clustloc='first', trace=None):
    '''return masks for a custom event list with no overlap allowed between events.
    decay: minimum gap between masks
     ignores changes in locomotion
     eps: replaces clusters with onset of firsts. retain all if 0.
     clustloc: 'first' - returns first member of a cluster, 'max': returns loc of max in the trace arg
     '''
    events = numpy.array(event_list, dtype=numpy.int64)
    if min_n is None:
        min_n = 2
        exc_single = False
    else:
        exc_single = True
    if eps:
        event_t = numpy.copy(events)
        if len(event_t) < min_n:
            events = event_t
        else:
            clustering = cluster.DBSCAN(eps=eps, min_samples=min_n).fit(event_t.reshape(-1, 1))
            single_event_indices = numpy.where(clustering.labels_ < 0)[0]
            any_clusters = clustering.labels_.max() > 0
            if exc_single:
                if any_clusters:
                    event_number = clustering.labels_.max() + (len(single_event_indices) > 0)
                    events = numpy.empty(event_number, dtype=numpy.int64)
                    for rci in range(clustering.labels_.max() + (
                            len(single_event_indices) > 0)):  # indices are different if there are zero noise events
                        current_cluster = numpy.where(clustering.labels_ == rci)[0]
                        rj = current_cluster[0]
                        events[rci] = event_t[rj]
                else:
                    events = []
            else:
                event_number = len(single_event_indices)
                if any_clusters:
                    event_number += clustering.labels_.max()
                events = numpy.empty(event_number, dtype=numpy.int64)
                ri, rci = 0, 0
                for ri, rj in enumerate(single_event_indices):
                    # try:
                    events[ri] = event_t[rj]
                    # except:
                    #     print(len(event_t), len(events), len(single_event_indices), clustering.labels_.max(), ri, rj)
                    #     assert False
                if any_clusters:
                    for rci in range(clustering.labels_.max() + (
                            len(single_event_indices) > 0)):  # indices are different if there are zero noise events
                        # get list of ripples in cluster
                        current_cluster = numpy.where(clustering.labels_ == rci)[0]
                        if clustloc == 'first':
                            rj = current_cluster[0]
                        elif clustloc == 'max':
                            rj = current_cluster[numpy.argmax(trace[event_t[current_cluster]])]
                        events[ri + rci] = event_t[rj]
    events.sort()
    if exclude_move:
        events = events[~a.pos.gapless[events]]
    mask = numpy.zeros((len(events), 2 * w))
    mask[:] = numpy.nan
    # following algorithm taken and modified from single ripples
    for frame_index, current_frame in enumerate(events):
        # trim end to start of next
        if frame_index < len(events) - 1:
            last_frame = min(current_frame + w, max(events[frame_index + 1] - decay, current_frame + decay))
        else:
            last_frame = current_frame + w
        if frame_index > 0:
            first_frame = max(current_frame - w, min(events[frame_index - 1] + decay, current_frame - decay))
        else:
            first_frame = current_frame - w
        if first_frame > w and last_frame < a.ca.frames - w:
            w0 = current_frame - first_frame
            w1 = last_frame - current_frame
            # set actual indices
            mask[frame_index, w - w0:w + w1] = numpy.arange(first_frame, last_frame)
    return events, mask


def PlaceCellsDistance(a, w, opposite=False):
    starts, stops = a.startstop()
    events = numpy.array(stops, dtype=numpy.int64)
    mask = numpy.empty((len(events), 2 * w))
    mask[:] = numpy.nan
    pc_order, pc_loc = a.get_place_cell_order(loc=True)
    pf_calc_bins = pc_loc.max() + 1
    weights = numpy.zeros((len(events), a.ca.cells))
    trim = min(100, w)
    for ri, rj in enumerate(events):
        current_frame = rj
        i0 = max(trim, rj - w)
        im = current_frame - i0
        i1 = min(rj + w, a.ca.frames - trim)
        w0 = im
        w1 = i1 - current_frame
        # set actual indices
        mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
        # compute weights
        curr_pos = a.pos.relpos[current_frame]
        if opposite:
            curr_pos = a.opp_pos(curr_pos)
        pc_order = list(pc_order)
        for c in range(a.ca.cells):
            if c in pc_order:
                loc = pc_loc[pc_order.index(c)]
                d = 1 - a.reldist(loc / pf_calc_bins, curr_pos)
                weights[ri, c] = d
    return events, mask, weights


def PlaceCellsZscoreWeightedStop(a, w, opposite=False, exp_decay=8, loc_type=1, pf_calc_param='nnd'):
    'loc 0 for starts, 1 for stops'
    # This uses z score place fields instead of pre saved arrays to eliminate possible errors
    times = a.startstop()
    starts, stops = times
    events = numpy.array(times[loc_type], dtype=numpy.int64)
    mask = numpy.empty((len(events), 2 * w))
    mask[:] = numpy.nan
    weights = numpy.zeros((len(events), a.ca.cells))
    pf_calc_bins = 25
    a.placefields_smooth(param=pf_calc_param, silent=True, bins=pf_calc_bins, exp_decay=exp_decay)
    trim = min(100, w)
    for ri, rj in enumerate(events):
        current_frame = rj
        i0 = max(trim, rj - w)
        im = current_frame - i0
        i1 = min(rj + w, a.ca.frames - trim)
        w0 = im
        w1 = i1 - current_frame
        # set actual indices
        mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
        # compute weights
        curr_pos = a.pos.relpos[current_frame]
        zscores = numpy.copy(a.smooth_zscores)
        # center on mean, flip for opposite, floor at zero
        zscores -= zscores.mean()
        if opposite:
            curr_pos = a.opp_pos(curr_pos)
            zscores *= -1
        curr_pos = min(int(curr_pos * pf_calc_bins), pf_calc_bins - 1)
        for c in range(a.ca.cells):
            weights[ri, c] = zscores[curr_pos, c]
        weights = numpy.maximum(0, weights)
    return events, mask, weights


# def ControlWeighted(a, w, opposite, exp_decay):
#     starts, stops = a.startstop()
#     events = numpy.array(stops, dtype=numpy.int64)
#     mask = numpy.empty((len(events), 2 * w))
#     mask[:] = numpy.nan
#     pc_order, pc_loc = a.get_place_cell_order(loc=True)
#     pf_calc_bins = pc_loc.max() + 1
#     weights = numpy.zeros((len(events), a.ca.cells))
#     trim = min(100, w)
#     for ri, rj in enumerate(events):
#         current_frame = rj
#         i0 = max(trim, rj - w)
#         im = current_frame - i0
#         i1 = min(rj + w, a.ca.frames - trim)
#         w0 = im
#         w1 = i1 - current_frame
#         # set actual indices
#         mask[ri, w - w0:w + w1] = numpy.arange(current_frame - w0, current_frame + w1)
#         incl = numpy.logical_not(numpy.isnan(a.ca.ontr[pc_order, current_frame]))
#         for wi, loc in enumerate(pc_loc):
#             if incl[wi]:
#                 weights[ri, pc_order[wi]] = a.ca.ontr[pc_order[wi], current_frame-45]
#         weights = numpy.maximum(0, weights)
#         if opposite:
#             weights = weights.max() - weights
#     return events, mask, weights
#
#
# def StopPlace(a, w, opposite=False, exp_decay=8):
#     starts, stops = a.startstop()
#     return PlaceCellsWeighted(a, w, stops, opposite=opposite, exp_decay=exp_decay)


def StopResponse(a, w):
    starts, stops = a.startstop()
    return masks_from_list(a, w, stops)


def StopResponse_control(a, w, n=30):
    gap = 150
    stops = numpy.random.choice(numpy.arange(gap, a.ca.frames - gap), n, replace=False)
    return masks_from_list(a, w, stops)


def StartResponse(a, w):
    starts, stops = a.startstop()
    return masks_from_list(a, w, starts)


def StopResponseClosedLoop(a, w, stim_mode=True, stim_window=50):
    '''return stop response for events when stim was on or off - default gap setting is 150 for events!'''
    starts, stops = a.startstop()
    frame_list = [x for x in stops if numpy.any(a.opto[x:x + stim_window]) == stim_mode]
    return masks_from_list(a, w, frame_list)


def RunResponseClosedLoop(a, w, stim_mode=True, stim_cutoff=0.5, gap=15):
    '''return stop response for events when stim was on or off for more tan cutoff of frames during running'''
    starts, stops = a.startstop(ret_loc='actual', gap=gap)
    frame_list = []
    for start, stop in zip(starts, stops):
        stim_frames = numpy.count_nonzero(a.opto[start:stop]) / (stop - start)
        gt = stim_frames > stim_cutoff
        if stim_mode == gt:
            frame_list.append(start)
    return masks_from_list(a, w, frame_list)


def SWRResponseOpto(a, w, stim_mode=True, stim_window=50):
    '''return swr response for events when stim was on or off, stim window symmetric.'''
    events, masks = all_ripples_immobility(a, w)
    events = [x for x in events if w < x < a.ca.frames - w]
    frame_list = [x for x in events if numpy.any(a.opto[x - stim_window:x + stim_window]) == stim_mode]
    return masks_from_list(a, w, frame_list)


def OptoStimOnset(a, w, run_status=None, gap=None):
    stims = numpy.where(numpy.diff(a.opto.astype('byte')) > 0)[0]
    if run_status == 'run':
        stims = [i for i in stims if a.pos.movement[i]]
    elif run_status == 'rest':
        stims = [i for i in stims if not a.pos.movement[i]]
    # filter the list for distance from past tim
    if gap is not None:
        stims = [x for x in stims if not numpy.any(a.opto[x - gap:x])]
    return masks_from_list(a, w, stims)


def FirstRipples(a, w):
    '''return first ripples after stop. mask includes between running and next ripple. ripple clusters included
    delays are not returned to preserve compatibility with PSTH, non nan elements before w equals delay.'''
    dist_treshold = w
    # get cleared masks aligned on start of ripples (incl. ripple clusters)
    r_events, r_masks = all_ripples_immobility(a, w)
    # get stops
    starts, stops = a.startstop()
    # after each stop, find first event, store event, mask
    n = len(stops)
    events = numpy.empty(n)
    masks = numpy.empty((n, r_masks.shape[1]))
    good_stops = []
    for j, t in enumerate(stops):
        r_index = numpy.searchsorted(r_events, t)
        if r_index < len(r_events):
            delay = r_events[r_index] - t
            if 0 < delay < dist_treshold:
                good_stops.append(j)
                events[j] = r_events[r_index]
                masks[j] = r_masks[r_index]
    return events[good_stops], masks[good_stops]


def FirstRipples_Manual(a, w, control=False, delay_range=None):
    '''return mask centered on first ripples after stop.
    delays are read from file in ripples folder, created by running ripples.pick_delay
     mask includes between running and next ripple. ripple clusters included
     control: shuffle delays n times
    delays are not returned to preserve compatibility with PSTH, non nan elements before w equals delay.'''
    delay_fn = a.ripples.path + 'DelaysFirstAfterStop.npy'
    delays = numpy.load(delay_fn)
    stop_times = delays[0, :]
    delay_times = numpy.copy(delays[1, :])
    if delay_range is not None:
        incl1 = delay_times > delay_range[0]
        incl2 = delay_times <= delay_range[1]
        incl = numpy.logical_and(incl1, incl2)
        stop_times = stop_times[incl]
        delay_times = delay_times[incl]
    n = len(delay_times)
    if control:
        iterations = control
    else:
        iterations = 1
    if len(delay_times) < 1:
        events = numpy.empty(n * iterations)
        masks = numpy.empty((n * iterations, 2 * w))
        return events, masks
    events = numpy.empty(n * iterations)
    masks = numpy.empty((n * iterations, 2 * w))
    masks[:] = numpy.nan
    good_stops = []
    run = a.pos.gapless
    for k in range(iterations):
        if control:
            numpy.random.shuffle(delay_times)
            # delay_times = numpy.random.sample(n) * w
        for j in range(n):
            total_count = k * n + j
            t = int(stop_times[j])  # frame of stop
            events[total_count] = t
            if numpy.isnan(delay_times[j]):
                continue
            d = int(delay_times[j])  # length of delay in frames
            good_stops.append(total_count)
            first_frame = t
            current_frame = t + d
            if not numpy.any(run[d:]):
                last_frame = min(len(run), current_frame + w)
            else:
                last_frame = current_frame + min(w, numpy.argmax(run[current_frame:]))
            w0 = d
            w1 = last_frame - current_frame
            # set actual indices
            masks[total_count, w - w0:w + w1] = numpy.arange(first_frame, last_frame)
            # print(w, d, t, last_frame, w + last_frame - t - d)
            # masks[j, w - d:w + last_frame - t - d] = numpy.arange(t, last_frame)
    return events[good_stops], masks[good_stops]


def hfo_mask(a, w):
    event_t = []
    for event in a.ripples.events:
        ripple_frame = a.sampletoframe(event.p[0])
        if w < ripple_frame < a.ca.frames - w:
            if not a.pos.gapless[ripple_frame]:
                event_t.append(ripple_frame)
    return nonoverlap_from_list(a, w, event_t)

def sca_spikes(a, w):
    event_t = []
    for t in get_spikes(a):
        if w < t < a.ca.frames - w:
            if not a.pos.gapless[t]:
                event_t.append(t)
    return nonoverlap_from_list(a, w, event_t)


def CaEvents(a, w: int):
    event_t = numpy.load(a.path + a.prefix + '_CaEventList.npy')
    return nonoverlap_from_list(a, w, event_t)


def mask_from_list_nosession(w, event_list, trace_len, decay=6, eps=16, min_n=None,
                             clustloc='first', trace=None):
    '''return masks for a custom event list with no overlap allowed between events.
     decay: minimum gap between masks
     eps: replaces clusters with onset of firsts. retain all if 0.
     clustloc: 'first' - returns first member of a cluster, 'max': returns loc of max in the trace arg
     '''
    events = numpy.array(event_list, dtype=numpy.int64)
    if min_n is None:
        min_n = 2
        exc_single = False
    else:
        exc_single = True
    if eps:
        event_t = numpy.copy(events)
        if len(event_t) < min_n:
            events = event_t
        else:
            clustering = cluster.DBSCAN(eps=eps, min_samples=min_n).fit(event_t.reshape(-1, 1))
            single_event_indices = numpy.where(clustering.labels_ < 0)[0]
            any_clusters = clustering.labels_.max() > 0
            if exc_single:
                if any_clusters:
                    event_number = clustering.labels_.max() + (len(single_event_indices) > 0)
                    events = numpy.empty(event_number, dtype=numpy.int64)
                    for rci in range(clustering.labels_.max() + (
                            len(single_event_indices) > 0)):  # indices are different if there are zero noise events
                        current_cluster = numpy.where(clustering.labels_ == rci)[0]
                        rj = current_cluster[0]
                        events[rci] = event_t[rj]
                else:
                    events = []
            else:
                event_number = len(single_event_indices)
                if any_clusters:
                    event_number += clustering.labels_.max()
                events = numpy.empty(event_number, dtype=numpy.int64)
                ri, rci = 0, 0
                for ri, rj in enumerate(single_event_indices):
                    # try:
                    events[ri] = event_t[rj]
                    # except:
                    #     print(len(event_t), len(events), len(single_event_indices), clustering.labels_.max(), ri, rj)
                    #     assert False
                if any_clusters:
                    for rci in range(clustering.labels_.max() + (
                            len(single_event_indices) > 0)):  # indices are different if there are zero noise events
                        # get list of ripples in cluster
                        current_cluster = numpy.where(clustering.labels_ == rci)[0]
                        if clustloc == 'first':
                            rj = current_cluster[0]
                        elif clustloc == 'max':
                            rj = current_cluster[numpy.argmax(trace[event_t[current_cluster]])]
                        events[ri + rci] = event_t[rj]
    events.sort()
    # if exclude_move:
    #     events = events[~a.pos.gapless[events]]
    mask = numpy.zeros((len(events), 2 * w))
    mask[:] = numpy.nan
    # following algorithm taken and modified from single ripples
    for frame_index, current_frame in enumerate(events):
        # trim end to start of next
        if frame_index < len(events) - 1:
            last_frame = min(current_frame + w, max(events[frame_index + 1] - decay, current_frame + decay))
        else:
            last_frame = current_frame + w
        if frame_index > 0:
            first_frame = max(current_frame - w, min(events[frame_index - 1] + decay, current_frame - decay))
        else:
            first_frame = current_frame - w
        if first_frame > w and last_frame < trace_len - w:
            w0 = current_frame - first_frame
            w1 = last_frame - current_frame
            # set actual indices
            mask[frame_index, w - w0:w + w1] = numpy.arange(first_frame, last_frame)
    return events, mask
