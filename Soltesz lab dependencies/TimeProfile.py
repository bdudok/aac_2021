from LoadPolys import LoadImage
import numpy
from tifffile import imsave
import os


def TimeProfile(prefix, cfg, ret=False, channel=0):
    duration = (cfg['Start'], cfg['Stop'])
    line = cfg['Line']
    kernel = cfg['Kernel']
    width = 512
    a = LoadImage(prefix)
    im = numpy.empty((duration[1] - duration[0], width))
    sh = a.info['sz']
    c0 = int((sh[1] - width) / 2)
    c1 = c0 + width
    l0 = max(0, int(line - kernel / 2))
    l1 = min(sh[0], int(line + kernel / 2))
    for t in range(duration[1] - duration[0]):
        im[t] = a.data[t + duration[0], l0:l1, c0:c1, channel].mean(axis=0)
    im = 65535 - im
    if ret:
        return im
    else:
        im -= im.min()
        im /= numpy.percentile(im, 95)
        im = numpy.minimum(1, im)
        imsave(prefix + f'_linescan_x_{line}-{kernel}-ch{channel}.tif', (im * 255).astype('uint8'))


def GetNeuroPil(prefix, sig_only=True, intensity=False):
    fn = prefix + '.neuropil.npy'
    if os.path.exists(fn):
        y = numpy.load(fn)
    else:
        print('Pulling neuropil trace: ', prefix)
        y = PullNeuroPil(prefix)
    if intensity:
        return y[0]
    if sig_only:
        return y[3] * y[4] #NB this returns synchrony signal, not intensity
    else:
        return y


def GetNeuropilIntensity(prefix):
    y = GetNeuroPil(prefix, sig_only=False)
    return y[5]


def PullNeuroPil(prefix, margin=80):
    a = LoadImage(prefix, explicit_need_data=True)
    nframes = a.nframes
    nchannels = len(a.channels)
    # find cropping
    sh = a.info['sz']
    width = int((sh[1] - 2 * margin) * 0.9)
    kernel = int(sh[0] * 0.9)
    line = int(sh[0] * 0.5)
    c0 = int((sh[1] - width) / 2)
    c1 = c0 + width
    l0 = max(0, int(line - kernel / 2))
    l1 = min(sh[0], int(line + kernel / 2))
    # pull mean line profile
    im = numpy.empty((nframes, width, nchannels))
    for t in range(nframes):
        im[t] = a.data[t, l0:l1, c0:c1].mean(axis=0)
    m = im.mean(axis=1)
    # fit baseline with a simple smoothing
    smw = numpy.empty((nframes, nchannels))
    t1 = 50
    for t in range(nframes):
        ti0 = max(0, int(t - t1 * 0.5))
        ti1 = min(nframes, int(t + t1 * 0.5) + 1)
        smw[t] = numpy.mean(m[ti0:ti1], axis=0)
    # populate output array with: mean, median, IQR, diff
    y = numpy.empty((6, nframes, nchannels))
    y[0] = m - smw
    # sub baseline from image
    im -= smw[:, numpy.newaxis, :]
    # store median and spread
    s = numpy.sort(im, axis=1)
    y[1] = s[:, int(width / 2), :]
    y[2] = (s[:, int(width * 0.75), :] - s[:, int(width * 0.25), :])
    # create normalized differential of increasing median and decreasing spread
    def norm(d):
        return numpy.maximum(numpy.minimum(d / numpy.percentile(d, 99, axis=0), 1), 0)
    y[3:, 0, :] = 0
    y[3, 1:, :] = norm(numpy.diff(y[1], axis=0))
    y[4, 1:, :] = norm(-numpy.diff(y[2], axis=0))
    y[5] = 65535 - m
    numpy.save(prefix + '.neuropil', y)
    return y

if __name__ == '__main__':
    # os.chdir('//NEURO-GHWR8N2//AnalysisPC-Barna-2PBackup3//cckdlx//')
    # with open('_thclist.txt', 'r') as f:
    #     pflist = [s.strip() for s in f.readlines()]
    # for prefix in pflist:
    #     PullNeuroPil(prefix)

    os.chdir('//NEURO-GHWR8N2//AnalysisPC-Barna-2PBackup3//ihka//')
    prefix = 'ihka_099_100'
    y = PullNeuroPil(prefix)
    # margin=80
    # a = LoadImage(prefix)
    # nframes = 1000