import numpy
from sklearn.preprocessing import StandardScaler
from scipy import stats


def mad_based_outlier(points, thresh=3.5, return_mask=False):
    median = numpy.median(points)
    diff = (points - median) ** 2
    diff = numpy.sqrt(diff)
    med_abs_deviation = numpy.median(diff)
    modified_z_score = 0.6745 * diff / med_abs_deviation
    if return_mask:
        return modified_z_score < thresh
    return points[numpy.where(modified_z_score < thresh)]


def restrict_selection(df, column, criteria):
    return df.loc[df[column] == criteria]


def get_confidence_interval(points, alpha=0.95):
    return numpy.array(stats.t.interval(alpha, len(points) - 1, loc=numpy.mean(points), scale=stats.sem(points)))

def get_ci_errorbar(points, alpha):
    return numpy.array(stats.t.interval(alpha, len(points) - 1, loc=numpy.mean(points),
                                        scale=stats.sem(points))).reshape((2, 1)) - points.mean()


def get_parameter_standardized(df, column):
    return StandardScaler().fit_transform(df.loc[:, [column]].values)


def get_parameter_normalized(df, column, norm=True):
    y = df.loc[:, [column]].values
    y = y[numpy.where(numpy.logical_not(numpy.isnan(y)))]
    y = mad_based_outlier(y)
    if norm:
        return y / numpy.median(y)
    else:
        return y


def trim_ax(ca):
    ca.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
    ca.tick_params(axis='y', which='both', right='off', labelright='off')
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)
    ca.spines['bottom'].set_visible(False)


def get_fold_range(points, alpha=0.9):
    return numpy.percentile(points, alpha * 100) / numpy.percentile(points, (1 - alpha) * 100)

def get_range(points):
    return points.max() - points.min()

def get_IQR_err(points, alpha=0.25):
    return numpy.array([numpy.percentile(points, alpha * 100),
                        numpy.percentile(points, (1 - alpha) * 100)]).reshape((2, 1))

def plot_cv(ca, y, ytitle, width):
    #create x based on percentile
    y = numpy.sort(y)
    l = len(y)
    x = numpy.empty(l)
    for i in range(l):
        w = width - abs(i / l - width)
        x[i] = numpy.random.random() * w
        if numpy.random.random() > 0.5:
            x[i] *= -1
    ca.boxplot(y, showfliers=False, whis=1)
    # ca.scatter(x+1, y, s=15)
