import numpy
from LoadPolys import LoadImage, load_roi_file
import matplotlib.path as mplpath
import os
from multiprocessing import Process
from time import time
import datetime

class Worker(Process):
    def __init__(self, queue):
        super(Worker, self).__init__()
        self.queue = queue

    def run(self):
        for job in iter(self.queue.get, None):
            path, prefix, roi, ch = job
            os.chdir(path)
            if roi == 'Auto':
                #locate last saved roi file
                exs = [1]
                for f in os.listdir():
                    if prefix in f and '_saved_roi_' in f:
                        try:
                            if f[-6:-4] != '-1':
                                exs.append(int(f[:-4].split('_')[-1]))
                        except:
                            pass
                tag = str(max(exs))
            else:
                tag = roi
            pull_signals(prefix, tag=tag, ch=ch)


def pull_signals(prefix, tag=None, ch='All', raw=False):
    #get binary mask
    if tag is None:
        mask_name = f'{prefix}_nonrigid.masks.npy'
    else:
        mask_name = f'{prefix}_mask_{tag}.npy'
        roi_name = f'{prefix}_saved_roi_{tag}.npy'
    if not os.path.exists(roi_name):
        print(prefix, ': Roi file not found: ', tag)
        return -1
    im = LoadImage(prefix, explicit_need_data=True, raw=raw)
    if os.path.exists(mask_name):
        binmask = numpy.load(mask_name)
    else:
        data = load_roi_file(roi_name)
        #calculate binary mask
        height, width = im.info['sz']
        binmask = numpy.zeros((len(data), width, height), dtype='bool')
        print(f'Computing masks from {len(data)} rois...')
        for nroi, pr in enumerate(data):
            roi = numpy.empty((2, len(pr)), dtype='int')
            for j in range(len(pr)):
                roi[:, j] = pr[j]
            left, top = roi.min(axis=1)
            right, bottom = roi.max(axis=1)
            # load poly for pip function
            poly = mplpath.Path(pr)
            for x in range(left, right):
                for y in range(top, bottom):
                    if poly.contains_point([x, y]):
                        binmask[nroi, x, y] = True
        # numpy.save(mask_name, binmask) # turned this off, no point in storing those files
    #figure out channels to pull MODES = ['All', 'First', 'Second']
    if ch == 'First' or ch == 0:
        channels = [0]
    elif ch == 'Second' or ch == 1:
        channels = [1]
    else:
        channels = im.channels
    #init empty array
    nframes = len(im.data)
    ncells = len(binmask)
    nchannels = len(channels)
    traces = numpy.empty((ncells, nframes, nchannels))
    # print('Reading data from disk to memory...')
    # im.force_read()  # miniscope compatibility and improves performance if low on mem. moved to default force chunk read
    print(f'Pulling {int(nframes*ncells*nchannels)} signals ({ncells} regions, {nchannels} channels) from {prefix} roi {tag}...')
    # create chunks so that the same array is combed for each cell before moving on
    t0 = datetime.datetime.now()
    chunk_len = 500
    indices = []
    for c in range(ncells):
        indices.append(binmask[c].nonzero())
    rep_size = 0.20
    next_report = rep_size
    for t in range(int(nframes/chunk_len) + 1):
        start = int(t * chunk_len)
        if start / nframes > next_report:
            elapsed = datetime.datetime.now() - t0
            speed = (elapsed / ncells / len(channels) / start).microseconds
            print(f'Pulling {prefix}:{int(next_report*100):2d}% ({speed} microseconds / cell / frame)')
            next_report += rep_size
        stop = int(min(nframes, start + chunk_len))
        inmem_data = numpy.array(im.data[start:stop])
        for c in range(ncells):
            ind = indices[c]
            if len(channels) > 1:
                traces[c, start:stop, :] = inmem_data[:, ind[1], ind[0], :].mean(axis=1)
            else:
                traces[c, start:stop, 0] = inmem_data[:, ind[1], ind[0], channels[0]].mean(axis=1)


    # numpy.save(f'{prefix}_trace_{tag}_ch' + ''.join([str(x) for x in channels]), 65535-traces)
    if not im.hdf_source:
        numpy.save(f'{prefix}_trace_{tag}', 65535-traces)
    else:
        numpy.save(f'{prefix}_trace_{tag}', traces)
    elapsed = datetime.datetime.now() - t0
    minutes = datetime.timedelta.total_seconds(elapsed) / 60
    speed = (elapsed / ncells / len(channels) / nframes).microseconds
    print(f'{prefix} finished in {minutes:.1f} minutes ({speed} microseconds / cell / frame)')

if __name__ == '__main__':
    os.chdir('G://Barna//axax//')
    prefix = 'axax_124_230'
    # t0 = time()
    # PullSignals(prefix, '1', 0)
    # print('Ch 0 Finished in', time()-t0)
    t0 = time()
    pull_signals(prefix, '2', 1)
    print('Ch 1 Finished in', time()-t0)

