import ImportFirst
from ImagingSession import ImagingSession
from Scores import Scores
from matplotlib import pyplot as plt
from TimeProfile import GetNeuroPil
import os
import numpy
from scipy.signal import decimate, resample
import pandas


def plot_overlay(path, prefix, channels=(1, 1), cells=None, tick_step=60, upfactor=25, fps=15.4):
    # path = 'X:\Exclude\HFOs'
    # prefix = 'JF_152_016'
    # tick_step = 60  # x label spacing in seconds
    # upfactor = 25 # upsampling of Ca trace for plotting / # also determines resampling of LFP. 25 equals to ~400 Hz LFP
    # fps = 15.4

    os.chdir(path)
    a = ImagingSession(prefix, tag='skip', ephys_channels=channels)
    neuropil_signals = GetNeuroPil(prefix, sig_only=False)
    neuropil_intensity = neuropil_signals[5]
    neuropil_intensity[0] = neuropil_intensity[1]

    # resample trace for high res lfp overlay
    n_frames, n_channels = neuropil_signals.shape[1:]
    lens = int(n_frames * upfactor)
    s = numpy.copy(a.ripples.trace)
    factor = len(s) / lens
    while factor > 2:
        q = min(factor, 10)
        s = decimate(s, int(q))
        factor = len(s) / lens
    s = resample(s, lens)

    # approximate ephys sample to frame with a linear fit (x units: frame * upfactor).
    sx = numpy.empty(lens)
    factor = len(a.ephysframes) / lens
    first_sync = int(numpy.argmax(a.ephysframes > 0) / factor)
    last_sync = int(numpy.argmax(a.ephysframes > n_frames - 1) / factor)
    sx[first_sync:last_sync] = numpy.linspace(upfactor, upfactor * (n_frames - 1), last_sync - first_sync)
    sx[:first_sync] = numpy.linspace(0, upfactor, first_sync)
    sx[last_sync:] = numpy.linspace(upfactor * (n_frames - 1), upfactor * n_frames, lens - last_sync)

    seconds = numpy.arange(0, n_frames / fps, tick_step)
    xtk = seconds * fps * upfactor
    xvals = (seconds / 60).astype('int')
    fx = numpy.arange(n_frames) * upfactor

    fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True)

    # get events
    # load event times
    efn = prefix + '.csv'
    if os.path.exists(efn):
        have_events=True
        epi_event_types = ('IIS', 'HFO', 'SzStart', 'SzStop')
        epi_event_colors = ('red', 'green', 'blue')
        event_line_w = (1, 1)
        time_keys = ('IIS_Time', 'HFO_Time', 'Start', 'End')
        overlap_key = 'HFO_Overlap'
        event_data = pandas.read_csv(efn)
        epi_events = [event_data[key].loc[event_data[key].notna()].values for key in time_keys]

        # frames from events:
        ephys_fs = a.ripples.fs
        frame_dict = {}
        for event_type, event_fs in zip(epi_event_types, epi_events):
            event_frames = [a.sampletoframe(x) for x in (event_fs * ephys_fs).astype('int64')]
            frame_dict[event_type] = numpy.array(event_frames)

        # clean IIS
        IIS_frames = frame_dict['IIS']#[numpy.where(event_data[overlap_key].values == False)]

        # HFO
        HFO_frames = frame_dict['HFO']

        # Sz
        Sz_durations = [(a.sampletoframe(int(x * ephys_fs)), a.sampletoframe(int(y * ephys_fs)))
                        for x, y in zip(epi_events[2], epi_events[3])]

        # vlines for events, bars for seizures
        ylims = ((-2, 2), (0,1), (0, 1))
        for ca, ylim in zip(ax, ylims):
            for dur in Sz_durations:
                ca.fill_between(fx[slice(*dur)], *ylim, color=epi_event_colors[2], alpha=0.5)
            for frames, color, line_w in zip((HFO_frames, IIS_frames), [epi_event_colors[x] for x in [1, 0]],
                                             event_line_w):
                for f in frames:
                    ca.axvline(f * upfactor, color=color, alpha=0.5, linewidth=line_w)
    else:
        have_events = False
        if hasattr(a, 'ripple_frames'):
            color = 'orange'
            for ca in ax:
                for f in a.ripple_frames:
                    ca.axvline(f * upfactor, color=color, alpha=0.6)

    # LFP channel 1
    ca = ax[0]
    ca.plot(sx, s, color='black', alpha=0.8, label='LFP (V)')
    ca.set_ylabel('LFP (V)')

    ca.set_ylim(-2, 2)

    # neuropil mean
    ca = ax[1]
    ca.set_title('Neuropil intensity')
    ca.set_ylabel('Fluorescence')
    channels = ('green', 'red')
    for ch, color in enumerate(channels[:n_channels]):
        y = neuropil_intensity[:, ch]
        y = y - y.min()
        ca.plot(fx, y / numpy.percentile(y, 99), color=color, alpha=0.8, label=f'neuropil {color}')

    # speed
    ca = ax[2]
    ca.plot(fx, a.pos.smspd / a.pos.smspd.max(), color='magenta', alpha=0.8, label='speed')
    ca.set_ylim(0, 1)

    ca.set_xlabel('Time (min)')
    for ca in ax:
        ca.legend(loc='upper left')
        ca.set_xticks(xtk)
        ca.set_xticklabels(xvals)

    plt.show()
    # plt.tight_layout()

def plot_overlay_qa(path, prefix, tag, channels=(1,1), tick_step=10, upfactor=25, fps=15.4):
    # path = 'X:\Exclude\HFOs'
    # prefix = 'JF_152_016'
    # tick_step = 60  # x label spacing in seconds
    # upfactor = 25 # upsampling of Ca trace for plotting / # also determines resampling of LFP. 25 equals to ~400 Hz LFP
    # fps = 15.4

    os.chdir(path)
    a = Scores(prefix, tag=tag, ephys_channels=channels)

    # resample trace for high res lfp overlay
    n_frames, n_channels = a.ca.frames, a.ca.channels
    lens = int(n_frames * upfactor)
    s = numpy.copy(a.ripples.trace)
    factor = len(s) / lens
    while factor > 2:
        q = min(factor, 10)
        s = decimate(s, int(q))
        factor = len(s) / lens
    s = resample(s, lens)

    # approximate ephys sample to frame with a linear fit (x units: frame * upfactor).
    sx = numpy.empty(lens)
    factor = len(a.ephysframes) / lens
    first_sync = int(numpy.argmax(a.ephysframes > 0) / factor)
    last_sync = int(numpy.argmax(a.ephysframes > n_frames - 1) / factor)
    sx[first_sync:last_sync] = numpy.linspace(upfactor, upfactor * (n_frames - 1), last_sync - first_sync)
    sx[:first_sync] = numpy.linspace(0, upfactor, first_sync)
    sx[last_sync:] = numpy.linspace(upfactor * (n_frames - 1), upfactor * n_frames, lens - last_sync)

    seconds = numpy.arange(0, n_frames / fps, tick_step)
    xtk = seconds * fps * upfactor
    xvals = seconds.astype('int')
    fx = numpy.arange(n_frames) * upfactor

    fig, ax = plt.subplots(nrows=4, ncols=1, sharex=True)



    if hasattr(a, 'ripple_frames'):
        color = 'black'
        for ca in ax:
            for f in a.ripple_frames:
                ca.axvline(f * upfactor, color=color, alpha=0.6)

    # LFP channel 1
    ca = ax[0]
    ca.plot(sx, s, color='black', alpha=0.8, label='LFP (V)')
    ca.set_ylabel('LFP (V)')

    ca.set_ylim(-2, 2)

    # neuropil mean
    ca = ax[1]
    # ca.set_title('Neuropil intensity')
    # ca.set_ylabel('Fluorescence')
    channels = ('green', 'red')
    for ch, color in enumerate(channels[:n_channels]):
        y = numpy.nanmean(a.ca.rel, axis=0)
        y = y - y.min()
        ca.plot(fx, y, color=color, alpha=0.8, label=f'DF/F')

    # sync
    ca = ax[2]
    p_a = a.sync_events(trace_only=True)
    ca.plot(fx, p_a, color='blue', alpha=0.8, label='Fraction active')

    ca = ax[3]
    ca.plot(fx, a.pos.smspd / a.pos.smspd.max(), color='magenta', alpha=0.8, label='speed')
    ca.set_ylim(0, 1)

    ca.set_xlabel('Time (s)')
    for ca in ax:
        ca.legend(loc='upper left')
        ca.set_xticks(xtk)
        ca.set_xticklabels(xvals)

    plt.show()
    # plt.tight_layout()
