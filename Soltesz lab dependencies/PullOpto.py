from LoadPolys import LoadImage
import numpy
from Batch_Utils import gapless
import os

# use raw sbx to pull the max 10 px from the of first lines where led is on to detect opto periods
def pullopto(prefix, matpath=''):
    optoname = matpath + prefix + '_opto.npy'
    if not os.path.exists(optoname):
        im = LoadImage(prefix, raw=True, matpath=matpath, explicit_need_data=True)
        line = 18  # that's whete the led light is first detected, before gating kicks in
        kernel = 8  # 8 line ~ 1 ms with unidirectional - shortest pulse setting
        width = 20  # using first 20 columns - this is during laser is blanked
        trace = 65535 - im.data[:, line:line + kernel, :width, 0].mean(axis=(1, 2))
        thr = trace.max() / 2
        trace = trace > thr
        # some experiments have light in every other frame during stim. saveopto as gapless for mc and processing, keep
        # a copy of original for analysis.
        gapless_trace = gapless(trace, 5)
        if numpy.count_nonzero(gapless_trace) > numpy.count_nonzero(trace):
            numpy.save(optoname, gapless_trace)
            numpy.save(optoname.replace('.npy', '.orig.npy'), trace)
        else:
            numpy.save(optoname, trace)

if __name__ == '__main__':
    from matplotlib import pyplot as plt
    import os

    fpath = 'G://Barna//pvchr//'
    rpath = 'J://raw_sbx//'
    os.chdir(rpath)
    pflist = ['pvchr_105_412', 'pvchr_106_412', 'pvchr_106_414', 'pvchr_107_412', 'pvchr_107_414', 'pvchr_108_412', 'pvchr_108_414']
    for prefix in pflist:
        if not os.path.exists(fpath + prefix + '_opto.npy'):
            pullopto(prefix, fpath)

# # see what it looks like
# a = LoadImage(prefix)
# duration = (6000, 8000)
# width = 20
# im = numpy.empty((duration[1] - duration[0], width))
# sh = a.info['sz']
# c0 = 0
# c1 = width
# l0 = 0
# l1 = 256
# for t in range(duration[1] - duration[0]):
#     im[t] = a.data[t + duration[0], l0:l1, c0:c1, 0].mean(axis=0)
