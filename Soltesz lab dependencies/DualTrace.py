'''A wrapper for Firing to load both channels and pack in a single array with an extra last dimentsion.
Add specific functions to allow filtering roi by channel.
Will add DG/R functions later'''

from Firing import Firing
import os
import numpy


class Dual(Firing):
    def __init__(self, prefix, tag=None, ch=0):
        super().__init__(prefix, ch=ch, tag=tag)

    def concatenate(self, second_tag, ch=None):
        '''concatenates two Firing objects so that cells in the first are followed by cells in the second.
        All channels are passed, missing data is filled with nans
        '''
        self.load()
        a = self
        if ch is None:
            ch = a.ch
        b = Firing(self.prefix, tag=second_tag, ch=ch)
        b.load()
        n_cells = a.cells + b.cells
        frames = a.frames
        n_channels = max(a.channels, b.channels)
        if n_channels > 1:
            shape = (n_cells, frames, n_channels)
            chstr = 'dual'
        else:
            shape = (n_cells, frames)
            if ch in [0, 'First']:
                chstr = 'ch0'
            elif ch in [1, 'Second']:
                chstr = 'ch1'
            else:
                chstr = 'ch'+str(ch)
        self.dualpf = self.prefix + f'-{self.tag}+{second_tag}-{chstr}.np'
        os.mkdir(self.dualpf)
        secondary_keys = ['rate']
        for key in a.keys:
            if hasattr(a, key):
                if key in secondary_keys:
                    continue
                arr_in = a.__getattribute__(key)
                arr_out = numpy.empty((*shape,), dtype=arr_in.dtype)
                arr_out[:a.cells, ...] = arr_in
                arr_out[a.cells:, ...] = b.__getattribute__(key)
                if n_channels > 1:
                    if a.channels < n_channels:
                        arr_out[:a.cells, :, 1] = numpy.nan
                    if b.channels < n_channels:
                        arr_out[a.cells:, :, 1] = numpy.nan
                numpy.save(self.dualpf + '//' + key, arr_out)
        # self.version_info = a.version_info
        numpy.save(self.dualpf + '//' + 'info', self.version_info)
        print(self.dualpf, n_cells, 'cells merged.')

    def merge(self, second_tag=None, ch=1):
        '''merges two Firing objects of the same shape (same index for same roi) in different channels'''
        if second_tag is None:
            second_tag = self.tag
            self.dualpf = self.prefix + f'-{self.tag}-dual.np'
        else:
            tag = second_tag
            self.dualpf = self.prefix + f'-{self.tag}+{second_tag}-dual.np'
        a = self
        a.load()
        b = Firing(self.prefix, tag=second_tag, ch=ch)
        b.load()
        # init arrays, and combine
        if not os.path.exists(self.dualpf):
            os.mkdir(self.dualpf)
        for key in a.keys:
            if hasattr(a, key):
                arr_in = a.__getattribute__(key)
                arr_out = numpy.empty((*arr_in.shape, 2), dtype=arr_in.dtype)
                arr_out[..., 0] = arr_in
                arr_out[..., 1] = b.__getattribute__(key)
                numpy.save(self.dualpf + '//' + key, arr_out)
        # self.version_info = a.version_info
        numpy.save(self.dualpf + '//' + 'info', self.version_info)
        print(self.prefix, a.cells, 'cells merged.')


if __name__ == '__main__':
    os.chdir('G:\Barna\ihka//')
    prefix = 'JF_104_667'
    d = Dual(prefix)
    d.merge()
