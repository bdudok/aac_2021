import ImportFirst
from matplotlib import pyplot as plt
import cv2
import numpy
from skimage import filters
import os
import pandas
from scipy import stats
from ImagingSession import F, ImagingSession
from EventMasks import all_ripples_immobility


class Scores(ImagingSession):
    def stoprun_scores(self, param='ntr', image_too=False, ret_loc='actual', mode='mean', loc_input=None,
                       output_filename=None):
        param = self.getparam(param)
        # collect stop events. Criteria: last speed peak of 50 long run event followed by 150 gap.
        if loc_input is None:
            starts, stops = self.startstop(ret_loc=ret_loc)
        else:
            starts, stops = loc_input
        stoprun_scores = numpy.zeros((self.ca.cells, 2))
        if image_too:
            im = self.rois.image.data
            stoprun_image = numpy.zeros(self.rois.image.info['sz'])
        for ti in range(len(stops)):
            start, stop = starts[ti], stops[ti]
            # calculate stop and run response score:
            if mode == 'mean':
                stoprun_scores[:, 0] += numpy.nanmean(param[:, stop:min(self.ca.frames, stop + 100)], axis=1) - \
                                        numpy.nanmean(param[:, start:stop], axis=1)
                stoprun_scores[:, 1] += numpy.nanmean(param[:, start:stop], axis=1) - \
                                        numpy.nanmean(param[:, max(0, start - 100):start], axis=1)
            if mode == 'max':
                stoprun_scores[:, 0] += numpy.nanmax(param[:, stop:min(self.ca.frames, stop + 100)], axis=1) - \
                                        numpy.nanmax(param[:, start:stop], axis=1)
                stoprun_scores[:, 1] += numpy.nanmax(param[:, start:stop], axis=1) - \
                                        numpy.nanmax(param[:, max(0, start - 100):start], axis=1)
            if image_too:
                # generate images of score
                stoprun_image[:, :] += im[stop:min(self.ca.frames, stop + 100), :, :, 0].mean(axis=0) - \
                                       im[start:stop, :, :, 0].mean(axis=0)
        stoprun_scores /= len(stops)
        if image_too:
            # positive values as red, negs as green
            stoprun_rgb = numpy.zeros((*stoprun_image.shape, 3), dtype='uint8')
            neg = numpy.zeros(stoprun_image.shape)
            nw = numpy.where(stoprun_image < 0)
            neg[nw] -= stoprun_image[nw]
            pos = numpy.zeros(stoprun_image.shape)
            pw = numpy.where(stoprun_image > 0)
            pos[pw] += stoprun_image[pw]
            r = max(numpy.percentile(neg, 99), numpy.percentile(pos, 99))
            stoprun_rgb[:, :, 2] = numpy.minimum(neg / r, 1) * 255
            stoprun_rgb[:, :, 1] = numpy.minimum(pos / r, 1) * 255
            if output_filename is None:
                output_filename = 'plots//' + self.prefix + '_StopActivity.tif'
            cv2.imwrite(output_filename, stoprun_rgb)
        return stoprun_scores

    def ramping_scores(self, param='ntr', ret_loc='actual', use_stored=True):
        if any([not use_stored, not hasattr(self, 'stored_moves')]):
            self.startstop(ret_loc=ret_loc)
        starts, stops = self.stored_moves
        rs = numpy.empty((self.ca.cells, len(starts)))
        param = self.getparam(param)
        # get trace for each event
        for c in range(self.ca.cells):
            for e, (start, stop) in enumerate(zip(starts, stops)):
                y = param[c, start:stop]
                # linear regression to filter if there's an incr
                r = stats.linregress(numpy.arange(stop - start), y)
                if r[3] < 0.0001 and r[0] > 0:
                    # z scored increase as output score
                    m = int(len(y) / 2)
                    rs[c, e] = numpy.nanmean(y[m:]) - numpy.nanmean(y[:m])
                else:
                    rs[c, e] = 0
        return rs

    def startstop(self, span=None, duration=50, gap=150, ret_loc='actual'):
        if not hasattr(self, 'gapless'):
            self.gapless_move()
        starts, stops = self.pos.startstop(duration, gap, ret_loc, span)
        self.stored_moves = starts, stops
        return starts, stops

    def gapless_move(self, n=15, no_overwrite=False):
        self.pos.gapless = numpy.copy(self.pos.movement)
        ready = False
        while not ready:
            ready = True
            for t, m in enumerate(self.pos.gapless):
                if not m:
                    if numpy.any(self.pos.gapless[t - n:t]) and numpy.any(self.pos.gapless[t:t + n]):
                        self.pos.gapless[t] = 1
                        ready = False
        if not no_overwrite:
            self.pos.movement = self.pos.gapless[:self.ca.frames]

    def ripple_responses(self, frames, cells=None, window=((-10, -1), (0, 3))):
        # if cells is list of list, works for pulling means of cell groups
        # window is specified as pre start:stop, post
        if cells is None:
            cells = range(self.ca.cells)
        # pull responses
        resp = numpy.zeros((len(cells), len(frames)))
        for ci, c in enumerate(cells):
            for ri, fr in enumerate(frames):
                pre, post = self.ca.ntr[c, fr + window[0][0]:fr + window[0][1]].mean(), \
                            self.ca.ntr[c, fr + window[1][0]:fr + window[1][1]].mean()
                resp[ci, ri] = post - pre
        return resp

    def ripple_score(self, frames, window=((-20, -1), (0, 2)), param_key='ntr'):
        param = self.getparam(param_key)
        cells = range(self.ca.cells)
        # pull responses
        resp = []
        for c in cells:
            x = 0
            for fr in frames:
                x -= param[c, fr + window[0][0]:fr + window[0][1]].mean()
                x += param[c, fr + window[1][0]:fr + window[1][1]].mean()
            resp.append(x / len(frames))
        return resp

    def get_ripple_scores(self):
        failed = True
        if hasattr(self, 'ripples'):
            if len(self.ripples.events) > 2:
                failed = False
                me = [self.sampletoframe(x.p[0]) for x in self.ripples.events]
                ripp_score = self.ripple_score(me)
        if failed:
            ripp_score = [numpy.nan] * self.ca.cells
        return ripp_score

    def get_immo_ripples(self):
        ripples = []
        if hasattr(self, 'ripples'):
            for t in self.ripples.events:
                t = self.sampletoframe(t.p[0])
                if 100 < t < self.ca.frames - 100:
                    if not self.pos.gapless[t]:
                        ripples.append(t)
        return numpy.array(ripples)

    def build_laps(self):
        laps = []
        t0 = 100
        while self.pos.laps[t0] < self.pos.laps[-1]:
            for t1 in range(t0, len(self.pos.relpos)):
                if self.pos.relpos[t1] > self.pos.relpos[t0] and self.pos.laps[t1] > self.pos.laps[t0]:
                    laps.append([t0, t1])
                    break
            t0 = t1
        return laps

    def getloc(self, lap, pos):
        # find an almost full lap around the location and return the bounds with distance weights
        if lap == 0:
            if self.pos.relpos[8] >= pos - 0.45:
                t0 = 8
            else:
                t0 = numpy.where(self.pos.relpos < pos - 0.45)[0][-1]
        elif pos >= 0.5:
            t0 = numpy.where(numpy.array([lap - self.pos.laps == 0, self.pos.relpos <= pos - 0.45]).all(axis=0))[0][-1]
        else:
            a = numpy.where(numpy.array([lap - self.pos.laps == 1, self.pos.relpos < pos + 0.45]).all(axis=0))[0]
            if len(a) > 0:
                t0 = a[-1]
            else:
                t0 = 8
        if lap == self.pos.laps[-1]:
            if self.pos.relpos[-8] <= pos + 0.45:
                t1 = len(self.pos.relpos) - 8
            else:
                t1 = numpy.where(self.pos.relpos < pos + 0.45)[0][-1]
        elif pos >= 0.5:
            a = numpy.where(numpy.array([lap - self.pos.laps == -1, self.pos.relpos < pos - 0.45]).all(axis=0))[0]
            if len(a) > 0:
                t1 = a[-1]
            else:
                t1 = len(self.pos.relpos) - 8
        else:
            t1 = numpy.where(numpy.array([lap - self.pos.laps == 0, self.pos.relpos < pos + 0.45]).all(axis=0))[0][-1]

        if numpy.count_nonzero(self.pos.movement[t0:t1]) < 100:
            return t0, t1, -1
        weights = numpy.zeros(t1 - t0)
        for t in range(t1 - t0):
            if self.pos.movement[t + t0]:
                d = 1 - self.reldist(pos, self.pos.relpos[t + t0])
                weights[t] = numpy.e ** (1 - 1 / (d ** 2))

        return t0, t1, weights

    def placefield_score(self, bins=50, textout=True, param='spikes'):
        if not hasattr(self, 'gapless'):
            self.gapless_move()
        spikes = self.getparam(param)
        self.placefields_smooth(param=spikes, bins=bins, silent=True)
        self.calc_MI(param=spikes, selection='movement')
        # scores table: 0: MI, 1:max z score, 2:location, 3:persistence, 4:mean distance from peak, 5:number of laps with loc included, 6:number of laps cell fired
        table_head = 'Prefix,Cell,Mutual Information,Maximal z score,Location,Persistence,Spike distance from location,Number of laps visited,Number of laps with spiking'
        scores = numpy.zeros((7, self.ca.cells))
        scores[0, :] = self.mi
        scores[1, :] = self.smooth_zscores.max(axis=0)
        scores[2, :] = numpy.argmax(self.smooth_zscores, axis=0)
        scores[2] /= bins
        for c in range(self.ca.cells):
            # selecting laps that include the location of the cell and spikes
            pos = scores[2, c]
            mov_spikes = spikes[c] * self.pos.movement[:self.ca.frames]
            visited_laps = numpy.unique(self.pos.laps[numpy.where(numpy.absolute(self.pos.relpos * bins - pos) < 0.2)])
            if all([numpy.any(spikes[c]), self.mi[c] > 1]):
                scores[5, c] = len(visited_laps)
                active_laps = numpy.unique(self.pos.laps[numpy.nonzero(mov_spikes)])
                laps = []
                dists = []
                dw = []
                for lap in active_laps:
                    if lap in visited_laps:
                        laps.append(lap)
                scores[6, c] = len(laps)  # this has been changed from len(active_laps)
                if len(laps) > 1:
                    for lap in laps:
                        # calculate if the cell fired preferentially near the location in this lap
                        t0, t1, weights = self.getloc(lap, pos)
                        if weights is -1:  # this is is called if lap is last and too short. 1 is deducted from nlaps
                            scores[[5, 6], c] -= 1
                            continue
                        wh = numpy.where(self.pos.movement[t0:t1])
                        trace = mov_spikes[t0:t1]
                        avg_rate = trace[wh].mean()
                        if numpy.sum(weights) > 0:
                            weighted_rate = numpy.average(trace, weights=weights)
                        else:
                            print('Rate weights were zero. Check how this is possible.', self.prefix, 'lap', lap, 'pos',
                                  pos, ', frames:', len(wh))
                            weighted_rate = 0
                        if weighted_rate > avg_rate:
                            scores[3, c] += 1
                        firing = numpy.nonzero(trace)[0]
                        # caluculate the distance of each spike from the location, weighted by amplitude
                        for t in firing:
                            dists.append(self.reldist(pos, self.pos.relpos[t0 + t]))
                        dw.extend(trace[firing])
                if len(dists) > 0:
                    scores[4, c] = numpy.average(dists, weights=dw)
                else:
                    scores[4, c] = numpy.nan
            else:
                scores[[3, 4, 5,
                        6], c] = numpy.nan  # if the cell fired and had mi, these will be calculated (default zero)
        if textout:
            path = self.opath
            if path == '.':
                path = ''
            of = F(path, self.prefix + '_placefield_scores', table_head)
            for c in range(self.ca.cells):
                if numpy.any(spikes[c]):  # only writing output for cells with spike
                    op = [self.prefix, c]
                    for x in scores[:, c]:
                        if numpy.isnan(x):
                            x = ''
                        op.append(x)
                    of.w(op)
            of.cl()
        else:
            return scores

    def get_eventarray(self, cells, force=False):
        fn = f'{self.prefix}.np//eventarray.npy'
        if os.path.exists(fn) and not force:
            eventarray = numpy.load(fn)
            for x in ['peaks', 'b2']:
                self.rates[x] = numpy.load(f'{self.prefix}.np//event_{x}.npy')
            print(f'Loaded {eventarray.shape[0]} events from file')
        else:
            events = []
            lengths = 0
            for c in cells:
                es = self.peakdetection(c)
                events.append(es)
                lengths += len(es)
            # make an array of c, onset, start, peak, stop, decay from the events
            eventarray = numpy.empty((lengths, 6), dtype='int')
            i = 0
            for ci, c in enumerate(cells):
                for e in events[ci]:
                    eventarray[i, 0] = c
                    eventarray[i, 1:] = e
                    i += 1
            numpy.save(fn, eventarray)
            for x in ['peaks', 'b2']:
                numpy.save(f'{self.prefix}.np//event_{x}.npy', self.rates[x])
            print(f'Event array saved, {lengths} events')
        return eventarray

    def peakdetection(self, c, param='smtr', shortwindow=50, longwindow=300, gap=5, duration=10):
        # peak det part descriptors:
        # 1: 5 frame change
        s = self.getparam(param)[c, :]
        p1 = numpy.zeros(len(s))
        p1[5:] = s[5:] - s[:-5]
        p1 = numpy.maximum(p1, 0)
        # sliding positive auc minus negative auc
        l3 = numpy.zeros(self.ca.frames)
        for t in range(self.ca.frames - longwindow):
            t = int(t + longwindow / 2)
            # positive auc in 50 over 20 percentile in 300: nice for local, not sensitive to bsl loc
            l3[t] = numpy.maximum(0, s[t:t + shortwindow] -
                                  numpy.percentile(s[int(t - longwindow / 2):int(t + longwindow / 2)],
                                                   20)).sum() / shortwindow
        b1 = numpy.logical_and(p1, l3 > 1)  # b1 looks good for peaks and excludes decays nicely
        # fill gaps in b1 to allow burst det later
        gapless = numpy.copy(b1)
        ready = False
        while not ready:
            ready = True
            for t, m in enumerate(gapless):
                if not m:
                    if numpy.any(gapless[t - gap:t]) and numpy.any(gapless[t:t + gap]):
                        gapless[t] = 1
                        ready = False
        # chop off sections with decrease to split overlapping peaks. use inverse, fast, centered variant of l3
        flong, fshort = 90, 15
        l3if = numpy.zeros(self.ca.frames)
        for t in range(self.ca.frames - flong):
            t = int(t + flong / 2)
            l3if[t] = numpy.maximum(0, -s[int(t - fshort / 2):int(t + fshort / 2)] -
                                    numpy.percentile(-s[int(t - flong / 2):int(t + flong / 2)], 50)).sum() / fshort
        b2 = numpy.logical_and(gapless, l3if < s)
        # detect individual peaks
        events = []
        t = duration
        offset = int(t + shortwindow / 2)
        endpoint = self.ca.frames - 8
        # store peaks as a new param for plotting later
        for x in ['peaks', 'b2']:
            if x not in self.rates:
                self.rates[x] = numpy.zeros((self.ca.cells, self.ca.frames))
        while t < endpoint - gap:
            # find next point which has at least a duration after
            if numpy.all(b2[t:t + duration]) and t < endpoint - duration:
                start = t
                # go ahead while signal is over threshold
                t1 = t + duration
                while b2[t1] and t1 < endpoint:
                    t1 += 1
                stop = t1
                peak = start + numpy.argmax(s[start:stop])
                # expand while l3 is in decay
                if t1 < endpoint - offset:
                    while l3[t1] > l3[t1 + offset] and t1 < endpoint - offset:
                        t1 += 1
                decay = t1
                # to find onset, go back until l3 is in decline, unless we hit the previous event
                t0 = t
                while l3[t0] > l3[t0 - offset] and not numpy.all(b2[t0 - duration: t0]) and t0 > duration:
                    t0 -= 1
                # then find highest increase until peak
                if peak - t0 < 2:
                    t0 -= 5
                onset = t0 + numpy.argmax(p1[t0:peak]) - 1
                events.append([onset, start, peak, stop, decay])
                self.rates['b2'][c] = b2
                self.rates['peaks'][c, peak] = 1
                t = stop + gap
            t += 1
        return events

    def ripple_order(self, cells=None, frames=None):
        window = 10
        if cells is None:
            cells = range(self.ca.cells)
        if frames is None:
            frames = []
            for x in self.ripples.events:
                t = self.sampletoframe(x.p[0])
                if 100 < t < self.ca.frames - 100:
                    frames.append(t)
        # pull responses
        resps = self.ripple_responses(frames=frames, cells=cells)
        # get delays in positive resp events
        delays = numpy.zeros((len(cells), len(frames)))
        traces = numpy.zeros((window * 2, len(cells), len(frames)))
        for ci, c in enumerate(cells):
            # 1 frame change trace
            s = self.ca.trace[c, :]
            p1 = numpy.zeros(len(s))
            p1[1:] = s[1:] - s[:-1]
            p1 = numpy.maximum(p1, 0)
            for ti, t in enumerate(frames):
                traces[:, ci, ti] = self.ca.ntr[c, t - window:t + window]
                if resps[ci, ti] > 0:
                    delays[ci, ti] = numpy.argmax(p1[t - window:t + window] + window)
                else:
                    delays[ci, ti] = numpy.nan
        return delays, traces

    def ripple_triggered_nonoverlap_mean(self, param_key, range_secs, save=False):
        fps = 15.6
        w = int(range_secs * fps)
        events, mask = all_ripples_immobility(self, w)
        param = self.getparam(param_key)
        data = numpy.empty((len(events) * self.ca.cells, 2 * w + 2))
        data_counter = 0
        for frame, indices in zip(events, mask):
            lines = numpy.empty((self.ca.cells, 2 * w))
            lines[:] = numpy.nan
            loc = numpy.where(numpy.logical_not(numpy.isnan(indices)))[0]
            lines[:, loc] = param[:, indices[loc].astype(numpy.int64)]
            new_length = data_counter + len(lines)
            data[data_counter: new_length, 2:] = lines
            for i, x in enumerate((frame, numpy.arange(self.ca.cells))):
                data[data_counter:new_length, i] = x
            data_counter = new_length
        if save:
            colnames = ['Event', 'Cell']
            colnames.extend(numpy.arange(-w, w))
            pandas.DataFrame(data, columns=colnames).to_excel(self.prefix + '-Ripple-table.xlsx')
        else:
            fig, ca = plt.subplots()
            ca.axvline(w, color='black', alpha=0.8)
            for c in range(self.ca.cells):
                wh = numpy.where(data[:, 1] == c)[0]
                ca.plot(numpy.nanmean(data[wh, 2:], axis=0), color='grey', alpha=0.4, linewidth=1)
            ca.plot(numpy.nanmean(data[:, 2:], axis=0), color='green', linewidth=2)
            seconds = numpy.arange(-range_secs, range_secs + 1)
            xtk = (seconds * fps + w)
            ca.set_xticks(xtk)
            ca.set_xticklabels(seconds)
            ca.set_xlabel('Time (s)')
            ca.set_ylabel(param_key)
            plt.show()

#
if __name__ == '__main__':
    a = None
    # ripple sorting
    # import pandas
    # path = 'G://2P//All_from_2Prec_070918//dlx_Gergo//'
    # os.chdir(path)
    # prefix = 'dlx_Gergo_000_403'
    # a = Scores(prefix)
    # # a.ripple_order()
    # window = 10
    # self = a
    # cells = [x for x in range(10)]
    # frames = [self.sampletoframe(x.p[0]) for x in self.ripples.events]
    # # rs = a.get_ripple_scores()
    # # cells = list(numpy.where(numpy.array(rs) > 0.2)[0])
    #
    # for c in enumerate(numpy.argsort(-numpy.nanmean(delays, axis=1))):
    #     y = numpy.nanmean(traces[:, c, :], axis=1)
    #     ma = pandas.DataFrame(y-y[:5].mean()).ewm(span=3).mean()[0]
    #     plt.plot(ma[5:], color=plt.cm.copper(1-ci/len(cells)), alpha=0.5)
    #
    # for ci, c in enumerate(numpy.argsort(-numpy.nanmean(delays, axis=1))):
    #     y = numpy.nanmean(traces[:, c, :], axis=1)
    #     ma = pandas.DataFrame(y-y[:5].mean()).ewm(span=3).mean()[0]
    #     color = (plt.cm.copper(1-ci/len(cells)), 'red')[c==0]
    #     plt.plot(ma[5:], color=color, alpha=0.5)
    #
    #

    # event detection
    # os.chdir('D://qa-june-aug//')
    # prefix = 'qa_073_801'
    # a = Scores(prefix)
    # a.sync_events()
    # Scores('qa_072_700').sync_events()
    # Scores('qa_073_801').sync_events()

    # os.chdir('//NEURO-GHWR8N2//AnalysisPC-Barna-2PBackup3//cckdlx//')
    # a = Scores('cckdlx_080_360', norip=True)
    # sc = a.stoprun_scores(ret_loc='actual')
    # rs = a.ramping_scores()
