import os


class AssetFinder:
    def __init__(self, path=None):
        self.suffix_list = '''.mat,.tdml,.align,_avgmax.tif,_eye.mat,_nonrigid.sbx,_nonrigid.mat,_nonrigid.segment,.npy,
        _nonrigid.signals,_preview.tif,_quadrature.mat,.ephys,.png,_ripples//,.np//'''.replace('\n', '').split(',')
        # add separate locator for rois or handle outside
        self.contain_list = ['roi']
        self.sufdir = {}
        self.prefixes = []
        self.flist = []
        self.path = path
        if path is not None:
            self.update()

    def update(self, path=None):
        if path is not None:
            self.path = path
        self.prefixes = []
        self.flist = os.listdir(self.path)
        for f in self.flist:
            if f.endswith('.mat'):
                if not (f.endswith('_eye.mat') or f.endswith('_quadrature.mat') or f.endswith('align.mat')
                        or f.endswith('_radius.mat')):
                    if f.endswith('_nonrigid.mat'):
                        prefix = f[:f.find('_nonrigid')]
                    elif f.endswith('_rigid.mat'):
                        prefix = f[:f.find('_rigid')]
                    else:
                        prefix = f[:f.find('.mat')]
                    if prefix not in self.prefixes:
                        self.prefixes.append(prefix)
            elif f.endswith('.sbx'):
                if f.endswith('_nonrigid.sbx'):
                    prefix = f[:f.find('_nonrigid')]
                elif f.endswith('_rigid.sbx'):
                    prefix = f[:f.find('_rigid')]
                else:
                    prefix = f[:-4]
                if prefix not in self.prefixes:
                    self.prefixes.append(prefix)
            elif f.endswith('.hdf5'):
                if f.endswith('_rigid.hdf5'):
                    prefix = f[:f.find('_rigid')]
                else:
                    prefix = f[:-5]
                if prefix not in self.prefixes:
                    self.prefixes.append(prefix)
        self.prefixes.sort()
        for suffix in self.suffix_list:
            sublist = []
            for f in self.flist:
                if f.endswith(suffix):
                    sublist.append(f[:f.find(suffix)])
            self.sufdir[suffix] = sublist
        for sub in self.contain_list:
            sublist = []
            for prefix in self.prefixes:
                for f in self.flist:
                    if prefix in f:
                        if sub in f:
                            if prefix not in sublist:
                                sublist.append(prefix)
            self.sufdir[sub] = sublist

    def get_prefixes(self):
        return self.prefixes

    def has_suffix(self, prefix, suffix):
        return prefix + suffix in self.flist

    def get_list(self, plist: list, mode: str):
        newsel = []
        if mode == 'roi-no-segment':
            for i, prefix in enumerate(plist):
                if prefix in self.sufdir['roi']:
                    if prefix not in self.sufdir['_nonrigid.segment']:
                        newsel.append(i)
        else:
            return -1
        return newsel

    '''---------------------add functions here for each specific call---------------------'''

if __name__ == '__main__':
    path = '//NEURO-GHWR8N2//AnalysisPC-Barna-2PBackup3//cckdlx//'
    a = AssetFinder()
