import os
import numpy
from datetime import datetime
import shutil
from sbxreader import loadmat

class RenameTDML:
    def __init__(self, path, pflist):
        self.result = {}
        self.pflist = pflist
        report = ''
        os.chdir(path)
        all_files = os.listdir(path)
        tdmls = {}
        mats = {}
        tdml_frames = {}
        for f in all_files:
            if f.endswith('.tdml'):
                tdmls[f] = min(os.path.getctime(f), os.path.getmtime(f))
            elif f.endswith('.mat'):
                if all([not f.endswith('_eye.mat'), not f.endswith('_quadrature.mat'), not f.endswith('_rigid.mat'),
                        not f.endswith('_nonrigid.mat')]):
                    mats[f[:-4]] = min(os.path.getctime(f), os.path.getmtime(f))
        #sort files by time
        pfs = []
        times = []
        for p, t in mats.items():
            pfs.append(p)
            times.append(t)
        spfs = [pfs[i] for i in numpy.argsort(times)]
        for pf in pflist:
            report += pf + ' - '
            if pf not in mats:
                report += '.mat file not found\n'
                continue
            t0 = mats[pf]
            #find next and previous .mat time
            i0 = spfs.index(pf)
            i1 = max(0, i0-1)
            i2 = min(len(spfs)-1, i0+1)
            t1 = max(mats[spfs[i1]], t0-600)
            t2 = min(mats[spfs[i2]], t0+600)
            # get list of tdmls in between:
            qual_ts = []
            tdelta = []
            for f, t in tdmls.items():
                if t1 < t < t2:
                    if os.path.exists(f):
                        tdelta.append(t0-t)
                        qual_ts.append(f)
            if len(qual_ts) == 0:
                report += 'no tdml found\n'
                continue
            elif len(qual_ts) == 1:
                res_f = qual_ts[0]
                report += f'Found 1 tdml: {res_f}\n'
                self.result[pf] = res_f
            else:
                # discard all but last and next files before-after t0
                tdelta = numpy.array(tdelta)
                res_ts = []
                wh = tdelta[numpy.where(tdelta <= 0)]
                if len(wh) > 0:
                    res_ts.append(qual_ts[numpy.argmax(tdelta == wh.max())])
                wh = tdelta[numpy.where(tdelta > 0)]
                if len(wh) > 0:
                    res_ts.append(qual_ts[numpy.argmax(tdelta == wh.min())])
                if len(res_ts) > 1:
                    #see which one is closer in frames
                    fdiff = []
                    n_frames = len(loadmat(pf + '_quadrature.mat')['quad_data'])
                    for f in res_ts:
                        if f in tdml_frames:
                            ntf = tdml_frames[f]
                        else:
                            with open(f, 'r') as tdml:
                                ntf = tdml.read().count('sensor') * 15 / 2
                            tdml_frames[f] = ntf
                        fdiff.append(abs(1 - ntf / n_frames))
                    res_f = res_ts[numpy.argmin(fdiff)]
                else:
                    res_f = res_ts[0]
                report += f'Multiple tdmls, best guess: {res_f}\n'
                self.result[pf] = res_f
            # del(tdmls[res_f])
        self.report = report[:-1]

    def perform_rename(self, indices):
        budir = 'moved_tdmls//'
        if not os.path.exists(budir):
            os.makedirs(budir)
        report = ''
        n_moved = 0
        for i in indices:
            pf = self.pflist[i]
            if pf in self.result:
                res_f = self.result[pf]
                if os.path.exists(res_f):
                    newname = pf + '.tdml'
                    if res_f != newname:
                        stat = os.stat(res_f)
                        shutil.copy(res_f, budir+res_f)
                        os.utime(budir + res_f, (stat.st_atime, stat.st_mtime))
                        shutil.move(res_f, newname)
                        os.utime(newname, (stat.st_atime, stat.st_mtime))
                        n_moved += 1
                    report += pf + '\t' + res_f + '\n'
                else:
                    report += pf + '\t' + 'Not found' + '\n'
        ts = str(datetime.now())
        ts = 'tdml-move-'+ts[:ts.find(' ')]+'-'+ts[ts.find(' ')+1:ts.find('.')].replace(':','-')+'-report.txt'
        with open(budir + ts, 'w') as f:
            f.write(report)
        return f'{n_moved} files renamed'
if __name__ == '__main__':
    path = 'D://qa-june-aug'
    pflist = ['qa_075_701']
    pf = pflist[0]
    os.chdir(path)
    # n_frames = len(loadmat(pf + '_quadrature.mat')['quad_data'])
    # m=loadmat(pf + '_quadrature.mat')
    a = RenameTDML(path, pflist)