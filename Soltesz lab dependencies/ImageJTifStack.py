from tifffile import TiffFile


class CFStack:
    def __init__(self, fn):
        self.tf = TiffFile(fn)
        self.im = self.tf.asarray()
        self.nz, self.nc, self.w, self.h = self.im.shape  # dimensions
        self.x, self.y, self.z = 0, 0, 0  # resolutions
        self.meta = self.tf.imagej_metadata
        # parse resolution
        info_fields = self.meta['Info'].split('\n')
        scale_key = 'Experiment|AcquisitionBlock|AcquisitionModeSetup|'
        dim_keys = {'x': 'ScalingX', 'y': 'ScalingY', 'z': 'ScalingZ'}
        for i in info_fields:
            if scale_key in i:
                for key, value in dim_keys.items():
                    if value in i:
                        v = float(i.split('=')[1].strip())
                        setattr(self, key, v * 10e5)  # microns
