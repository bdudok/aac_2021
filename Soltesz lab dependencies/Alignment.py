import os
import numpy
# from skimage.feature import register_translation
from skimage import transform
import cv2
import matplotlib.path as mplpath
from matplotlib.patches import Polygon
from PIL import ImageTk
from PIL import Image as ImagePIL
from tkinter import *
from tkinter import filedialog, messagebox, font
from tkinter.filedialog import askopenfilenames
from multiprocessing import Queue, Process
import json
from TkApps import EntryDialog
import shutil


# import imreg_dft


class Mouse:
    def __init__(self, path: str, mouse_id: str):
        self.path = path
        if not self.path.endswith('//'):
            self.path += '//'
        self.id = mouse_id
        self.fn = mouse_id + '.mouse'
        if not os.path.exists(self.path + self.id):
            os.mkdir(self.path + self.id)
        os.chdir(self.path + self.id)
        # load lists
        if os.path.exists(self.fn):
            with open(self.fn, 'r') as f:
                self.data = json.load(f)
        else:
            self.data = {'cells': {}, 'live': {}, 'confoc': {}, 'stacks': {}, 'cell_index': 0, 'reference': None}
        self.cache = {}
        self.raw_cache = {}

    def save(self, backup=False):
        # back up existing version
        if backup and os.path.exists(self.fn):
            os.rename(self.fn,
                      f'{self.id}-backup_{numpy.count_nonzero([f.endswith(".mouse") for f in os.listdir()])}.mouse')
        with open(self.fn, 'w') as f:
            json.dump(self.data, f)

    def next_cell(self):
        self.data['cell_index'] += 1
        return self.data['cell_index']

    def add_cell(self, point=None):
        c = self.next_cell()
        self.data['cells'][c] = {'markers': {'PV': 1, 'CCK': 1, 'VIP': 1}, 'positions': {}, 'reference': None,
                                 'IJ': None, 'notes': '', 'layer': None, 'region': None}
        if point is not None:
            self.update_cell_pos(c, point)
        return c

    def update_cell_pos(self, c, point):
        fn, pos = point
        pos = list(self.apply_transform(pos, self.get_transform(fn), inverse=True))
        self.data['cells'][c]['positions'][fn] = pos
        if self.data['cells'][c]['reference'] == None:
            self.data['cells'][c]['reference'] = [fn, pos]

    def del_cell(self, c, fn=None):
        if fn is None:
            self.data['cells'].pop(c, None)
        else:
            self.data['cells'][c]['positions'].pop(fn, None)
            return len(self.data['cells'][c]['positions'])

    def get_pos(self, c, fn):
        if c in self.data['cells']:
            return self.data['cells'][c]['positions'].get(fn, None)
        return None

    def get_marker_values(self, c):
        # for values, 0,1,2, 3 are used for neg, none, pos, weak
        return self.data['cells'][c]['markers']

    def set_marker_values(self, c, markers):
        self.data['cells'][c]['markers'] = markers

    def get_layer_values(self, c):
        #  values 0,1,2 are 'ori', 'pyr', 'rad'
        return self.data['cells'][c].get('layer', None)

    def get_region_values(self, c):
        #  values 0,1,2 are 'CA1', 'CA2', 'CA3'
        return self.data['cells'][c].get('region', None)

    def set_layer_values(self, c, layer):
        self.data['cells'][c]['layer'] = layer

    def set_region_values(self, c, x):
        self.data['cells'][c]['region'] = x

    def get_cf(self, c):
        return self.data['cells'][c].get('IJ')

    def set_cf(self, c, fn):
        self.data['cells'][c]['IJ'] = fn

    def set_found(self, c, found):
        if c in self.data['cells']:
            self.data['cells'][c]['found'] = found

    def get_found(self, c):
        if c in self.data['cells']:
            return self.data['cells'][c].get('found', False)
        else:
            return False

    def get_note(self, c):
        return self.data['cells'][c].get('notes')

    def set_note(self, c, v):
        if c in self.data['cells']:
            self.data['cells'][c]['notes'] = v

    def add_lives(self, fns, dst='live'):
        for f in fns:
            items = f.split('/')
            fn = items[-1]
            try:
                shutil.copy(f, fn)
            except shutil.SameFileError:
                pass
            self.data[dst][fn] = {'transform': None}

    def set_transform(self, fn, transform):
        self.data['live'][fn]['transform'] = list(transform)
        # transform is stored as [x, y, r]

    def get_transform(self, fn):
        tr = self.data['live'][fn]['transform']
        if tr is not None:
            return transform.SimilarityTransform(translation=(tr[0], tr[1]), rotation=numpy.deg2rad(tr[2]))
        else:
            return None

    def get_cell_indices(self):
        return self.data['cells'].keys()

    def enumerate_cells(self, fn):
        ret = []
        tr = self.get_transform(fn)
        for i, c in self.data['cells'].items():
            if fn in c['positions']:
                ret.append((i, self.apply_transform(c['positions'][fn], tr)))
        return ret

    def pick_cell(self, x, y, fn):
        min_d = 999
        my_c = None
        for c, p in self.enumerate_cells(fn):
            d = numpy.sqrt((x - p[0]) ** 2 + (y - p[1]) ** 2)
            if d < min_d:
                min_d = d
                my_c = c
        return my_c

    def apply_transform(self, pos, tr, inverse=False):
        if tr is None:
            return pos
        if inverse:
            return transform.matrix_transform(pos, tr._inv_matrix)[0]
        return transform.matrix_transform(pos, tr.params)[0]

    def enumerate_images(self, c):
        return [p for p in self.data['cells'][c]['positions'].keys()]

    def get_reference_position(self, c):
        point = self.data['cells'][c].get('reference', None)
        if point is None:
            return [None]
        fn, pos = point
        pos = self.apply_transform(pos, self.get_transform(fn))
        return [fn, pos]

    def get_live_tags(self, src='live'):
        return self.data[src].keys()

    def get_marker_set(self):
        m_set = []
        for c, d in self.data['cells'].items():
            for m in d['markers']:
                if m not in m_set:
                    m_set.append(m)
        m_set.sort()
        return m_set

    def get_image(self, fn):
        if fn not in self.raw_cache:
            pic = cv2.imread(fn, 1)
            # move blue to red if necessary for better visibility
            if pic[:, :, 0].mean() < pic[:, :, 2].mean():
                pic = pic[:, :, [2, 1, 0]]
            self.raw_cache[fn] = pic
        return self.raw_cache[fn]

    def get_image_PIL(self, fn):
        if fn not in self.cache:
            self.update_cached_image(fn)
        return self.cache[fn]

    def update_cached_image(self, fn):
        if fn is None:
            return -1
        image = self.get_image(fn)
        tr = self.data['live'][fn]['transform']
        if tr is not None and not all(numpy.array(tr) == 0):
            x, y, r = tr
            shift_y, shift_x = (numpy.array(image.shape[:2]) - 1) / 2.
            tf_rotate = transform.SimilarityTransform(rotation=numpy.deg2rad(r))
            tf_shift = transform.SimilarityTransform(translation=[-shift_x, -shift_y])
            tf_shift_inv = transform.SimilarityTransform(translation=[shift_x + x, shift_y + y])
            image = (transform.warp(image, (tf_shift + (tf_rotate + tf_shift_inv)).inverse, order=3) * 255).astype(
                'uint8')
        self.cache[fn] = ImagePIL.fromarray(image)

    @staticmethod
    def dist(p1, p2):
        return numpy.sqrt(numpy.sum([(p1[i] - p2[i]) ** 2 for i in (0, 1)]))

    def pair(self, c, pi):
        self.id_pairs[pi] = c
        self.paired_id.append(c)
        self.paired_pg.append(pi)
        self.paired_dists.append(self.dist(self.cms[pi], self.m_cells[c]))

    def pair_with_polygons(self, session_instance, contain_only=False):
        a = session_instance
        self.m_cells = {}
        for i, c in self.data['cells'].items():
            for fn in c['positions'].keys():
                if a.prefix in fn:
                    self.m_cells[i] = c['positions'][fn]
                    break
        ps = []
        for c in range(a.ca.cells):
            ps.append(Polygon(a.rois.polys[c]))

        # cycle through polys and find id cells inside. if multiple, pick closest to cm. keep track of pairs
        self.cms = [(a.rois.polys[pi].min(axis=0) + a.rois.polys[pi].max(axis=0)) / 2 for pi in range(a.ca.cells)]
        self.paired_id = []
        self.paired_pg = []
        self.id_pairs={}
        self.paired_dists = []

        for pi, pg in enumerate(ps):
            contains = []
            for c, p in self.m_cells.items():
                if pg.contains_point(p):
                    if c not in self.paired_id:
                        contains.append(c)
            if len(contains) == 1:
                self.pair(contains[0], pi)
            elif len(contains) > 1:
                dists = [self.dist(self.cms[pi], self.m_cells[c]) for c in contains]
                self.pair(contains[numpy.argmin(dists)], pi)
        # cycle remaining points and see if unused poly is within distance limit
        if not contain_only:
            if len(self.paired_dists) > 5:
                thr = numpy.percentile(self.paired_dists, 90)
            else:
                thr = 5
            for c, p in self.m_cells.items():
                if c in self.paired_id:
                    continue
                avail = []
                for pi, pg in enumerate(ps):
                    if pi not in self.paired_pg:
                        avail.append(pi)
                if len(avail) > 0:
                    dists = [self.dist(self.cms[pi], self.m_cells[c]) for pi in avail]
                    nn = numpy.argmin(dists)
                    if dists[nn] < thr:
                        self.pair(c, avail[nn])

        return self.id_pairs


class App:
    def __init__(self):
        self.root = Tk()
        self.root.title('Alignment')
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.current_column = 0
        self.mouse = None

        # define panels
        self.celllist = CellList(self, self.root, self.column(), span=2)
        self.livelist = LiveList(self, self.root, self.column(), span=2)
        self.view1 = ImgView(self, self.root, self.column(0), row=0, color='green')
        self.view2 = ImgView(self, self.root, self.column(), row=1, color='red', keepref=True)
        self.zoom1 = ZoomView(self, self.root, self.column(0), row=0)
        self.zoom2 = ZoomView(self, self.root, self.column(), row=1)
        self.view1.bind_zoom(self.zoom1)
        self.view2.bind_zoom(self.zoom2)
        self.annotations = Annotations(self, self.root, self.column(), span=2)

        # bind controls
        self.listen_mouse = True
        self.root.bind_all("<MouseWheel>", self.wheel_callback)

        self.root.mainloop()

    def on_closing(self):
        if messagebox.askokcancel("Quit", "OK to quit?"):
            print('Quit.')
            self.root.destroy()

    def wheel_callback(self, event):
        if self.listen_mouse:
            flag = event.state
            if hasattr(event, 'widget'):
                if hasattr(event.widget, 'widgetName'):
                    if event.widget.widgetName == 'listbox':
                        return None  # don't scroll if a listbox is being scrolled
            if flag == 12:  # ctrl pressed
                self.livelist.next(event.delta)
            else:
                self.celllist.next(event.delta)

    def column(self, incr=1):
        self.current_column += incr
        return self.current_column - incr

    def pick_cell(self, x, y, fn, remove=False):
        c = self.mouse.pick_cell(x, y, fn)
        if remove:
            if self.mouse.del_cell(c, fn) == 0:
                self.del_cell_callback(c)
        else:
            self.celllist.set_selection(c)

    def mouse_open_callback(self, path):
        if self.mouse is not None:
            self.clear()
        items = path.split('/')
        self.mouse = Mouse('//'.join(items[:-1]), items[-1])
        # put cells in list
        for i, c in enumerate(self.mouse.get_cell_indices()):
            self.celllist.listbox.insert(END, c)
            if self.mouse.get_found(c):
                self.celllist.listbox.itemconfig(i, {'fg': 'magenta'})
        self.celllist.btn_text.set(items[-1])
        # put lives
        for x in self.mouse.get_live_tags():
            self.livelist.listbox.insert(END, x)
        # put confocs
        for x in self.mouse.get_live_tags(src='confoc'):
            self.annotations.listbox.insert(END, x)
        self.annotations.update_markers_widget()

    def clear(self):
        self.celllist.clear_selection()
        for widget in (self.view1, self.view2, self.annotations):
            widget.clear()
        for widget in (self.celllist, self.annotations, self.livelist):
            widget.listbox.delete(0, END)


    def add_cell_callback(self, point=None):
        c = self.mouse.add_cell(point)
        self.celllist.listbox.insert(END, c)
        self.celllist.set_selection(c)
        self.celllist.update_reference()

    def del_cell_callback(self, c):
        if c is not None:
            self.mouse.del_cell(c)
            self.celllist.active_cell = None
            i = self.celllist.list_values.get().index(c)
            self.celllist.listbox.delete(i)
            self.celllist.clear_selection()
            self.view1.update()


class AppPanel:
    def __init__(self, parent, master, column, row=0, span=1, color=None):
        self.parent = parent
        self.master = master
        self.current_row = 0
        self.config = {}
        self.color = color
        self.event = MyEvent()
        self.frame = Frame(master)
        self.frame.grid(row=row, column=column, rowspan=span, sticky=N + W)

    def row(self, incr=1):
        self.current_row += incr
        return self.current_row - incr

    def next(self, delta):
        if self.active_item is None:
            i = 0
        else:
            i = self.active_item
        delta = (1, -1)[delta > 0]
        self.event.i = max(0, min(len(self.list_values.get()) - 1, i + delta))
        self.listbox.select_clear(0, END)
        self.listbox.select_set(self.event.i)
        self.select_callback(self.event)


class MyEvent:
    def __init__(self):
        self.x_root = -1
        self.y_root = 1
        self.i = 0


class Lasso:
    def __init__(self, parent):
        im = numpy.array(parent.mouse.get_image_PIL(parent.view2.active_image))
        for c, p in parent.mouse.enumerate_cells(parent.view2.active_image):
            cv2.circle(im, tuple([int(x) for x in p]), 5, (255, 255, 255))
        self.im = im
        self.polys = []
        self.coords = []
        self.drawing = False
        cv2.imshow('Lasso', self.im)
        cv2.moveWindow('Lasso', 0, 0)
        cv2.setMouseCallback('Lasso', self.drawmouse)
        self.retval = False
        while self.retval is False:
            if cv2.getWindowProperty('Lasso', 0) < 0:
                break
            k = cv2.waitKey(1) & 0xFF
        cv2.destroyWindow('Lasso')

    def drawmouse(self, event, y, x, flags, param):
        if event == cv2.EVENT_RBUTTONDBLCLK:
            self.retval = -1
        elif event == cv2.EVENT_LBUTTONDBLCLK:
            if len(self.polys) > 0:
                self.retval = self.polys
            else:
                self.retval = -1
        elif event == cv2.EVENT_RBUTTONDOWN:
            if len(self.coords) > 2:
                cv2.line(self.im, (y, x), (self.coords[0][1], self.coords[0][0]), color=(255, 255, 0))
                self.clean_poly()
                cv2.imshow('Lasso', self.im)
        elif event == cv2.EVENT_LBUTTONDOWN:
            self.drawing = True
        elif event == cv2.EVENT_LBUTTONUP:
            self.drawing = False

        if event == cv2.EVENT_MOUSEMOVE and self.drawing:
            self.coords.append([x, y])
            if len(self.coords) > 1:
                cv2.line(self.im, (y, x), (self.coords[-2][1], self.coords[-2][0]), color=(255, 255, 0))
                cv2.imshow('Lasso', self.im)

    def clean_poly(self):
        new = []
        for p in self.coords:
            x = round(p[0])
            y = round(p[1])
            np = [y, x]
            if not np in new:
                new.append(np)
        new.append(new[0])
        self.polys.append(new)
        self.coords = []


class Annotations(AppPanel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        Label(self.frame, text='Markers').grid(row=self.row(), sticky=N)
        self.marker_frame = Frame(self.frame)
        self.marker_frame.grid(row=self.row(), sticky=N + W)
        self.market_set = []
        self.modes = {}
        self.active_cell = None
        self.is_found = False
        Button(self.frame, text='Add marker', command=self.add_marker_callback).grid(row=self.row(), sticky=N)

        self.toggle_found = Button(self.frame, text='Cell found', command=self.found_callback, relief='raised')
        self.toggle_found.grid(row=self.row(), sticky=N, pady=10)

        Label(self.frame, text='Layer').grid(row=self.row(), sticky=N)
        self.layer_frame = Frame(self.frame)
        self.layer_frame.grid(row=self.row(), sticky=N)
        self.layer_set = ['ori', 'pyr', 'rad', 'none']
        self.layer_modes = IntVar()
        for col, m in enumerate(self.layer_set):
            Radiobutton(self.layer_frame, text=self.layer_set[col], variable=self.layer_modes, value=col, width=4,
                        command=self.update_layer, indicatoron=0).grid(row=0, column=col)
        self.layer_modes.set(3)

        Label(self.frame, text='Region').grid(row=self.row(), sticky=N)
        self.region_frame = Frame(self.frame)
        self.region_frame.grid(row=self.row(), sticky=N)
        self.region_set = ['CA1', 'CA2', 'CA3', 'none']
        self.region_modes = IntVar()
        for col, m in enumerate(self.region_set):
            Radiobutton(self.region_frame, text=self.region_set[col], variable=self.region_modes, value=col, width=4,
                        command=self.update_region, indicatoron=0).grid(row=0, column=col)
        self.region_modes.set(3)

        Label(self.frame, text='Confocal images').grid(row=self.row(), pady=10)
        Button(self.frame, text='Add files', command=self.add_callback).grid(row=self.row(), sticky=N)
        self.list_values = Variable()
        self.listbox = Listbox(self.frame, selectmode=BROWSE, height=10, width=50, listvariable=self.list_values)
        self.listbox.grid(row=self.row(), sticky=N)
        self.listbox.bind('<<ListboxSelect>>', self.select_callback)

        Label(self.frame, text='Notes').grid(row=self.row(), pady=10)
        self.notes = Text(self.frame, height=10, width=50)
        self.notes.grid(row=self.row(), column=0, sticky=N + E)

        Label(self.frame, text='Draw layer').grid(row=self.row(), sticky=N)
        self.draw_frame = Frame(self.frame)
        self.draw_frame.grid(row=self.row(), sticky=N)
        self.draw_mode = IntVar()
        for col, m in enumerate(self.layer_set):
            Radiobutton(self.draw_frame, text=self.layer_set[col], variable=self.draw_mode, value=col, width=4,
                        command=self.draw_layer, indicatoron=0).grid(row=0, column=col)
        self.draw_mode.set(3)

    def clear(self):
        self.active_cell = None

    def update_markers_widget(self):
        self.market_set = self.parent.mouse.get_marker_set()
        bullet = '•'
        self.modes = {}
        row = 0
        for m in self.market_set:
            self.modes[m] = IntVar()
            Label(self.marker_frame, text=m).grid(row=row, column=0, sticky=W)
            for col in range(4):
                Radiobutton(self.marker_frame, text=('Neg ', 'None', 'Pos', 'Weak')[col], variable=self.modes[m],
                            value=col, width=4, command=self.update, indicatoron=0).grid(row=row, column=col + 1,
                                                                                         sticky=E)
            self.modes[m].set(1)
            row += 1

    def add_callback(self):
        if self.parent.mouse is not None:
            fn = askopenfilenames(title='Select image files to import', defaultextension='.tif',
                                  filetypes=[('PNG Image', '.png'), ('all files', '.*'), ])
            self.parent.mouse.add_lives(fn, dst='confoc')
            for f in fn:
                items = f.split('/')
                self.listbox.insert(END, items[-1])

    def found_callback(self, found=None):
        if found is None:
            self.is_found = not self.is_found
        else:
            self.is_found = found
        self.toggle_found.config(relief=('raised', 'sunken')[self.is_found])
        self.parent.mouse.set_found(self.active_cell, self.is_found)
        self.parent.celllist.set_color(self.active_cell, self.is_found)
        if found is None:
            self.parent.view1.update()

    def add_marker_callback(self):
        ed = EntryDialog('Name of new marker:', root=self.master)
        m = ed.ret
        self.update({m: 1})
        ed.kill()
        self.update_markers_widget()

    def update_notes(self):
        self.parent.mouse.set_note(self.active_cell, self.notes.get())

    def update_cell(self, c):
        if self.active_cell is not None:
            self.parent.mouse.set_note(self.active_cell, self.notes.get(1.0, END))
        self.notes.delete(1.0, END)
        self.active_cell = c
        for m, v in self.parent.mouse.get_marker_values(c).items():
            if v is None:
                v = 1
            if m not in self.modes:
                self.update_markers_widget()
            self.modes[m].set(v)
        self.layer_modes.set(self.parent.mouse.get_layer_values(c))
        self.region_modes.set(self.parent.mouse.get_region_values(c))
        self.found_callback(self.parent.mouse.get_found(c))
        self.listbox.selection_clear(0, END)
        cf = self.parent.mouse.get_cf(c)
        if cf is not None:
            self.listbox.selection_set(self.list_values.get().index(cf))
        n = self.parent.mouse.get_note(c)
        if n is not None:
            self.notes.insert(END, n)

    def update(self, m_v=None):
        if m_v is None:
            m_v = {}
        for m, v in self.modes.items():
            m_v[m] = v.get()
        self.parent.mouse.set_marker_values(self.active_cell, m_v)

    def update_layer(self, l_v=None):
        if l_v is None:
            l_v = self.layer_modes.get()
        self.parent.mouse.set_layer_values(self.active_cell, l_v)

    def update_region(self, l_v=None):
        if l_v is None:
            l_v = self.region_modes.get()
        self.parent.mouse.set_region_values(self.active_cell, l_v)

    def draw_layer(self):
        l_v = self.draw_mode.get()
        if l_v < 3:
            self.parent.listen_mouse = False
            ps = Lasso(self.parent).retval
            if ps is not False:
                for poly in ps:
                    roi = mplpath.Path(poly)
                    for c, p in self.parent.mouse.enumerate_cells(self.parent.view2.active_image):
                        if roi.contains_point(p):
                            self.parent.mouse.set_layer_values(c, l_v)
            self.draw_mode.set(3)
            self.parent.view2.update()
        self.parent.listen_mouse = True

    def select_callback(self, event):
        if self.parent.listen_mouse:
            if event.x_root == -1:
                self.found_callback(True)
                self.parent.mouse.set_cf(self.active_cell, self.list_values.get()[self.listbox.curselection()[0]])


class CellList(AppPanel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.active_item = None
        self.active_cell = None
        self.active_reference = None
        self.btn_text = StringVar(self.frame, value='Open Mouse')
        Button(self.frame, textvariable=self.btn_text, command=self.open_callback).grid(row=self.row(), sticky=N)

        Label(self.frame, text='Cells').grid(row=self.row(), pady=10)

        self.list_values = Variable()
        self.listbox = Listbox(self.frame, selectmode=BROWSE, height=50, listvariable=self.list_values, name='cellbox')
        self.listbox.grid(row=self.row(), sticky=N)
        self.listbox.bind('<<ListboxSelect>>', self.select_callback)

        Button(self.frame, text='Add Cell', command=self.add_callback).grid(row=self.row(), sticky=N)
        Button(self.frame, text='Delete Cell', command=self.del_callback).grid(row=self.row(), sticky=N)
        Button(self.frame, text='New Mouse', command=self.new_callback).grid(row=self.row(), sticky=N)
        Button(self.frame, text='Save Mouse', command=self.save_callback).grid(row=self.row(), sticky=N)
        Button(self.frame, text='Back up Mouse', command=self.backup_callback).grid(row=self.row(), sticky=N)

    def add_callback(self):
        self.parent.add_cell_callback()

    def del_callback(self):
        self.parent.del_cell_callback(self.active_cell)

    def open_callback(self):
        self.parent.mouse_open_callback(filedialog.askdirectory(title='Open mouse folder'))

    def new_callback(self):
        wdir = filedialog.askdirectory(title='Select a directory where new mouse will be saved')
        ed = EntryDialog('ID of new mouse:', root=self.master)
        mouse_id = ed.ret
        self.parent.mouse = Mouse(wdir, mouse_id)
        self.btn_text.set(mouse_id)
        self.parent.clear()
        ed.kill()

    def save_callback(self):
        self.parent.mouse.save()

    def backup_callback(self):
        self.parent.mouse.save(backup=True)

    def select_callback(self, event):
        if self.parent.listen_mouse:
            if event.x_root == -1:
                if event.y_root == 1:  # hack to reuse code for mouse wheel
                    i = event.i
                else:
                    i = self.listbox.curselection()[0]
                c = self.list_values.get()[i]
                self.active_cell = c
                if self.active_item is not None:
                    self.listbox.itemconfig(self.active_item, {'bg': 'white'})
                self.active_item = i
                self.listbox.itemconfig(i, {'bg': 'blue'})
                self.parent.livelist.color_available(c)
                self.update_reference()

    def set_color(self, c, found):
        self.listbox.itemconfig(self.list_values.get().index(c), {'fg': ('black', 'magenta')[found]})

    def update_reference(self):
        r = self.parent.mouse.get_reference_position(self.active_cell)
        if len(r) > 1:
            self.parent.view2.set_active_image(r[0])
            self.active_reference = r[0]
            self.parent.view1.update()
        # also update cell annotations using the same call:
        self.parent.annotations.update_cell(self.active_cell)

    def clear_selection(self):
        self.listbox.select_clear(0, END)
        self.active_cell = None
        self.active_item = None
        self.active_reference = None

    def set_selection(self, c):
        self.active_cell = c
        l = self.list_values.get()
        if c in l:
            self.active_item = l.index(c)
            self.listbox.select_clear(0, END)
            self.listbox.select_set(self.active_item)
            self.update_reference()
        else:
            self.active_item = None


class LiveList(AppPanel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.active_item = None
        self.active_image = None
        Button(self.frame, text='Add', command=self.add_callback).grid(row=self.row(), sticky=N)

        Label(self.frame, text='Live movies').grid(row=self.row(), pady=10)

        self.list_values = Variable()
        self.listbox = Listbox(self.frame, selectmode=BROWSE, height=25, width=50, listvariable=self.list_values,
                               name='livebox')
        self.listbox.grid(row=self.row(), sticky=N)
        self.listbox.bind('<<ListboxSelect>>', self.select_callback)

        # Button(self.frame, text='Auto Transform', command=self.transform_callback).grid(row=self.row(), sticky=N)
        xframe = Frame(self.frame)
        xframe.grid(row=self.row())
        self.translation_x = Scale(xframe, from_=-300, to=300, orient=HORIZONTAL, command=self.on_slider_move)
        self.translation_x.set(0)
        self.translation_x.grid(row=0, column=1, sticky=N)
        Button(xframe, text='+', command=self.xplus_callback).grid(row=0, column=2)
        Button(xframe, text='-', command=self.xminus_callback).grid(row=0, column=0)
        Button(self.frame, text='-', command=self.yminus_callback).grid(row=self.row(), sticky=W)
        self.translation_y = Scale(self.frame, from_=-200, to=200, orient=VERTICAL, command=self.on_slider_move)
        self.translation_y.set(0)
        self.translation_y.grid(row=self.row(), sticky=W)
        Button(self.frame, text='+', command=self.yplus_callback).grid(row=self.row(), sticky=W)
        self.rotation = Scale(self.frame, from_=-30, to=30, orient=HORIZONTAL, command=self.on_slider_move)
        self.rotation.set(0)
        self.rotation.grid(row=self.row(), sticky=N)
        Button(self.frame, text='Reset Transform', command=self.reset_callback).grid(row=self.row(), sticky=N)
        Button(self.frame, text='Copy reference', command=self.copy_callback).grid(row=self.row(), sticky=N)

    def on_slider_move(self, *args):
        self.apply_transform()
        self.parent.view1.update()

    def xplus_callback(self):
        self.translation_x.set(self.translation_x.get() + 1)

    def xminus_callback(self):
        self.translation_x.set(self.translation_x.get() - 1)

    def yplus_callback(self):
        self.translation_y.set(self.translation_y.get() + 1)

    def yminus_callback(self):
        self.translation_y.set(self.translation_y.get() - 1)

    # def transform_callback(self):
    #     ref = self.parent.mouse.get_reference_position(self.parent.celllist.active_cell)
    #     if len(ref) == 0:
    #         print('No reference image set for this cell')
    #         return -1
    #     ref = self.parent.mouse.get_image(ref[0])
    #     img = self.parent.mouse.get_image(self.active_image)
    #     # try to get transform with rotation, if fails to estimate, translation only
    #     result = imreg_dft.similarity(ref[:, :, 1], img[:, :, 1], numiter=5, constraints={'scale': [1., 0.1]})
    #     if 'timg' in result:
    #         self.translation_x.set(round(result['tvec'][0]))
    #         self.translation_y.set(round(result['tvec'][1]))
    #         self.rotation.set(round(result['angle']))
    #     else:
    #         tr = register_translation(img, self.parent.mouse.get_image(ref[0]))[0]
    #         self.translation_x.set(tr.translation[0])
    #         self.translation_y.set(tr.translation[1])
    #         self.rotation.set(0)
    #     self.apply_transform()

    def load_transform(self):
        tr = self.parent.mouse.get_transform(self.active_image)
        if tr is None:
            self.translation_x.set(0)
            self.translation_y.set(0)
            self.rotation.set(0)
        else:
            self.translation_x.set(round(tr.translation[0]))
            self.translation_y.set(round(tr.translation[1]))
            self.rotation.set(round(numpy.rad2deg(tr.rotation)))
        self.apply_transform()

    def apply_transform(self):
        self.parent.mouse.set_transform(self.active_image,
                                        (self.translation_x.get(), self.translation_y.get(), self.rotation.get()))
        self.parent.mouse.update_cached_image(self.active_image)

    def reset_callback(self):
        self.translation_x.set(0)
        self.translation_y.set(0)
        self.rotation.set(0)
        self.apply_transform()

    def copy_callback(self):
        if self.parent.view2.active_image is None:
            return -1
        tr = self.parent.mouse.get_transform(self.parent.view2.active_image)
        self.translation_x.set(round(tr.translation[0]))
        self.translation_y.set(round(tr.translation[1]))
        self.rotation.set(round(numpy.rad2deg(tr.rotation)))
        self.apply_transform()

    def add_callback(self):
        if self.parent.mouse is not None:
            fn = askopenfilenames(title='Select image files to import', defaultextension='.tif',
                                  filetypes=[('TIFF Image', '.tif'), ('all files', '.*'), ])
            self.parent.mouse.add_lives(fn)
            for f in fn:
                items = f.split('/')
                self.listbox.insert(END, items[-1])

    def color_available(self, c):
        av = self.parent.mouse.enumerate_images(c)
        for i, fn in enumerate(self.list_values.get()):
            if i != self.active_item:
                color = ('white', 'lightgreen')[fn in av]
                if fn == self.parent.mouse.get_reference_position(c)[0]:
                    color = 'salmon'
                self.listbox.itemconfig(i, {'bg': color})

    def select_callback(self, event):
        if self.parent.listen_mouse:
            if event.x_root == -1:
                if event.y_root == 1:  # hack to reuse code for mouse wheel
                    i = event.i
                else:
                    i = self.listbox.curselection()[0]
                if self.active_item is not None:
                    self.listbox.itemconfig(self.active_item, {'bg': 'white'})
                if self.active_item != i:
                    self.active_item = i
                    self.listbox.itemconfig(i, {'bg': 'green'})
                    self.active_image = self.list_values.get()[i]
                    self.parent.view1.set_active_image(self.active_image)
                    self.load_transform()


class ZoomView(AppPanel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.size = 256
        self.factor = 4
        self.canvas = Canvas(self.frame, width=self.size, height=self.size)
        self.canvas.grid(row=self.row())
        self.fig = None

    def update(self, fig):
        self.fig = fig
        self.canvas.create_image(self.canvas.winfo_width() / 2, self.canvas.winfo_height() / 2, image=self.fig)
        self.canvas.update()


class ImgView(AppPanel):
    def __init__(self, *args, keepref=False, **kwargs):
        super().__init__(*args, **kwargs)

        self.active_image = None
        self.active_cell = None
        self.current_point = None
        self.fig = None
        self.phimobj = None
        self.cache = {}
        self.mode = IntVar()
        self.put_flag = False
        self.placed = []
        self.keepref = keepref
        self.showall = True
        self.showlayer = False
        self.zoom = None
        self.layersymbols = (('o', 'yellow'), ('sq', 'red'), ('x', 'blue'))
        modes = ['Add cell', 'Put cell', 'Move', 'Pick', 'Remove']
        row = self.row()
        for r, text in enumerate(modes):
            Radiobutton(self.frame, text=text, variable=self.mode, value=r, width=10, command=self.update,
                        indicatoron=0).grid(row=row, column=r, sticky=W)
        if keepref:  # 2 variations for the button on the 2 views
            Button(self.frame, text='Current', command=self.set_current_callback).grid(row=row, column=r + 1, sticky=W)
            self.layer_toggle = Button(self.frame, text='Layers', command=self.toggle_layer, relief='raised')
            self.layer_toggle.grid(row=row + 1, column=r + 1, sticky=N + W)
        else:
            self.all_toggle = Button(self.frame, text='Show All', command=self.toggle, relief='sunken')
            self.all_toggle.grid(row=row, column=r + 1, sticky=W)
            Button(self.frame, text='Put All', command=self.put_all_callback).grid(row=row + 1, column=r + 1,
                                                                                   sticky=N + W)

        row = self.row()
        self.textvar_image = StringVar()
        self.textvar_cell = StringVar()
        Label(self.frame, textvariable=self.textvar_image).grid(row=row, column=0, sticky=W)
        Label(self.frame, textvariable=self.textvar_cell).grid(row=row, column=1, sticky=W)
        self.canvas = Canvas(self.frame, width=796, height=512, highlightthickness=0)
        self.canvas.grid(row=self.row(), columnspan=len(modes))
        self.canvas.bind('<Double-Button-1>', self.tool_callback)
        if not keepref:
            self.canvas.bind('<Enter>', self.focus_keyboard)
            self.canvas.bind('<Key>', self.key_callback)

    def clear(self):
        self.active_image = None
        self.active_cell = None

    def bind_zoom(self, zoom):
        self.zoom = zoom

    def focus_keyboard(self, event):
        self.canvas.focus_set()

    def drawcell(self, c, p, s=5, mark='x'):
        x, y = p
        option = (c == self.active_cell)
        color = ('magenta', 'cyan')[option]
        weight = (2, 3)[option]
        if option:
            self.current_point = p
        if self.showlayer:
            layer = self.parent.mouse.get_layer_values(c)
            if layer in (0, 1, 2):
                mark, color = self.layersymbols[layer]
            else:
                color = 'grey'
        if mark == 'x':
            self.canvas.create_line(x - s, y - s, x + s, y + s, fill=color, width=weight)
            self.canvas.create_line(x - s, y + s, x + s, y - s, fill=color, width=weight)
        elif mark == 'o':
            self.canvas.create_oval(x - s, y - s, x + s, y + s, fill='', outline=color, width=weight)
        elif mark == 'sq':
            self.canvas.create_line(x - s, y - s, x + s, y - s, fill=color, width=weight)
            self.canvas.create_line(x + s, y - s, x + s, y + s, fill=color, width=weight)
            self.canvas.create_line(x + s, y + s, x - s, y + s, fill=color, width=weight)
            self.canvas.create_line(x - s, y + s, x - s, y - s, fill=color, width=weight)

    def put_temp_cell(self):
        if not self.showall:
            c = self.parent.celllist.active_cell
            if c is not None:
                if c not in self.placed:
                    s = 5
                    p = self.parent.mouse.get_reference_position(c)
                    self.current_point = p
                    if len(p) > 1:
                        x, y = p[1]
                        self.canvas.create_line(x - s, y - s, x + s, y + s, fill='grey', width=3)
                        self.canvas.create_line(x - s, y + s, x + s, y - s, fill='grey', width=3)

    def toggle(self):
        self.showall = not self.showall
        self.all_toggle.config(relief=('raised', 'sunken')[self.showall])
        self.update()

    def toggle_layer(self):
        self.showlayer = not self.showlayer
        self.layer_toggle.config(relief=('raised', 'sunken')[self.showlayer])
        self.update()

    def update(self):
        # later: check if anything changed for performance
        if self.active_image is not None:
            self.put_image()

    def set_current_callback(self):
        self.set_active_image(self.parent.view1.active_image)

    def set_active_image(self, img):
        self.active_image = img
        self.textvar_image.set(self.active_image)
        self.update()

    def get_fig(self):
        if not self.keepref:
            if self.active_image is None:
                self.active_image = self.parent.livelist.active_image
        self.textvar_image.set(self.active_image)
        self.fig = self.parent.mouse.get_image_PIL(self.active_image)

    def put_all_callback(self):
        for c, p in self.parent.mouse.enumerate_cells(self.parent.view2.active_image):
            if c not in self.placed:
                self.parent.mouse.update_cell_pos(c, (self.active_image, p))
        self.update()

    def put_image(self):
        self.active_cell = self.parent.celllist.active_cell
        self.textvar_cell.set(f'c:{self.active_cell}')
        # init figure
        self.get_fig()
        self.phimobj = ImageTk.PhotoImage(self.fig)
        self.canvas.create_image(self.canvas.winfo_width() / 2, self.canvas.winfo_height() / 2, image=self.phimobj)
        # draw cells
        self.placed = []
        for c, p in self.parent.mouse.enumerate_cells(self.active_image):
            self.placed.append(c)
            self.drawcell(c, p, mark=('x', 'sq')[self.parent.mouse.get_found(c)])
        if self.mode.get() == 1:
            self.put_temp_cell()
        if not self.keepref:
            # color buttons in cell list
            for i, c in enumerate(self.parent.celllist.list_values.get()):
                color = ('white', self.color)[c in self.placed]
                if i == self.parent.celllist.active_item:
                    color = 'blue'
                self.parent.celllist.listbox.itemconfig(i, {'bg': color})
            # add cells as circles if show all turned on
            if self.showall:
                ref = self.parent.view2.active_image
                if ref is not None:
                    for c, p in self.parent.mouse.enumerate_cells(ref):
                        if c not in self.placed:
                            self.drawcell(c, p, mark='o')
        self.canvas.update()
        self.put_zoom()

    def put_zoom(self):
        pos = self.current_point
        if pos is None:
            self.zoom.update(None)
        else:
            size = int(self.zoom.size / self.zoom.factor)
            x, y = [max(0, int(min(self.fig.size[i] - size, pos[i] - size / 2))) for i in (0, 1)]
            pic = self.fig.crop((x, y, x + size, y + size))
            self.zoom.update(ImageTk.PhotoImage(pic.resize((self.zoom.size, self.zoom.size), ImagePIL.BICUBIC)))
            # self.current_point = None

    def tool_callback(self, event):
        if self.parent.listen_mouse:
            [self.add_tool, self.put_tool, self.move_tool, self.pick_tool, self.remove_tool][self.mode.get()].__call__(
                event)
            self.update()

    def key_callback(self, event):
        if event.keysym == 'space':
            # put active cell in current position
            if self.mode.get() == 1:
                self.parent.mouse.update_cell_pos(self.active_cell, (
                    self.active_image, self.parent.mouse.get_reference_position(self.active_cell)[1]))
                self.parent.celllist.next(-1)

    def add_tool(self, event):
        point = (self.active_image, (event.x, event.y))
        self.parent.add_cell_callback(point)

    def put_tool(self, event):  # ends up same as move, setting the flag at button click defines behavior
        self.move_tool(event)
        self.parent.celllist.next(-1)

    def move_tool(self, event):
        point = (self.active_image, (event.x, event.y))
        self.parent.mouse.update_cell_pos(self.parent.celllist.active_cell, point)

    def pick_tool(self, event):
        self.parent.pick_cell(event.x, event.y, self.active_image)

    def remove_tool(self, event):
        self.parent.pick_cell(event.x, event.y, self.active_image, remove=True)


if __name__ == '__main__':
    t1 = Process(target=App)
    t1.start()
    print('Running.')
    # for test:
    # wdir = 'C://Barna//2p//alignment//'
    # mouse_id = 'Testmouse'
    # mouse = Mouse(wdir, mouse_id)
    # for i in range(5):
    #     mouse.add_cell()
    # mouse.save()
    # self = mouse
    # tr = transform.SimilarityTransform(translation=(30, 20),
    #                                    rotation=0)
    # pic = transform.warp(mouse.get_image('cckdlx_080_360_avgmax.tif'), tr)
