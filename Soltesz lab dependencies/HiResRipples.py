import ImportFirst
import numpy
from sklearn import cluster
from EventMasks import all_ripples_immobility


def get_delays(session, hires_window, param='ntr'):
    a = session
    prefix = a.prefix
    array_filename = prefix + f'_ripples_delays_{param}-{hires_window}.npy'
    if os.path.exists(array_filename):
        return numpy.load(array_filename)
    assert all([hasattr(a, x) for x in ('ripples', 'ca')])
    param = a.getparam(param)
    n_of_lines = a.rois.image.info['sz'][0]
    ripples, mask = all_ripples_immobility(a, hires_window)
    # x and y values for each ripple: x is delay in frames compared to ripple, y is F in that frame
    time_F = numpy.empty((a.ca.cells, len(ripples), 2 * hires_window, 2))
    time_F[:] = numpy.nan
    cell_delay = numpy.empty(a.ca.cells)
    for c in range(a.ca.cells):
        cell_delay[c] = a.rois.polys.data[c][:, 1].mean() / n_of_lines  # position of cell within frame
    n_found = 0
    range_x = numpy.arange(-hires_window, hires_window)
    y_values = a.getparam(param)
    for event in a.ripples.events:
        ripple_frame = a.sampletoframe(event.p[0])
        if ripple_frame not in ripples:
            continue
        frame_in_samples = a.frametosample(ripple_frame)
        # position of ripple within frame
        ripple_delay = (event.p[0] - frame_in_samples[0]) / (frame_in_samples[1] - frame_in_samples[0])
        non_run = numpy.where(a.pos.gapless[range_x + ripple_frame] < 1)
        time_F[:, n_found, non_run, 0] = numpy.expand_dims(cell_delay[:, numpy.newaxis]
                                                           + range_x[non_run] - ripple_delay, 1)
        raw_y = y_values[:, range_x[non_run] + ripple_frame]
        # substract mean to prevent outliers
        time_F[:, n_found, non_run, 1] = numpy.expand_dims((raw_y - raw_y.mean(axis=1)[:, numpy.newaxis]), 1)
        n_found += 1
    time_F = time_F[:, :n_found, ...]
    numpy.save(array_filename, time_F)
    return time_F

def get_supersampled(session, param='ntr', w_len_sec=5, fps=15.6, res_ms=10):
    hires_window = int(fps * w_len_sec) + 1
    time_F = get_delays(a, hires_window, param=param)
    return time_F
    res_multiplier = 1000 / fps / res_ms
    res_len = int(2 * hires_window * res_multiplier)
    res_F = numpy.empty(res_len)
    starts = numpy.linspace(-hires_window, hires_window, res_len)
    for i in range(res_len):
        x_mask = numpy.logical_and(time_F[..., 0] >= starts[i + res_multiplier],
                                   time_F[..., 0] < starts[i + 1 + res_multiplier])
        n_x = numpy.count_nonzero(x_mask)
        if n_x > 3:
            res_F[i] = numpy.nansum(time_F[..., 1] * x_mask) / n_x
        else:
            res_F[i] = numpy.nan
    return res_F


if __name__ == '__main__':
    import os
    from Scores import Scores
    from matplotlib import pyplot as plt

    path = 'C:\Temp'
    prefix = 'axax_124_342'
    selected_cell = 2
    os.chdir(path)
    a = Scores(prefix, tag='1')
    time_F = get_supersampled(a)
    # res_F = get_supersampled(a)
    #
    # plt.plot(res_F)
