% cd('C:\2pdata\htr3a\');
% %---------------------------
% flist=dir('*.segment');
% for f = flist.'
%     fn = f.name;
%     of = strrep(fn,'.segment','.signals');
%     if exist(of, 'file')
%         continue;
%     else
%         sbn = strrep(fn,'.segment','');
%         sbxpullsignals(sbn);
%     end
% end

function []=extract_rois(path)
    cd(path)
    flist=dir('*.segment');
    for f = flist.'
        fn = f.name;
        of = strrep(fn,'.segment','.signals');
        if exist(of, 'file')
            continue;
        else
            sbn = strrep(fn,'.segment','');
            sbxpullsignals(sbn);
        end
    end
    quit
end
