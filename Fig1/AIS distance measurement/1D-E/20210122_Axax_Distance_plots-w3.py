from ImportFirst import *
from ImageJTifStack import CFStack
from CommonFunc import restrict_selection
from scipy import stats

# path = 'X:/Barna_Confocal/axax-AIS/convert/output/'
path = 'C:/temp/axax-lm/'
max_d = 3  # match with stack batch script
y_bins = numpy.linspace(-1, 3, 50)
x_bins = numpy.linspace(0, max_d, 9)

op_fn = path + '_distance_stats.xlsx'
df = pandas.read_excel(op_fn)

plt.rcParams['font.size'] = 8
plt.rcParams['font.sans-serif'] = 'Arial'
figsize = (1.2, 1.5)
cats = ('f', 'm')
fig, ax = plt.subplots(ncols=1, sharex=True)
col_dict = {'f': get_color('mountains1'), 'm': get_color('mountains5')}
x_dict = {'f': 0, 'm': 1}
width = 0.3
col_w = 0.6
scatter_focus = 0.6

#percent by sex#
ca = ax
show_raw = True
show_box = True
markersize = 3
comp_vals = []
box_settings = {}
# box_settings['facecolor'] = 'white'

param = 'Within3'
for gi, tag in enumerate(cats):
    x_cat = x_dict[tag]
    sub = restrict_selection(df, 'Sex', tag)
    y = sub[param].values * 100
    y = y[numpy.logical_not(numpy.isnan(y))]  # exclude nan values
    # y = y_transform(y) #in case we use a scaling
    l = len(y)
    comp_vals.append(y)
    x_center = x_cat
    x = numpy.empty(l)
    for i in range(l):
        w = col_w * scatter_focus
        x[i] = numpy.random.random() * w * 0.5
        if numpy.random.random() > 0.5:
            x[i] *= -1
    if not show_raw:
        box_settings['edgecolor'] = col_dict[tag]
    # plot mean data
    if show_box:
        ca.boxplot(y, showfliers=False, whis=1, positions=[x_center], notch=False, bootstrap=5000,
                   patch_artist=not show_raw, widths=col_w, medianprops={'color': 'black'},
                   boxprops=box_settings)
    # plot raw data
    if show_raw:
        ca.scatter(x_center + x, y, color=col_dict[tag], marker='o', s=markersize)

ca.set_ylabel('% within 3 microns')
ca.set_ylim(0, ca.get_ylim()[1])
ca.set_xticks([0, 1])
mw = stats.mannwhitneyu(*comp_vals)
print(f'U test for groups: {param} p={mw[1]:.5f}, U={mw[0]:.2f}, n={sum([len(x) for x in comp_vals])}')

# for ca in ax:
if True:
    ca.spines['right'].set_visible(False)
    ca.spines['top'].set_visible(False)
    ca.spines['bottom'].set_visible(False)
    ca.set_xticklabels(list(cats))
    ca.set_xlabel('Sex')

# ofn = 'ais-dist-boxplot-w3'
# fig.set_size_inches(*figsize)
# plt.tight_layout()
# fig.savefig(path + ofn + '.png', dpi=300)
# fig.savefig(path + ofn + '.svg')


