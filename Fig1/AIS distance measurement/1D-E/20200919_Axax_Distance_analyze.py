from ImportFirst import *
from ImageJTifStack import CFStack

path = 'X:/Barna_Confocal/axax-AIS/convert/output/'
tif_path = 'X:/Barna_Confocal/axax-AIS/convert/'
suffix = '.tif'
res_im_suffix = '_distance_image.npy'
stack_suffix = '_rgb_distance.tif'
rgb_suffix = '_rgb_d_mip.tif'
pospix_suffix = '_ais_pixels.tif'
max_d = 2.98  # match with stack batch script. make it 2.98 bc of rounding error of pixel conversion, max values of 3 are actually 2.981875
y_bins = numpy.linspace(-1, 3, 50)
x_bins = numpy.append(numpy.linspace(0, max_d, 10), 3)

hist_all = numpy.zeros((len(x_bins) - 1, len(y_bins) - 1))

df = pandas.DataFrame()
op_fn = path + '_distance_stats.xlsx'


def get_sex(fn):
    mouse_n = int(fn.split('-')[0][2:])
    if mouse_n > 260:
        return mouse_n, 'm'
    else:
        return mouse_n, 'f'


n_images = 0
for fn in os.listdir(path):
    if not fn.endswith(res_im_suffix):
        continue
    n_images += 1
    print(n_images, fn)
    res_im = numpy.load(path + fn)  # 0: distance, 1: intensity
    tif = CFStack(tif_path + fn.replace(res_im_suffix, suffix))
    # intensity image pseudo colored by distance
    # 2d histo of z sored intensity and distance
    # extract values into 2-d array
    y = res_im.reshape(res_im.shape[0], -1)
    incl = numpy.logical_not(numpy.isnan(y[0]))
    d = y[0, incl]
    # get raw intensities from tiff
    raw_v = tif.im.reshape(res_im.shape[0], -1)  # 0:ais, 1:mcherry
    v = raw_v[1, incl].astype('float')
    # z score intensity
    normvals = raw_v[1].astype('float')
    v -= normvals.mean()
    normvals -= normvals.mean()
    v /= numpy.std(normvals)
    # calc histogram
    hist_all += numpy.histogram2d(d, v, bins=[x_bins, y_bins])[0] / len(v)
    # compute stats for output
    mouse, sex = get_sex(fn)
    op = pandas.DataFrame({'File': fn, 'Mouse': mouse, 'Sex': sex}, index=[n_images])
    # percent of voxels within 3 microns
    incl = d < 2.9
    op['Within3'] = numpy.count_nonzero(incl) / len(d)
    # weigted average distance of these
    nonzero_weights = v - v.min()
    op['Mean.D'] = d[incl].mean()
    op['Weighted.D'] = numpy.average(d[incl], weights=nonzero_weights[incl])
    df = df.append(op)
#     break
# assert False

df.to_excel(op_fn)
plt.rcParams['font.size'] = 8
plt.rcParams['font.sans-serif'] = 'Arial'
figsize = (2, 2)
fig, ax = plt.subplots()
ca = ax
mappable = ca.imshow((hist_all / n_images).transpose(), cmap='plasma', interpolation='nearest', aspect='auto')
ca.invert_yaxis()
# ca.set_xticks(numpy.linspace(0, len(x_bins) - 1, 4) - 0.5)
# ca.set_xticklabels([0, 1, 2, '3+'])
xticks = [0, 1, 2, 3]
ca.set_xticks([x * 3 - 0.5 for x in xticks])
ca.set_xticklabels(xticks)
y_show = numpy.linspace(0, len(y_bins) - 1, 5)
ca.set_yticks(y_show - 0.5)
ca.set_yticklabels([-1, 0, 1, 2, 3])
ca.set_xlabel('Distance from AIS (micron)')
ca.set_ylabel('Fluorescence intensity (z)')
plt.colorbar(mappable, ticks=[])
ofn = 'density_plot-plasma-nearest'
fig.set_size_inches(*figsize)
plt.tight_layout()
fig.savefig(path + ofn + '.png', dpi=300)
fig.savefig(path + ofn + '.svg')
