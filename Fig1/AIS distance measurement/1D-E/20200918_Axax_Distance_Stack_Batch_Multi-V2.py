from ImportFirst import *
from tifffile import TiffFile, imsave
import pysal
import scipy
import cv2
from scipy import ndimage as ndi
from multiprocessing import Queue, freeze_support, Process
from datetime import datetime


class CFStack:
    def __init__(self, fn):
        self.tf = TiffFile(fn)
        self.im = self.tf.asarray()
        self.nz, self.nc, self.w, self.h = self.im.shape  # dimensions
        self.x, self.y, self.z = 0, 0, 0  # resolutions
        self.meta = self.tf.imagej_metadata
        # parse resolution
        info_fields = self.meta['Info'].split('\n')
        scale_key = 'Experiment|AcquisitionBlock|AcquisitionModeSetup|'
        dim_keys = {'x': 'ScalingX', 'y': 'ScalingY', 'z': 'ScalingZ'}
        for i in info_fields:
            if scale_key in i:
                for key, value in dim_keys.items():
                    if value in i:
                        v = float(i.split('=')[1].strip())
                        setattr(self, key, v * 10e5)  # microns


class DMWorker(Process):
    def __init__(self, queue, res_queue):
        super(DMWorker, self).__init__()
        self.queue = queue
        self.res_queue = res_queue

    def run(self):
        for data in iter(self.queue.get, None):
            self.res_queue.put(dm_compute(data))

def dm_compute(data):
    b_tree, ais_tree, dmax, wh, z_squeeze, x0, x1, y0, y1, res_im = data
    sparse_dm = b_tree.sparse_distance_matrix(ais_tree, dmax, output_type='ndarray')
    for t in sparse_dm:  # extract bouton point index and min distance
        p = wh[t[0]]
        index_tuple = (int(round(p[0] / z_squeeze)), int(p[1]), int(p[2]))
        res_im[index_tuple] = min(res_im[index_tuple], t[2])
    return (x0, x1, y0, y1, res_im)

# path = 'X:/axax-lm/'
path = 'X:/Barna_Confocal/axax-AIS/convert/'
output = path + 'output/'
suffix = '.tif'

nworker = 0
ncores = 7

if __name__ == '__main__':
    freeze_support()
    request_queue = Queue()
    result_queue = Queue()

    for fn in os.listdir(path)[::-1]:
        if not fn.endswith(suffix):
            continue
        output_fn = output + fn.replace(suffix, '_distance_image.npy')
        if os.path.exists(output_fn):
            continue
        # fn = 'BD255-L-pIKBA-Cherry-60x-a-crop.tif'
        # output_fn = path + 'test.npy'
        print(datetime.now(), 'Loading image:', fn)
        im = CFStack(path + fn)
        # positive pixel detection, serial 2D
        sz = im.w, im.h
        z_squeeze = im.z / im.x  # for isotropic
        pos_lim = 50  # number of positive px per kernel to compute tile
        pospix_fn = path + fn + '_pospix_3D_v2.npy'
        if os.path.exists(pospix_fn):
            pospix = numpy.load(pospix_fn)
        else:
            kernelsize = int(2 / im.x)  # microns
            ais_size = int((1 / im.x) ** 2)  # 1 sq micron
            wmx = pysal.lat2W(kernelsize, kernelsize)
            op = numpy.zeros((im.nz, *sz), dtype='bool')
            th_input = im.im[:, 0]
            scaleval = th_input.max()
            m = th_input / scaleval
            thr, _ = cv2.threshold((m.flatten() * 255).astype('uint8'), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            for z in range(im.nz):
                m = im.im[z, 0] / scaleval
                inp = cv2.medianBlur((m * 255).astype('uint8'), 3)
                th_b = max(thr, cv2.threshold(inp, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0], 22)
                # th3 = numpy.logical_and(inp > th_b,
                #                         cv2.adaptiveThreshold(inp, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,
                #                                               511, -th_b))
                # measure tiles for lisa
                # offset in 2 iterations to avoid false negs due to tiling
                for trim in (0, kernelsize // 2):
                    for xt in range(0, m.shape[0] - kernelsize - trim, kernelsize):
                        for yt in range(0, m.shape[1] - kernelsize - trim, kernelsize):
                            x_slice = slice(xt + trim, xt + kernelsize + trim)
                            y_slice = slice(yt + trim, yt + kernelsize + trim)
                            y = inp[x_slice, y_slice]
                            y_incl = y > thr
                            if y_incl.sum() > pos_lim:
                                # detect pos pixels with LISA
                                lisa = pysal.Moran_Local(y, wmx)
                                lisa_p = lisa.p_sim.reshape((kernelsize, kernelsize)) < 0.05
                                lisa_z = lisa.z.reshape((kernelsize, kernelsize)) > 0
                                op[z, x_slice, y_slice] = numpy.maximum(op[z, x_slice, y_slice],
                                                                        numpy.logical_and(lisa_p, lisa_z, y_incl))
                # filter sizes
                fill_objects = ndi.binary_fill_holes(op[z])
                label_objects, nb_labels = ndi.label(fill_objects)
                sizes = numpy.bincount(label_objects.ravel())
                mask_sizes = sizes > ais_size
                mask_sizes[0] = 0
                op[z] = mask_sizes[label_objects]
            numpy.save(pospix_fn, op)
            imsave(output + fn + '_pospix_3D_v2.tif', op)
            pospix = op

        # now detect bouton objects. serial 2D again, but with stack threshold
        th_input = im.im[:, 1]
        scaleval = th_input.max()
        m = th_input / scaleval
        thr, _ = cv2.threshold((m.flatten() * 255).astype('uint8'), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        obj_size = int((1 / im.x) ** 2)  # 1 sq micron
        intensity_vol = numpy.zeros((im.nz, *sz), dtype='float')
        binary_vol = numpy.zeros((im.nz, *sz), dtype='bool')
        for z in range(im.nz):
            m = im.im[z, 1] / scaleval
            inp = (m * 255).astype('uint8')
            # create threshold to zero
            th_b = max(thr, cv2.threshold(inp, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0], 22)
            _, th_im = cv2.threshold(inp, thr, 255, cv2.THRESH_TOZERO)
            # filer small objects
            fill_objects = ndi.binary_fill_holes(th_im.astype('bool'))
            label_objects, nb_labels = ndi.label(fill_objects)
            sizes = numpy.bincount(label_objects.ravel())
            mask_sizes = sizes > obj_size
            mask_sizes[0] = 0
            cleaned = mask_sizes[label_objects]
            intensity_vol[z] = th_im
            binary_vol[z] = cleaned

        # for each bouton pixel, draw distance, intensity in output (sparse matrix)
        res_im = numpy.empty((2, im.nz, *sz))
        res_im[:] = numpy.nan
        max_dist = 3  # micron
        dmax = int(max_dist / im.x)
        idx = numpy.where(binary_vol)
        res_im[1, idx[0], idx[1], idx[2]] = intensity_vol[idx]
        res_im[0, idx[0], idx[1], idx[2]] = dmax

        # get coordinates of positive pixels. for performance of nn,
        # do this parallel in overlapping chunks (so edges included in measurement)
        chunk_size = 20  # micron
        chunk_step = int(chunk_size / im.x)
        print('Starting chunked distance matrix')
        proc_bloks = 0
        done_bloks = 0
        for x_chunk in range(0, im.w, chunk_step):
            # print(f'x = {x_chunk} of {im.w}')
            x0 = max(0, x_chunk - dmax)
            x1 = min(im.w, x_chunk + chunk_step + dmax)
            for y_chunk in range(0, im.h, chunk_step):
                y0 = max(0, y_chunk - dmax)
                y1 = min(im.h, y_chunk + chunk_step + dmax)
                # bouton pixels (if any)
                idx = numpy.where(binary_vol[:, x0:x1, y0:y1])
                if len(idx[0]) > 0:
                    wh = numpy.asarray(idx).transpose().astype('float')
                    wh[:, 0] *= z_squeeze  # isotropic distance
                    b_tree = scipy.spatial.cKDTree(wh)
                    # ais pixels
                    ais_wh = numpy.asarray(numpy.where(pospix[:, x0:x1, y0:y1])).transpose().astype('float')
                    ais_wh[:, 0] *= z_squeeze
                    ais_tree = scipy.spatial.cKDTree(ais_wh)
                    if nworker < ncores:
                        DMWorker(request_queue, result_queue).start()
                        nworker += 1
                    # def dm_compute(b_tree, ais_tree, dmax, z_squeeze, x0, y0, res_im):
                    request_queue.put((b_tree, ais_tree, dmax, wh, z_squeeze, x0, x1, y0, y1, res_im[0, :, x0:x1, y0:y1]))
                    proc_bloks += 1
        print(datetime.now(), f'All blocks queued: {proc_bloks}')
        for x0, x1, y0, y1, ret_im in iter(result_queue.get, None):
            done_bloks +=1
            print(datetime.now(), f'Block {x0}, {y0} returned, {done_bloks} of {proc_bloks}')
            res_im[0, :, x0:x1, y0:y1] = numpy.minimum(res_im[0, :, x0:x1, y0:y1], ret_im)
            if done_bloks == proc_bloks:
                break
        res_im[0] *= im.x  # convert distances to microns

        # d2 histogram of dist vs intensity
        # plt.imshow(res_im[0, 10], vmin=0, vmax=max_dist, cmap='cool')
        # plt.hist2d(res_im[0, idx[0], idx[1]], res_im[1, idx[0], idx[1]], cmap='inferno', bins=30)
        numpy.save(output_fn, res_im)
        print(datetime.now(), fn, 'done.')
