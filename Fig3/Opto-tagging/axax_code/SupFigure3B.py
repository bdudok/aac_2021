#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 10 11:40:19 2021

@author: ernie
"""
import os, math
import spikeextractors as se
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

filepath = os.path.realpath(__file__)
os.chdir(filepath+'/Python')
import DirsInput as di
import helper as h
import DataRoutine as dr
from Processing.Ripples import Ripples

def ThetaPhaseDist(recording,spkind,sampfreq,ripples,run_index):
    max_chan = Get_ripple_channel(ripples)
    
    # Get theta phase distribution of spikes
    lfp = np.squeeze(recording.get_traces(channel_ids=max_chan))
    theta_phase = thetaPhase(lfp,sampfreq)
    spkind_log = np.zeros(len(lfp),dtype=bool)
    spkind_log[spkind] = True
    spkphase = theta_phase[run_index & spkind_log]
    r_vecL = ResultantVectorLength(spkphase)
    phaseAxis = np.linspace(-math.pi,math.pi,20)
    return phaseAxis,spkphase,r_vecL

def adjustFrame(ax):
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.autoscale(enable=True, axis='both', tight=True) 

def Get_ripple_channel(ripples:object):
    ch_ids = ripples.channelID_sorted[0]
    return ch_ids

def GetRunPeriod(folder_dir,sampfreq,sessionID,outfd,ADCchannels=[2,3],downSampling=1000,Upsampe=True):
    # obtain data from rotary encoder
    if len(ADCchannels) == 2:
        A = dr.loadADC(folder_dir, channel=ADCchannels[0])
        B = dr.loadADC(folder_dir, channel=ADCchannels[1])
        # Decode position
        pos = h.ReadRotaryEncoder(A, B)
    elif len(ADCchannels) == 1:
        A = dr.loadADC(folder_dir, channel=ADCchannels[0])
        # Decode position
        pos = h.ReadRotaryEncoder_oneCH(A)
        
    # Decode speed    
    speed = np.append(np.array([0.]), np.diff(pos))*sampfreq
    # Downsample position and speed
    pos_ds = signal.resample_poly(pos, 1, downSampling)
    speed_ds = signal.resample_poly(speed, 1, downSampling)
    
    # find run start and stop
    gapless, smspd = h.compute_distances(speed_ds,sampfreq=int(sampfreq/downSampling))
    starts_ds, stops_ds = h.startstop(gapless, smspd, speed_ds, pos_ds, sampfreq=int(sampfreq/downSampling))    
    
    # Upsample to original sampling rate
    if Upsampe:
        starts = [start * downSampling for start in starts_ds]
        stops = [stop * downSampling for stop in stops_ds]    
        upsample_ind = np.repeat(np.arange(0,len(gapless)),downSampling)[:len(pos)]
        gapless = gapless[upsample_ind]
    else:
        starts = starts_ds
        stops = stops_ds
        pos = pos_ds
    return starts, stops, gapless, pos

def thetaPhase(lfp,sampfreq):
    b, a = signal.butter(2, [5,10], btype='band', fs=sampfreq)
    lfp_filtered = signal.filtfilt(b,a,lfp)
    lfp_analytical = signal.hilbert(lfp_filtered)
    theta_phase = np.angle(lfp_analytical)
    return theta_phase
    
def ResultantVectorLength(angle):
    r = np.sum(np.exp(1j*angle))
    r = np.abs(r)/len(angle)
    return r  
  
def ExampleUnit(recording,sorting,UnitID,outfd,dir1,ripples,
                t_win,onsets,run_index):
    sorting_example = se.SubSortingExtractor(sorting,unit_ids=[UnitID])
    sampfreq = recording.get_sampling_frequency()
    spkind = sorting_example.get_unit_spike_train(UnitID)
    
    phaseAxis,spkphase,r_vecL = ThetaPhaseDist(recording,spkind,sampfreq,ripples,run_index)
    # Repeat one cycle for illustration purpose
    phaseAxis = np.concatenate((phaseAxis,phaseAxis+2*math.pi),axis=0)
    spkphase = np.concatenate((spkphase, spkphase+2*math.pi),axis=0)
    
    fig = plt.figure(figsize=[5,5])
    gs = GridSpec(4, 3)
    # make raster plot for example unit
    ax2 = fig.add_subplot(gs[0,:-1])
    h.raster_plot(sorting_example, [UnitID], onsets, sampfreq, t_win, [ax2])        
    ax2.set_ylabel('Trials')
    adjustFrame(ax2) 
    # make peri stimulus time histogram
    ax3 = fig.add_subplot(gs[1:,:-1], sharex=ax2)
    h.PSTH_plot(sorting_example, [UnitID], onsets, sampfreq, t_win, [ax3],bin_width = 0.5)
    ax3.set_xlabel('Time (s)')
    ax3.set_ylabel('Spike rate (/s)')
    adjustFrame(ax3) 
    # make theta trace
    ax4 = fig.add_subplot(gs[0,-1])
    ax4.plot(phaseAxis,np.cos(phaseAxis))
    ax4.axis('off')
    # make histogram of theta phase distribution
    ax5 = fig.add_subplot(gs[1:,-1],sharex=ax4)    
    ax5.hist(spkphase, bins=phaseAxis, density=True)
    ax5.set_xlabel('Theta phase (rad)')
    ax5.set_ylabel('Proportion')
    ax5.set_title('r='+str(round(r_vecL,2)))
    adjustFrame(ax5)     
    fname = outfd+'/'+di.MiceID(dir1)+'_'+di.SessionID(dir1)+'_Unit'+str(UnitID)
    plt.tight_layout()
    plt.savefig(fname+'.pdf',format='pdf')
    plt.close()
    
if __name__ == "__main__":      
    dir1='/EH18_001_2020-05-15_09-52-43'
    ExampleUnitID = 5
    outfd = di.analysisfd('AxaxProject_opto')+'/runStartStop' 
    if not os.path.isdir(outfd):
        os.mkdir(outfd)    
    sessionID = di.MiceID(dir1) + '_' + di.SessionID(dir1)
    folder_dir = di.datafd('AxaxProject_opto')+dir1
    phy_folder = folder_dir+'/phy2'
    
    recording, sorting, sampfreq = dr.loadSpikeData(phy_folder,reload=True)  
    ripples = Ripples(dir1,outputfd=di.analysisfd('AxaxProject_opto'))
    starts, stops, gapless, pos = GetRunPeriod(folder_dir,sampfreq,sessionID,outfd)
    ExampleUnit(recording,sorting,ExampleUnitID,outfd,dir1,ripples,t_win=5,onsets=starts,run_index=gapless) 