import os
import spikeextractors as se
import spikeinterface.toolkit as st
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.signal
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

filepath = os.path.realpath(__file__)
os.chdir(filepath+'/Python')
import DirsInput as di
import helper as h
import DataRoutine as dr

plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

def ExampleUnit(recording:object,sorting:object,ExampleUnitID:int,
                ch_order:list,dir1:str,outfd:str,onsets:list,offsets:list,
                t_win=0.15,all_chanel=True):
    sampfreq = recording.get_sampling_frequency()
    sorting_example = se.SubSortingExtractor(sorting,unit_ids=[ExampleUnitID])
    max_ch = st.postprocessing.get_unit_max_channels(recording, sorting_example,peak='neg')

    # Get spike waveform
    wf_all = st.postprocessing.get_unit_waveforms(recording, sorting_example, ms_before=1, ms_after=2,
                                              channel_ids=max_ch,max_spikes_per_unit=np.inf,recompute_info=True)
    fig = plt.figure(figsize=[5,5])
    gs = GridSpec(2, 1)
    # make raster plot for example unit
    ax2 = fig.add_subplot(gs[0])
    h.raster_plot(sorting_example, [ExampleUnitID], onsets, sampfreq, t_win, [ax2],sortOnset_win=[])        
    ax2.set_ylabel('Trials')
    ax2.axvspan(0,.02,color='b',alpha=0.2)
    plt.autoscale(enable=True, axis='xy', tight=True)       
    # make peri stimulus time histogram
    ax3 = fig.add_subplot(gs[1], sharex=ax2)    
    h.PSTH_plot(sorting_example, [ExampleUnitID], onsets, sampfreq, t_win, [ax3])
    axins3 = inset_axes(ax3, width="30%", height="50%")
    spktrain = np.array(sorting_example.get_unit_spike_train(unit_id=ExampleUnitID),dtype=int)
    waveform_stim,waveform_base = compare_spike_waveforms(wf_all[0][:,0,:],spktrain,onsets,offsets)
    axins3.plot(np.mean(waveform_stim,axis=0),color='blue',alpha=0.5)
    axins3.plot(np.mean(waveform_base,axis=0),color='black',alpha=0.5)
    axins3.plot([0,0],[0,100],'k')
    axins3.plot([0,30],[0,0],'k')
    axins3.axis('off')
    ax3.set_xlabel('Time (s)')
    ax3.set_ylabel('Spike rate (/s)')
    fname = outfd+'/'+di.MiceID(dir1)+'_'+di.SessionID(dir1)+'_ExampleUnit_unit'+str(ExampleUnitID)
    plt.savefig(fname+'.pdf',format='pdf')
    plt.close()
    
def compare_spike_waveforms(wf:np.array,spktrain:np.array,onsets:list,offsets:list):    
    stim_in = np.zeros(len(spktrain),dtype=bool)
    for ind, (onset,offset) in enumerate(zip(onsets,offsets)):
        if any((spktrain>=onset) & (spktrain<=offset)):
            stim_in[ind] = True
    waveform_stim = wf[stim_in,:]
    waveform_base = wf[~stim_in,:]
    return waveform_stim, waveform_base

def FRplot_session(meanFR_norm,taxis,unit_group,outfd,dir1,stim_dur=0.02):        
    sortInd = sorted(range(len(unit_group)), key=lambda k: unit_group[k])       
    tdiff = (taxis[1]-taxis[0])/2
    fig, axes = plt.subplots(1,1,figsize=[5,5],constrained_layout=True)        
    im = axes.imshow(meanFR_norm[sortInd,:],aspect='auto',interpolation='nearest',
                extent=(taxis[0]+tdiff,taxis[-1]+tdiff,1-0.5,meanFR_norm.shape[0]+0.5),origin='lower',cmap='hot')
    axes.plot([0,stim_dur],[1,1],color='C0',linewidth=5)
    axes.set_xlabel('Time (s)')
    axes.set_ylabel('Unit ID')
    title_str = []
    for temp in set(unit_group):
        count = unit_group.count(temp)
        title_str.append((temp,count))
    axes.set_title(title_str)
    cbar = axes.figure.colorbar(im, ax=axes)
    cbar.set_label('FR (z)')
    fname = outfd+'/FRChange_'+di.MiceID(dir1)+'_'+di.SessionID(dir1)
    plt.savefig(fname+'.pdf',format='pdf')
    plt.close()
    
def normalizedFR_stim_trig(recording, sorting, sampfreq, cluster_id_good, onsets,
                           t_win=0.2, bin_width=0.001, t_win_edge=0.05):
    nSpike_thr = 0    
    # compute mean firing rate aligned by light stimulus
    taxis = np.arange(-t_win-t_win_edge,t_win+t_win_edge+bin_width,bin_width)
    tindex = (taxis*sampfreq).astype('int')
    meanFR = np.zeros([len(cluster_id_good), len(taxis)-1])
    mask = np.zeros(len(cluster_id_good), dtype=bool)
    for ind, unit in enumerate(cluster_id_good):
        spktrain = sorting.get_unit_spike_train(unit).astype('int64')
        if len(spktrain) > nSpike_thr:
            mask[ind] = True
        spkdiff = np.expand_dims(spktrain,axis=1)-np.expand_dims(onsets,axis=1).T
        spkdiff = spkdiff[abs(spkdiff)<=(t_win+t_win_edge)*sampfreq]            
        # exclude units without spikes near time of interest
        if len(spkdiff) == 0:
            mask[ind] = False
        ## smoothing method 1: kernel density smoothing
        meanFR[ind,:] = kernel_smooth(spkdiff,tindex[:-1], sigma=5*bin_width*sampfreq)

    t_ind = (taxis>=-t_win) & (taxis<t_win+bin_width)
    start_twin = np.where(t_ind)[0][0]
    end_twin = np.where(t_ind)[0][-1]

    meanFR_above = meanFR[mask,start_twin:end_twin]/(bin_width*len(onsets))
    maxFR = np.max(meanFR_above,axis=1,keepdims=True)
    meanFR_norm = scipy.stats.zscore(meanFR_above,axis=1)
    cluster_id_good = np.array(cluster_id_good)
    unitIDs = cluster_id_good[mask]
    return meanFR_norm, meanFR_above, taxis[start_twin:end_twin], unitIDs, mask

def kernel_smooth(signal,axis_bin,sigma=5,normalized=True,half=False):
    if len(signal.shape)==1:
        signal = np.expand_dims(signal,axis=1)
    if len(axis_bin.shape)==1:
        axis_bin = np.expand_dims(axis_bin,axis=1)
    delta = (signal-axis_bin.T).astype('float')
    if half:
       delta[delta<0] = np.inf 
    W = np.exp(-0.5*delta*delta/sigma**2)
    if normalized:
        norm = np.sum(W,axis=1,keepdims=True)
        norm[norm==0]=1
        W = W/norm
    signal_s = np.sum(W,axis=0)
    return signal_s 

if __name__ == "__main__": 
    outfd = di.analysisfd('AxaxProject_opto')+'/OptoStimulation'
    if not os.path.isdir(outfd):
            os.mkdir(outfd)
    # Load CellInfo
    cellInfofile = di.analysisfd('AxaxProject_opto')+'/CellInfo.csv'
    CellInfo = pd.read_csv(cellInfofile)
    dirs=['/EH31_004_2020-10-09_10-57-59','/EH18_001_2020-05-15_09-52-43','/EH20_001_2020-06-07_12-38-05']
    ExampleUnitIDs=[24,5,None]
    dur_thr = 0.02
    for dir1,ExampleUnitID in zip(dirs,ExampleUnitIDs):
        folder_dir = di.datafd('AxaxProject_opto')+dir1
        phy_folder = folder_dir+'/phy2'

        recording_filtered, sorting, sampfreq = dr.loadSpikeData(phy_folder)
        ch_order = recording_filtered.get_channel_ids()
        # read cluster group info
        cluster_id_good = dr.selectCluster(phy_folder)          
        # obtain stimulation trace
        signal = dr.loadADC(folder_dir, channel=0)    

        # find onsets and offsets  
        onset_all, offset_all = h.signal_timing(signal)    
        # combine a train of pulse into one event
        onsets, offsets = h.combineEvents(onset_all, offset_all, isi_thr=.2, sampfreq=sampfreq)

        stim_duration = np.round((offsets-onsets)/sampfreq,3)
        onsets = onsets[stim_duration>=dur_thr]
        offsets = offsets[stim_duration>=dur_thr]
        if ExampleUnitID is not None:
            ExampleUnit(recording_filtered,sorting,ExampleUnitID,
                        [0],dir1,outfd,onsets,offsets,t_win=0.15,all_chanel=False)
        else:
            OptoTagSessionID, OptoTagUnitIDs = di.optoTaggedUnit('AxaxProject_opto')
            sessionID = di.MiceID(dir1) + '_' + di.SessionID(dir1)
            cluster_id_good = dr.selectCluster(phy_folder)
            # compute stimulus triggered firing rate
            meanFR_norm, meanFR_array, taxis, unitIDs, mask = normalizedFR_stim_trig(recording_filtered, sorting, sampfreq, 
                                                     cluster_id_good, onsets, t_win=0.15)
            # Extract unit type from CellInfo
            ind=CellInfo['Session_full_name']==dir1[1:]
            df = CellInfo.loc[ind,['cell_ID','unit_type']]
            unit_type = [None]*len(unitIDs)
            tagged = np.zeros(len(unitIDs),dtype=bool)
            for idx,unitID in enumerate(unitIDs):
                ind = df['cell_ID']==unitID
                df_sub = df.loc[ind]
                unit_type[idx] = df_sub.iloc[0]['unit_type']
                if (sessionID,unitID) in zip(OptoTagSessionID,OptoTagUnitIDs):
                    tagged[idx] = True
            # plot stimulus triggered firing rate for one session
            unit_group = unit_type.copy()
            for ind in range(len(unit_group)):
                if tagged[ind]:
                    unit_group[ind]='AAC'
            FRplot_session(meanFR_norm,taxis,unit_group,outfd=outfd,dir1=dir1)