import numpy, scipy, os, pandas
from matplotlib import pyplot as plt
from scipy.signal import bessel, hilbert, filtfilt
from datetime import datetime
import spikeextractors as se

class Event(object):
    def __init__(self, ripples, p1, p2):
        self.r = ripples
        self.p = [int(p1), int(p2)]
        self.power = self.r.get_power(self.p)
        self.incl = True

    def flip(self):
        self.incl = not self.incl


class UI(object):
    def __init__(self):
        self.pressed = None

    def key_press_callback(self, event):
        self.pressed = event.key


class Ripples(object):
    def __init__(self, prefix, fs=30000, bands=('ripple'), config=None, enum=False, force=False, selected=None,
                 keep_ripples=False, Manual=False, ephys_channels=(1, 1), outputfd=None, recording=None,**kwargs):
        if config is None:
            config = {}
        if prefix.endswith('.ephys'):
            prefix = prefix[:-6]
        if prefix.startswith('/'):
            prefix = prefix[1:]
        if outputfd is None:
            outputfd = os. getcwd()
        self.attrs = []
        if 'ripple' in bands:
            self.attrs.extend(['ftr', 'envelope', 'trace',
                               'channelID_sorted', 'EnvelopeAboveThr_sorted'])
        if 'theta' in bands:
            self.attrs.extend(['theta', 'tft'])
        if 'sgamma' in bands:
            self.attrs.extend(['sgamma', 'sgft'])
        self.load_attrs = ['ftr', 'theta', 'sgamma']
        self.bands = bands
        self.prefix = prefix
        self.path = outputfd + '/' + prefix + '/ripples//'
        self.fs = fs
        self.param_defaults(config)
        self.channels = None
        self.channelID_sorted = None
        self.EnvelopeAboveThr_sorted = None
        self.ephys_channel_config = ephys_channels
        if not Manual:
            exist = os.path.exists(self.path)        
            if not exist:
                os.makedirs(self.path)
            if (not exist) or force:
                if (os.path.exists(self.prefix+'/phy2/Recording.bin')) or (recording is not None):
                    self.read_binary(recording=recording,selected=selected)  
                else:
                    self.read_openephys(selected=selected)                      
                #self.read_phys()
                self.gen_filt()
                if 'ripple' in bands:
                    self.run_filt()
                if 'theta' in bands:
                    self.calc_theta()
                if 'sgamma' in bands:
                    self.calc_sgamma()
                self.save()
            else:
                self.load()
            if exist and not force:
                self.load_ripples()
            elif not keep_ripples:
                self.pick_events()
            if enum:
                self.enum_ripples()
    
            # sort events based on start time - this is assumed at single ripple mask pulling
            times = numpy.array([x.p[0] for x in self.events])
            self.events = [self.events[i] for i in numpy.argsort(times)]

    def save(self):
        for attr in self.attrs:
            self.__getattribute__(attr).tofile(self.path + attr + '.np')

    def load(self):
        for attr in self.attrs:
            if attr == 'channelID_sorted':
                self.__setattr__(attr, numpy.fromfile(self.path + attr + '.np',dtype='int8'))
            elif attr == 'EnvelopeAboveThr_sorted':
                self.__setattr__(attr, numpy.fromfile(self.path + attr + '.np',dtype='float32'))
            else:
                self.__setattr__(attr, numpy.fromfile(self.path + attr + '.np'))
        for attr in self.load_attrs:
            if attr not in self.attrs:
                fn = self.path + attr + '.np'
                if os.path.exists(fn):
                    self.__setattr__(attr, numpy.fromfile(self.path + attr + '.np'))
        self.std = self.ftr.std()
    
            
    def param_defaults(self, config):
        self.lowcut = 130
        self.highcut = 200
        self.filt_order = 3
        self.tr1 = 5
        self.tr2 = 3
        self.minl = self.ms(20)
        self.maxgap = self.ms(15)
        self.mindist = self.ms(50)
        self.window = self.ms(1)
        self.width_lotr = self.ms(50)
        self.y_scale = 1
        for key, value in config.items():
            setattr(self, key, value)

    def ms(self, ms):
        return int(ms * self.fs / 1000)

    def read_phys(self):
        ch, n_channels = self.ephys_channel_config
        raw_shape = n_channels + 1
        ep_raw = numpy.fromfile(self.prefix + '.ephys', dtype='float32')
        n_samples = int(len(ep_raw) / raw_shape)
        ep_formatted = numpy.reshape(ep_raw, (n_samples, raw_shape))
        self.trace = ep_formatted[:, ch]
        self.n = n_samples
        self.ephys_all_channels = ep_formatted[:, 1:]
        # if more channels are available, load it:
        mpn = self.prefix + '.morephys'
        if os.path.exists(mpn):
            self.channels = numpy.fromfile(mpn, dtype='float32')
    
    def read_openephys(self,selected=None):
        recording = se.OpenEphysRecordingExtractor(self.prefix)   
        self.fs = recording.get_sampling_frequency()
        self.pick_ripple_channel(recording,selected)       
    
    def read_binary(self,recording=None,selected=None):
        if recording is None:
            recording = se.BinDatRecordingExtractor(self.prefix+'/phy2/Recording.bin',30000,128,'int16')   
        self.fs = recording.get_sampling_frequency()
        self.pick_ripple_channel(recording,selected)  
        
    def pick_ripple_channel(self, recording,selected=None):
        traces = recording.get_traces()
        self.gen_filt()
        EnvelopeAboveThr = numpy.zeros(traces.shape[0])
        for i, trace in enumerate(traces):
            self.trace = trace
            self.run_filt()
            tr1 = self.tr1 * self.std
            # get total number of samples above first threshold
            EnvelopeAboveThr[i] = numpy.sum(self.envelope>tr1)
        sortInd = numpy.argsort(EnvelopeAboveThr)
        sortInd = numpy.flip(sortInd)
        channelID = numpy.array(recording.get_channel_ids(),dtype='int')
        if selected is None:
            max_ch = channelID[sortInd][0]
        else:
            max_ch = selected     
        print('Using channel {:.0f}'.format(max_ch))
        self.trace = numpy.squeeze(recording.get_traces(channel_ids=[max_ch]))
        self.n = int(len(self.trace))
        self.EnvelopeAboveThr_sorted = numpy.array(EnvelopeAboveThr[sortInd],dtype='float32')
        self.channelID_sorted = numpy.concatenate(([max_ch],channelID[sortInd]))

        
    def ch_ripple_trace(self, recording, channel_id):
        self.fs = recording.get_sampling_frequency()
        self.trace = numpy.squeeze(recording.get_traces(channel_id))
        self.n = int(len(self.trace))
        self.gen_filt() 
        self.run_filt()
        self.pick_events(save_output=False)
        
    def gen_filt(self):
        highcut = self.highcut
        lowcut = self.lowcut
        order = self.filt_order
        nyq = 0.5 * self.fs
        low = lowcut / nyq
        high = highcut / nyq
        self.filter = bessel(order, [low, high], btype='band')

    def run_filt(self):
        self.ftr = filtfilt(*self.filter, self.trace)
        htr = hilbert(self.ftr)
        self.envelope = numpy.abs(htr)
        self.std = self.ftr.std()

    def calc_theta(self):
        nyq = 0.5 * self.fs
        filter = bessel(self.filt_order, [5.0 / nyq, 10.0 / nyq], btype='band')
        self.tft = filtfilt(*filter, self.trace)
        htr = hilbert(self.tft)
        self.theta = numpy.abs(htr)

    def calc_sgamma(self):
        nyq = 0.5 * self.fs
        filter = bessel(self.filt_order, [20.0 / nyq, 50.0 / nyq], btype='band')
        self.sgft = filtfilt(*filter, self.trace)
        htr = hilbert(self.sgft)
        self.sgamma = numpy.abs(htr)

    def set_up_recursive(self, calc_diff):
        self.incl = numpy.ones(self.envelope.shape, dtype='bool')
        self.pend = self.n - self.width_lotr - self.minl - self.window
        self.events = []
        if calc_diff:
            self.trace_diff = numpy.abs(numpy.diff(self.trace))
            self.diff_threshold = self.trace_diff.mean() + self.trace_diff.std() * 3

    def det_next(self, exclude_spikes=False):
        '''exclude_spikes will skip events if there's a 3 SD peak in the raw trace differential'''
        ftr = self.ftr[numpy.where(self.incl)]
        std = ftr.std()
        tr1 = self.tr1 * std
        tr2 = self.tr2 * std
        while self.incl.sum() > 1:  # find new ones until a good one is found or the trace is used up
            # find max envelope not excluded:
            p1 = numpy.argmax(self.envelope * self.incl)
            maxval = self.envelope[p1]
            # filter if second threshold is met
            if maxval < tr1:
                return None
            # walk back until envelope is up:
            # walk through envelope with window steps
            while self.envelope[p1 - self.window] > tr2:
                p1 -= self.window
                if p1 < self.window:
                    break
            # use same logic as in pick_events to detect end of peak
            p2 = p1
            while True:
                p = p2 + self.maxgap
                if self.envelope[p] > tr2:
                    p2 = p
                else:
                    for p in range(p2 + self.maxgap, p2, -self.window):
                        if self.envelope[p] > tr2:
                            p2 = p
                            break
                    break
            # exclude region:
            self.incl[p1 - self.mindist:p2 + self.mindist] = False
            print(
                f'Event {len(self.events)}; peak {(maxval / self.std):.1f} SD;'
                f' {int((p2 - p1) / (self.fs/1000))} ms; {self.count_included()} included')
            # filter if spike is present
            if exclude_spikes:
                passed_spike_test = self.trace_diff[p1:p2].max() < self.diff_threshold
            else:
                passed_spike_test = True
            # filter if minlength is met
            if passed_spike_test and p2 - p1 > self.minl:
                self.events.append(Event(self, p1, p2))
                return p1, p2

    def pick_events(self, save_output=True):
        # redefine std based on no-noise regions
        #self.std = self.ftr[numpy.where(numpy.abs(self.ftr) < self.std * 2)].std()
        # pick regions tr2-tr2 that hit tr1
        tr1 = self.tr1 * self.std
        tr2 = self.tr2 * self.std
        print(f'Treshold set: SD = {self.std}; tr1 = {self.tr1}; tr2 = {self.tr2}')
        self.events = []
        p1, pend = 0, self.n - self.width_lotr - self.minl - self.window
        while True:
            # walk through envelope with window steps
            while self.envelope[p1] < tr2:
                p1 += self.window
                if p1 > pend:
                    break
            p2 = p1
            # extend region with maxgap steps and then go backwards to find last value above tr
            while True:
                p = p2 + self.maxgap
                if p > self.n:
                    break
                if self.envelope[p] > tr2:
                    p2 = p
                else:
                    for p in range(p2 + self.maxgap, p2, -self.window):
                        if self.envelope[p] > tr2:
                            p2 = p
                            break
                    break
            # filter if second treshold is met
            for i in range(p1, p2):
                if self.envelope[i] > tr1:
                    # filter if minlength is met
                    if p2 - p1 > self.minl:
                        self.events.append(Event(self, p1, p2))
                    break
            p1 = p2 + self.window
            if p2 > pend:
                break
        self.calc_powers()
        if save_output:
            self.save_ripples()

    def calc_powers(self):
        self.powers = numpy.empty(len(self.events))
        for i, e in enumerate(self.events):
            self.powers[i] = e.power
        self.plist = numpy.argsort(-self.powers)
        print(len(self.events), 'events')
        # self.save_ripples()

    def prep_fig(self):
        self.fig = plt.subplots(2, 1, sharex=True)
        # plt.get_current_fig_manager().window.showMaximized()
        self.cursor = UI()
        self.exit = False
        self.fig[0].canvas.mpl_connect('key_press_event', self.cursor.key_press_callback)
        self.fig[0].canvas.mpl_connect('close_event', self.on_close)
        self.redraw = False

    def on_close(self, event):
        self.exit = True

    def show_event(self, i, j=None):
        # redefine std based on no-noise regions
        #self.std = self.ftr[numpy.where(numpy.abs(self.ftr) < self.std * 2)].std()
        if j is None:
            p1, p2 = self.events[i].p
            start = int(p1 - (self.fs - (p2 - p1)) * 0.5)
            stop = int(start + self.fs)
        else:
            p1, p2 = i, j
            start, stop = int(i), int(j)
        fig, axes = self.fig
        for ax in axes:
            ax.cla()
        if self.tsource:
            lcolor = 'black'
        elif self.events[i].incl:
            lcolor = 'black'
        else:
            lcolor = 'red'
        axes[0].plot(self.trace[start:stop], color=lcolor, linewidth=0.5)
        # stretch y scale
        ylim = numpy.array(axes[0].get_ylim())
        lim_m = ylim.mean()
        axes[0].set_ylim(lim_m - (lim_m - ylim[0]) * self.y_scale, lim_m + (ylim[1] - lim_m) * self.y_scale)
        axes[1].plot(self.ftr[start:stop])
        axes[1].plot(self.envelope[start:stop])
        if not self.tsource:
            axes[1].axvline(p1 - start)
            axes[1].axvline(p2 - start)
        axes[1].axhline(self.tr1 * self.std, color='grey')
        axes[1].axhline(self.tr2 * self.std, color='black')
        if self.redraw:
            plt.draw()
            self.redraw = False
        fig.show()
        try:  # to avoid displaying error message when figure closed by clicking the x
            fig.waitforbuttonpress()
            return self.cursor.pressed
        except:
            self.exit = True

    def get_power(self, p):
        p1, p2 = p
        return numpy.sum(self.envelope[p1:p2])

    def disp_ripples(self, lines, i, j=None):
        lines0, lines1, converter = lines
        self.prep_fig()
        if j is None:
            j = i + self.fs
            d = self.fs
        else:
            d = j - i
        self.tsource = True
        while not self.exit:
            bp = self.show_event(i, j)
            if bp == 'q':
                self.exit = True
                for l in lines0:
                    l.remove()
                for l in lines1:
                    l.remove()
            elif bp == 'left':
                i = max(0, i - d)
                j = max(0, j - d)
            elif bp == 'right':
                i = min(len(self.trace), i + d)
                j = min(len(self.trace), j + d)
            for l in lines0:
                l.set_xdata(converter(i))
            for l in lines1:
                l.set_xdata(converter(j))
            self.cursor.pressed = None

    def enum_ripples(self, order='power', no_save=False, select_ch=None):
        self.prep_fig()
        self.tsource = False
        if order == 'power':
            order = self.plist
        else:
            order = range(len(self.events))
        i = 0
        selected=0
        if len(order) == 0:
            print('No events')
            self.exit = True
        while not self.exit:
            bp = self.show_event(order[i])
            if bp == 'q':
                self.exit = True
                if not no_save:
                    self.save_ripples()
                if selected>0:
                    self.save()
            elif bp == 'left':
                i -= 1
                self.redraw = True
                if i < 0:
                    i = len(self.events) - 1
            elif bp == 'right':
                i += 1
                self.redraw = True
                if i > len(self.events) - 1:
                    i = 0
            elif bp == 'up' or bp == 'down':
                self.redraw = True
                self.events[order[i]].flip()
            elif bp == 'r': # recompute ripple using another channel
                 if select_ch is None:
                     selected+=1
                     channelID_selected = self.channelID_sorted[selected]
                 else:
                     channelID_selected = select_ch
                 if os.path.exists(self.prefix+'/phy2/Recording.bin'):
                    self.read_binary(selected=channelID_selected)  
                 else:
                    self.read_openephys(selected=channelID_selected)
                 self.gen_filt()
                 self.run_filt()
                 self.pick_events(save_output=False)
                 
            self.cursor.pressed = None
            self.current = i

    def rec_enum_ripples(self, exclude_spikes=True):
        self.prep_fig()
        self.tsource = False
        self.set_up_recursive(calc_diff=exclude_spikes)
        i = 0
        p = self.det_next(exclude_spikes=exclude_spikes)
        if p is None:
            print('No events')
            self.exit = True
        while not self.exit:
            bp = self.show_event(i)
            if bp == 'q':
                self.exit = True
            if bp == 's':
                self.exit = True
                self.calc_powers()
                self.save_ripples()
            elif bp == 'left':
                i -= 1
                if i < 0:
                    i = len(self.events) - 1
            elif bp == 'right':
                i += 1
                if i > len(self.events) - 1:
                    p = self.det_next()
                    if p is None:
                        print('No more events')
                        i = 0
            elif bp == 'up' or bp == 'down':
                self.redraw = True
                self.events[i].flip()
            self.cursor.pressed = None
            self.current = i

    def count_included(self):
        i = 0
        for event in self.events:
            if event.incl:
                i += 1
        return i

    def save_ripples(self):
        ts = str(datetime.now())
        ts = ts[:ts.find(' ')] + '-' + ts[ts.find(' ') + 1:ts.find('.')].replace(':', '-') + '.ripples'
        if len(self.events) < 1:
            ea = numpy.zeros((1, 2))
        else:
            events = []
            for e in self.events:
                if e.incl:
                    events.append(e.p)
            ea = numpy.empty((len(events), 2))
            for i, p in enumerate(events):
                ea[i, :] = p
        ea.tofile(self.path + ts)
        print(ts, f'saved with {self.count_included()} events.')

    def export_ripple_times(self):
        if len(self.events) > 0:
            events = []
            for e in self.events:
                if e.incl:
                    events.append(e.p)
            ea = numpy.empty((len(events), 2))
            for i, p in enumerate(events):
                ea[i, :] = p
        pandas.DataFrame(ea, columns=['RippleStart', 'RippleStop']).to_excel(self.prefix + '_ripple_times.xlsx')
        print(self.prefix, 'ripples exported.')

    def load_ripples(self, fn=None):
        if fn is None:
            flist = []
            for f in os.listdir(self.path):
                if f.endswith('.ripples'):
                    flist.append(f)
            if len(flist) == 0:
                print('No ripples')
                return -1
            else:
                flist.sort()
                fn = flist[-1]
        else:
            if not fn.endswith('ripples'):
                fn += '.ripples'
        print(fn)
        events = numpy.fromfile(self.path + fn)
        events = events.reshape((int(len(events) / 2), 2))
        self.events = []
        for p in events:
            self.events.append(Event(self, *p))
        self.calc_powers()

    def export_matlab(self):
        '''
        {'__globals__': ['fs', 's', 'total_ns', 'gn', 'of'],
         '__header__': b'MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Thu Mar 29 16:13:43 2018',
         '__version__': '1.0',
         'fs': 20000,
         'gn': array([ 0.67108864,  1.048576  ,  0.17592186,  0.11258999]),
         'of': array([ 1.1 , -0.35, -3.  , -0.8 ]),
         's': array([[ 0.0691672 ,  0.06171192, -1.3152473 ,  0.0221665 ],
                [ 0.0623602 ,  0.07273277, -1.3133024 ,  0.02281478],
                [ 0.09185719,  0.0782432 , -1.32205427,  0.02184235],
                ...,
                [-0.07410391,  0.02767693,  0.99264997,  0.02249064],
                [-0.08058677,  0.03027007,  0.98811197,  0.02281478],
                [-0.09225591,  0.02994592,  0.98324984,  0.02313893]], dtype=float32),
         'total_ns': 11994000}
        '''

        header = b'MATLAB 5.0 MAT-file, Platform: PCWIN64, Created on: Wed Jan 03 04:43:06 2018'
        version = '1.0'
        globals = ['fs', 's', 'total_ns', 'gn', 'of']
        gn = numpy.ones(4, dtype='<f8')
        s = numpy.zeros((self.n, 4), dtype='<f4')
        s[:, 0] = self.trace

        mat = {'__header__': header, '__version__': version, '__globals__': globals, 'fs': self.fs, 'gn': gn, 'of': gn,
               's': s, 'total_ns': self.n}

        scipy.io.savemat(self.prefix + '_ephysexport.mat', mat)


#
if __name__ == '__main__':
    import DirsInput as di    
    projectName = 'AxaxProject_opto'
    #projectName = 'SNCGProject'
    path = di.datafd(projectName)
    os.chdir(path)
    dirs, _ = di.dirs(projectName)
    for dir1 in dirs[2:3]:
        print('Working on the following folder: '+dir1)
        ripples = Ripples(dir1, force=True, config={'tr1': 6, 'tr2': 3, 'filt_order': 3}, 
                          outputfd=di.analysisfd(projectName)) 
