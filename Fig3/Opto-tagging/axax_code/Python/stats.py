#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 14:35:25 2020

@author: ernie
"""
import numpy as np
    
def permutationTest(data1,data2,repetitions=1000,random_state=0):
    np.random.seed(random_state)
    dataSize = data1.size
    data_combined = np.concatenate((data1,data2))
    Diff_null = np.zeros(repetitions)
    for ii in range(repetitions):
        sampleIndex = np.random.choice(data_combined.size, dataSize, replace=False) 
        boolInd = np.zeros(data_combined.size,dtype=bool)
        boolInd[sampleIndex] = True
        Diff_null[ii] = np.mean(data_combined[boolInd]) - np.mean(data_combined[np.invert(boolInd)])
    
    meanDiff = np.mean(data1) - np.mean(data2)
    if meanDiff>0:
        pvalue = np.sum(Diff_null>meanDiff)/repetitions
        sign = 1
    else:
        pvalue = np.sum(Diff_null<meanDiff)/repetitions
        sign = -1
    return pvalue,sign

def bootstrap_ci(data, repetitions = 1000, alpha = 0.05, random_state=0):
    counts = data.shape[0]
    np.random.seed(random_state)
    data_mean = np.zeros([repetitions,data.shape[1]])
    for i in range(repetitions):
        sampleIndex = np.random.choice(counts, counts)        
        data_mean[i,:] = np.mean(data[sampleIndex,:],axis=0)
    ci_upper = np.percentile(data_mean,100-alpha/2*100,axis=0)
    ci_lower = np.percentile(data_mean,alpha/2*100,axis=0)
    return ci_upper,ci_lower

def salt(spt_baseline,spt_test,dt,wn=0.01):
    """    
    Stimulus-associated spike latency test.
    
    Parameters
    ----------
    spt_baseline : binary matrix
        Discretized spike raster for stimulus-free baseline
        period. N x M binary matrix with N rows for trials and M 
        columns for spikes. Spike times have to be converted to a
        binary matrix with a temporal resolution provided in DT. The
        baseline segment has to excede the window size (WN) multiple
        times, as the length of the baseline segment divided by the
        window size determines the sample size of the null
        distribution (see below).
    spt_test : binary matrix
        Discretized spike raster for test period, i.e. after
        stimulus. N x M binary matrix with N rows for trials and M 
        columns for spikes. Spike times have to be converted to a
        binary matrix with a temporal resolution provided in DT. The
        test segment has to excede the window size (WN). Spikes out of
        the window are disregarded.
    dt : number
        Time resolution of the discretized spike rasters in seconds.
    wn : number
         Window size for baseline and test windows in seconds
         (optional; default, 0.01 s).
    
    Returns
    -------
    P : Resulting P value for the Stimulus-Associated spike Latency
        Test.
    I : Test statistic, difference between within baseline and 
        test-to-baseline information distance values.     
    
    code adapted from
    Kvitsiani D, Ranade S, Hangya B, Taniguchi H, Huang JZ, Kepecs A (2013)
    Distinct behavioural and network correlates of two interneuron types in
    prefrontal cortex. Nature
    
    """
    wn = wn*1000
    dt = dt*1000
    
    # Latency histogram - baseline
    [tno, st] = np.shape(spt_baseline)
    nmbn = round(wn/dt)
    edges = np.array(range(nmbn+1))
    nm = int(np.floor(st/nmbn))
    lsi, slsi = np.zeros((tno,nm)), np.zeros((tno,nm))
    hlsi, nhlsi = np.zeros((nmbn,nm+1)), np.zeros((nmbn,nm+1))
    Next = 0                                             
    for t in range(0,st,nmbn):
        for k in range(0,tno):
            cspt = spt_baseline[k,t:t+nmbn]
            pki = np.where(cspt==1)[0].shape[0]
            if pki == 0 :
                lsi[k,Next] = 0
            else:
                lsi[k,Next] = np.where(cspt==1)[0][0]                
        slsi[:,Next] = np.sort(lsi[:,Next])
        hst = np.histogram(slsi[:,Next],edges)[0]                               
        hlsi[:,Next] = hst
        nhlsi[:,Next] = hlsi[:,Next] / sum(hlsi[:,Next])
        Next += 1
    
    # ISI histogram - test
    tno_test = np.shape(spt_test)[0]
    lsi_tt = np.empty(tno_test)
    lsi_tt[:] = np.NaN
    for k in range(0,tno_test):
        cspt = spt_test[k,0:nmbn]
        pki = np.where(cspt==1)[0].shape[0]
        if pki == 0:
            lsi_tt[k] = 0
        else:
            lsi_tt[k] = np.where(cspt==1)[0][0]
    slsi_tt = np.sort(lsi_tt)
    hst = np.histogram(slsi_tt,edges)[0]
    hlsi[:,Next] = hst
    nhlsi[:,Next] = hlsi[:,Next] / sum(hlsi[:,Next])
    
    # JS-divergence
    kn = nm+1
    jsd = np.empty((kn,kn))
    jsd[:] = np.NaN
    for k1 in range(0,kn-1):
        D1 = nhlsi[:,k1]
        for k2 in range(k1+1,kn):
            D2 = nhlsi[:,k2]
            jsd[k1,k2] = np.sqrt(JSdiv(D1,D2)*2)
    
    # Calculate p-value and information difference
    [p, I] = makep(jsd,kn)
    return p, I
    
def makep(kld,kn):
    pnhk = kld[0:kn-1,0:kn-1]
    nullhypkld = pnhk[~np.isnan(pnhk)]
    testkld = np.median(kld[0:kn-1,kn-1])
    sno = np.shape(nullhypkld.flatten())
    p_value = sum(nullhypkld>=testkld) / sno
    Idiff = testkld - np.median(nullhypkld)
    return p_value[0], Idiff

def JSdiv(P,Q):
    if abs(sum(P.flatten())-1) > 0.00001 or abs(sum(Q.flatten())-1) > 0.00001:
        raise Exception('Input arguments must be probability distributions.')
    if P.shape != Q.shape:
        raise Exception('Input distributions must be of the same size.')    
    
    # JS-divergence
    M = (P+Q) / 2
    D1 = KLdist(P,M)
    D2 = KLdist(Q,M)
    D = (D1+D2) / 2
    return D

def KLdist(P,Q):
    if abs(sum(P.flatten())-1) > 0.00001 or abs(sum(Q.flatten())-1) > 0.00001:
        raise Exception('Input arguments must be probability distributions.')
    if P.shape != Q.shape:
        raise Exception('Input distributions must be of the same size.')
  
    # KL-distance
    P2 = P[np.multiply(P,Q)>0]     # restrict to the common support
    Q2 = Q[np.multiply(P,Q)>0]
    P2 /= sum(P2)  # renormalize
    Q2 /= sum(Q2)
    
    a = np.divide(P2,Q2)
    b = np.log(a)
    c = np.multiply(P2,b)
    D = sum(c)
    return D
    
    
    
    