#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 19:50:32 2020

@author: ernie
"""
import os
import spikeextractors as se
import spikeinterface.toolkit as st
import numpy as np
import pandas as pd
from scipy import stats, interpolate
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import zetapy

filepath = os.path.realpath(__file__)
os.chdir(filepath+'/Python')
import stats as _stats
import DirsInput as di
import DataRoutine as dr
import helper as h


def RunZeta(ProjectName='AxaxProject_opto',exp_group='experiment',window=0.1):
    if exp_group != 'experiment':
        outfd = di.analysisfd(ProjectName)+'/OptoStim_zeta_'+exp_group
    else:
        outfd = di.analysisfd(ProjectName)+'/OptoStim_zeta'
    if not os.path.isdir(outfd):
           os.mkdir(outfd)
    ZetaInfo = {}
    ZetaInfo_jt = {}
    dirs, _ = di.dirs(ProjectName,exp_group=exp_group,Rotary=None)       
    for dir1 in dirs:
        print('Working on the following folder: '+dir1)
        folder_dir = di.datafd(ProjectName)+dir1
        phy_folder = folder_dir+'/phy2'
    
        recording, sorting, sampfreq = dr.loadSpikeData(phy_folder)
        ch_order = recording.get_channel_ids()
        # read cluster group info
        cluster_id_good = dr.selectCluster(phy_folder)          
        # obtain stimulation trace
        signal = dr.loadADC(folder_dir, channel=0)    
        
        # find onsets and offsets  
        onset_all, offset_all = h.signal_timing(signal)    
        # combine a train of pulse into one event
        onsets, offsets = h.combineEvents(onset_all, offset_all, isi_thr=.2, sampfreq=sampfreq)
        # shift onsets to check false alarm rate
        jitter = np.random.randint(0.1*30000,0.38*30000,onsets.size)
        onsets_jt, offsets_jt = onsets+jitter,offsets+jitter
        
        for unitID in cluster_id_good:
            spkind = sorting.get_unit_spike_train(unitID)
            EventTimes = np.array([onsets/sampfreq,offsets/sampfreq])
            InFR,InFR_t,InFR_lat,InFR_sign,_ = getZetaValueForEachUnit(spkind,EventTimes,sampfreq,window)    
            if len(InFR) == 0:
                continue
            
            # Extract Zeta p value from Cell info (no baseline window included)
            ZetaP = extract_from_CellInfo(ProjectName, dir1, unitID, 'zeta_pvalue')
            
            
            # Jitter stimulus onsets and offsets to check for false alarm
            #EventTimes_jt = np.array([onsets_jt/sampfreq,offsets_jt/sampfreq])
            #InFR_jt,InFR_t_jt,InFR_lat_jt,InFR_sign_jt,ZetaP_jt = getZetaValueForEachUnit(spkind,EventTimes_jt,sampfreq,window)
            
            # Output
            if len(ZetaInfo) == 0:
                ZetaInfo = {'SessionID':[di.MiceID(dir1)+'_'+di.SessionID(dir1)], 
                            'unitID':[unitID],
                            'InFR':[InFR],
                            'InFR_t':[InFR_t],
                            'InFR_lat':[InFR_lat],
                            'InFR_sign':[InFR_sign],
                            'ZetaP':[ZetaP]}
                '''
                ZetaInfo_jt = {'SessionID':[di.SessionID(dir1)], 
                               'unitID':[unitID],
                               'InFR':[InFR_jt],
                               'InFR_t':[InFR_t_jt],
                               'InFR_lat':[InFR_lat_jt],
                               'InFR_sign':[InFR_sign_jt],
                               'ZetaP':[ZetaP_jt]}
                '''
            else:
                ZetaInfo['SessionID'].append(di.MiceID(dir1)+'_'+di.SessionID(dir1))
                ZetaInfo['unitID'].append(unitID)
                ZetaInfo['InFR'].append(InFR)
                ZetaInfo['InFR_t'].append(InFR_t)
                ZetaInfo['InFR_lat'].append(InFR_lat)
                ZetaInfo['InFR_sign'].append(InFR_sign)
                ZetaInfo['ZetaP'].append(ZetaP)
                '''
                ZetaInfo_jt['SessionID'].append(di.SessionID(dir1))
                ZetaInfo_jt['unitID'].append(unitID)
                ZetaInfo_jt['InFR'].append(InFR_jt)
                ZetaInfo_jt['InFR_t'].append(InFR_t_jt)
                ZetaInfo_jt['InFR_lat'].append(InFR_lat_jt)
                ZetaInfo_jt['InFR_sign'].append(InFR_sign_jt)
                ZetaInfo_jt['ZetaP'].append(ZetaP_jt)                
                '''
    InFR_sign_array = np.array(ZetaInfo['InFR_sign'])
    InFR_lat_array = np.array(ZetaInfo['InFR_lat'])
    ZetaP_array = np.array(ZetaInfo['ZetaP'])               
    # Get the largest size of time axis
    t_size=0
    for ii,temp in enumerate(ZetaInfo['InFR']):
        if temp.size>t_size:
           t_size = temp.size          
    
    # Interpolate firing rate values
    taxis = np.linspace(-window, window, t_size)
    InFR_array = np.zeros((len(ZetaInfo['InFR']),t_size))
    for ii,(infr,t) in enumerate(zip(ZetaInfo['InFR'],ZetaInfo['InFR_t'])):
        InFR_array[ii,:] = interpolate.interp1d(t, infr)(taxis)
    InFR_array = stats.zscore(InFR_array,axis=1)
    
    # Sort units using sign and latency
    InFR_array_sort = np.array([])
    InFR_sign_sort = np.array([])
    sessionID_sort = []
    unitID_sort = []
    InFR_list_sort = []
    Lat_list_sort = []
    signs = list(set(InFR_sign_array))
    for sign in signs:
        boolInd = (InFR_sign_array==sign) & (InFR_lat_array<.05) & (InFR_lat_array>.001) & (ZetaP_array<0.01)
        temp = InFR_array[boolInd,:]
        lat = InFR_lat_array[boolInd]
        sortInd = np.argsort(lat)
        SessionID_sub,unitID_sub = [ZetaInfo['SessionID'][i] for i in np.where(boolInd)[0]],[ZetaInfo['unitID'][i] for i in np.where(boolInd)[0]]
        sessionID_sort.extend([SessionID_sub[i] for i in sortInd])
        unitID_sort.extend([unitID_sub[i] for i in sortInd])
        InFR_list_sort.append(temp[sortInd,:])
        Lat_list_sort.append(lat[sortInd])
        if InFR_array_sort.size == 0:
           InFR_array_sort = temp[sortInd,:]
           InFR_sign_sort = InFR_sign_array[boolInd][sortInd]
        else:                
           InFR_array_sort = np.concatenate((InFR_array_sort,temp[sortInd,:]),axis=0)
           InFR_sign_sort = np.concatenate((InFR_sign_sort,InFR_sign_array[boolInd][sortInd]),axis=0)   
    
    # Output
    SortedData = {
        'InFR_array': InFR_array_sort,
        'InFR_sign': InFR_sign_sort,
        'sessionID': sessionID_sort,
        'Lat_list': Lat_list_sort,
        'unitID': unitID_sort,
        'taxis': taxis
        }
    dr.save_data(outfd+'/ZetaInfo.p',ZetaInfo)
    #dr.save_data(outfd+'/ZetaInfo_jitter.p',ZetaInfo_jt)
    dr.save_data(outfd+'/ZetaInfo_sorted.p',SortedData)
    ZetaInfo.pop('InFR')
    ZetaInfo.pop('InFR_t')
    ZetaInfo_csv = pd.DataFrame(ZetaInfo)
    ZetaInfo_csv.to_csv(outfd+'/ZetaInfo.csv',index=False)
    
    fig = plt.figure(figsize=[10,10])
    gs = GridSpec(3, 3)
    ax1 = fig.add_subplot(gs[:2,:2])
    ax12 = fig.add_subplot(gs[:2,-1])
    # Plot instaneous firing rate
    im = ax1.imshow(InFR_array_sort,aspect='auto',interpolation='nearest',
               extent=(taxis[0],taxis[-1],1-0.5,InFR_array_sort.shape[0]+0.5),origin='lower',vmin=-3,vmax=6)
    ax1.axhline(InFR_list_sort[0].shape[0]+1,color='white')
    cb = plt.colorbar(im, ax=[ax12],location='left')
    cb.set_label('FR (z)')
    ax12.set_axis_off()
    ax1.set_ylabel('Unit#')
    
    ax2 = fig.add_subplot(gs[-1,:2],sharex=ax1)
    colors = ['r','c']
    labels = ['+ (n=','- (n=']
    for idx,infr in enumerate(InFR_list_sort):
         ci_u,ci_l = _stats.bootstrap_ci(infr,random_state=0)
         ax2.plot(taxis,np.mean(infr,axis=0),color=colors[idx],label=labels[idx]+str(infr.shape[0])+')')
         ax2.fill_between(taxis,ci_l,ci_u,color=colors[idx],alpha=0.2)
    ax2.legend()
    ax2.set_xlabel('Time relative to light onset (s)')
    ax2.set_ylabel('Firing rate (z)')
    if (len(Lat_list_sort[0])>0) & (len(Lat_list_sort[1])>0): 
        ax3 = fig.add_subplot(gs[-1,-1])
        _violinplot(ax3,Lat_list_sort,['+','-'])
        pvalue,sign = _stats.permutationTest(Lat_list_sort[0],Lat_list_sort[1])
        ax3.set_title('Permutation test p = %.5f' %pvalue)
        ax3.set_ylabel('Latency (s)')
        ax3.yaxis.set_label_position("right")
        ax3.yaxis.tick_right()
        
    fname = outfd+'/InstaneousFR_optoUnits'
    plt.savefig(fname+'.pdf',format='pdf')
    plt.close()

def loadData(ProjectName='AxaxProject_opto',fileName='ZetaInfo_sorted.p',outfd=None):
    if outfd is None:
        outfd = di.analysisfd(ProjectName)+'/OptoStim_zeta'
    data = dr.load_data(outfd+'/'+fileName)
    return data

def getZetaValueForEachUnit(spkind,EventTimes,sampfreq,window):
    (dblZetaP, vecLatencies, sZETA, sRate) = zetapy.getZeta(spkind/sampfreq,EventTimes.T,
                                                            dblUseMaxDur=window,intLatencyPeaks=4,dblBaseline=window)
    #(dblZetaP, vecLatencies, sZETA, sRate) = zetapy.getZeta(spkind/sampfreq,EventTimes.T,intLatencyPeaks=4)
    if not isinstance(sRate, list):
        ind_t = sRate.vecT>0
        if sZETA.dblMeanD>=0:
           peakind = np.argmax(sRate.vecRate[ind_t])
           latency = sRate.vecT[ind_t][peakind]
           sign = 1
        elif sZETA.dblMeanD<0:
           troughInd = np.argmin(sRate.vecRate[ind_t])
           latency = sRate.vecT[ind_t][troughInd]
           sign = -1
        else:
            InFR = []
            InFR_t = []
            InFR_lat = []
            InFR_sign = []        
            ZetaP = []              
        
        InFR = sRate.vecRate
        InFR_t = sRate.vecT
        InFR_lat = latency
        InFR_sign = sign        
        ZetaP = dblZetaP
    else:
        InFR = []
        InFR_t = []
        InFR_lat = []
        InFR_sign = []        
        ZetaP = []        
    return InFR,InFR_t,InFR_lat,InFR_sign,ZetaP

def getOriginalFR(AllData,SortedData):
    InFR,taxis = [],[]
    for idx,unitInfo_sort in enumerate(SortedData['unitInfo_sort']):
        for idx2,unitInfo in enumerate(AllData['unitInfo']):
            if unitInfo_sort==unitInfo:
               InFR.append(AllData['InFR'][idx2])
               taxis.append(AllData['InFR_t'][idx2])
    return InFR,taxis

def baselineFR(InFR,taxis):
    InFR_baseline = np.zeros(len(InFR))
    for idx,(infr,t) in enumerate(zip(InFR,taxis)):
        InFR_baseline[idx] = infr[(t>0) & (t<.02)].size
    return InFR_baseline

def _violinplot(ax:object,data:list,labels:list):
    ax.violinplot(data,showmeans=True)
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)

def extract_from_CellInfo(ProjectName:str, dir1:str, unitID:int, featureName:str):
    CellInfo_file = di.analysisfd(ProjectName)+'/CellInfo.csv'
    if os.path.isfile(CellInfo_file):    
        CellInfo = pd.read_csv(CellInfo_file)
        ind_cellInfo = (CellInfo.loc[:,'Session_full_name'] == dir1[1:]) & (
                        CellInfo.loc[:,'cell_ID'] == unitID)                           
        newRow = CellInfo.loc[ind_cellInfo,:]
        featureValue = newRow.loc[:,featureName].to_numpy()[0]
        return featureValue

if __name__ == "__main__":  
    RunZeta()