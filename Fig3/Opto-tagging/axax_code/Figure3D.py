import os
import spikeextractors as se
import spikeinterface.toolkit as st
import numpy as np
import matplotlib.pyplot as plt
import pyopenephys as pyo
import quantities as pq

filepath = os.path.realpath(__file__)
os.chdir(filepath+'/Python')
import helper as h
import DataRoutine as dr
import DirsInput as di

plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

def EventTrigLFP(recording,onsets,ch_ids,t_win=0.1,depth_spacing=50E-6,pyr_ch_ind=0):
    sampfreq = recording.get_sampling_frequency()
    taxis = np.arange(-t_win,t_win,1/sampfreq)
    ind = np.linspace(onsets-int(t_win*sampfreq),onsets+int(t_win*sampfreq),
                      int(t_win*sampfreq)*2).astype(int).T
    inrange = ind[:,-1]<recording.get_traces(0).size
    ind = ind[inrange,:]
    lfp_data_loaded = np.zeros([len(ch_ids),len(taxis)],dtype='float')
    recording_filtered = st.preprocessing.bandpass_filter(recording=recording, freq_min=1, freq_max=300,
                                                          filter_type='butter',order=2)
    for idx,ch_id in enumerate(ch_ids):
        temp = np.squeeze(recording_filtered.get_traces(ch_id))
        lfp_data_loaded[idx,:] = np.mean(temp[ind],axis=0)
    lfp_data = lfp_data_loaded * 1E-6 * pq.V        # [uV] -> [V]
    z_data = np.linspace(0, depth_spacing*(len(ch_ids)-1), len(ch_ids)) * pq.m  # [m]

    depth_axis = np.array(z_data)*1E6-pyr_ch_ind*depth_spacing*1E6
    return taxis,depth_axis,lfp_data.magnitude*1E6

def reducebyave(arr, n):         #function to downsample LFP
    end =  n * int(len(arr)/n)
    return np.mean(arr[:end].reshape(-1, n), 1)

if __name__ == "__main__":  
    outdir = di.analysisfd('AxaxProject_opto')+'/OptoStimulation'
    parent_folder = di.datafd('AxaxProject_opto')
    dirs_in = ['/EH18_003_2020-05-16_10-12-31','/EH19_002_2020-06-08_09-27-39',
               '/EH20_002_2020-06-08_10-53-23','/EH30_003_2020-10-11_10-29-48',
               '/EH31_001_2020-10-08_12-20-44','/EH31_004_2020-10-09_10-57-59',
               '/EH31_002_2020-10-08_13-01-35','/EH31_003_2020-10-08_14-01-07',
               '/EH31_005_2020-10-09_11-42-58','/EH30_004_2020-10-11_11-06-40']
    pyr_ch_inds = [5,5,3,9,10,8,8,9,9,9]
    depthAxis_all=[]
    lfp_aligned_all=[]

    for dir_in,pyr_ch_ind in zip(dirs_in,pyr_ch_inds):
        folder = parent_folder+dir_in
        recording = se.OpenEphysRecordingExtractor(folder)
        sorting = se.PhySortingExtractor(folder_path=folder+'/phy2')
        adcFile = pyo.File(folder,'ADC',ch_selected=0).experiments[0].recordings[0]
        signal_stim = adcFile.analog_signals[0].signal[0]

        ds_factor = 30  #how much should the LFP be temporally downsampled?
        ds_fs = int(recording.get_sampling_frequency()/ds_factor)
        #downsample LFP
        signal_stim = reducebyave(signal_stim, ds_factor)
        recording = st.preprocessing.resample(recording, ds_fs)
        
        # find onsets and offsets  
        onset_all, offset_all = h.signal_timing(signal_stim)    
        # combine a train of pulse into one event
        onsets, offsets = h.combineEvents(onset_all, offset_all, isi_thr=.2, sampfreq=recording.get_sampling_frequency())

        ch_id = [0,31,1,30,15,16,14,17,8,23,9,22]
        depth_spacing = 50E-6
        taxis,depthAxis,lfp_aligned = EventTrigLFP(recording, onsets, ch_id,
                                                       depth_spacing=depth_spacing,pyr_ch_ind=pyr_ch_ind)
        depthAxis_all.append(depthAxis)
        lfp_aligned_all.append(lfp_aligned[pyr_ch_ind,:])

    lfp_aligned_all = np.array(lfp_aligned_all)
    fig, axes = plt.subplots(1,1,figsize=[3,5],constrained_layout=True)
    #axes.plot(taxis,lfp_aligned_all.T,color='k',alpha=0.3,zorder=1)
    std_err = np.std(lfp_aligned_all,axis=0)/np.sqrt(lfp_aligned_all.shape[0])
    meanvalue = np.mean(lfp_aligned_all,axis=0)
    axes.plot(taxis,meanvalue,color='k',zorder=1)
    axes.fill_between(taxis,meanvalue+std_err,meanvalue-std_err,color='k',alpha=0.2)
    ylim = axes.get_ylim()
    axes.fill_between([0,0.02],ylim[0],ylim[1],color='C0',alpha=0.3,zorder=0)
    axes.plot([-0.05, -0.04],[20, 20],'k',zorder=2)
    axes.plot([-0.05, -0.05],[20, 70],'k',zorder=2)
    axes.axis('off')
    fname = outdir+'/OptoTrigLFP_summary'
    fig.savefig(fname+'.pdf',format='pdf')
    plt.close()