from aac_analysis import *

palette = {
    'AAC': 'r',
    'PYR': 'g'
}

signal_spec = {
    'signal_type': 'transients',
    'label': 'PYR',
}

event_spec = {
    'signal_type': 'behavior',
    'event_type': 'led_context',
    'as_time_events': True
}

psth_strategy = PSTH(pre=2, post=2, dt=0.2)
responsiveness_strategy = Responsiveness(pre=2, post=2, dt=0.2)

# eNpHR data

# Calculate
enphr_trials = fetch_trials(project_name='aac', mouse_name=ENPHR_MICE, experimentType='RF_timed')
enphr_grp = ExperimentGroup.from_trials(enphr_trials, parallelize=True)
enphr_psth = enphr_grp.apply(psth_strategy, signal_spec=signal_spec, events=event_spec)
enphr_responses = enphr_grp.apply(responsiveness_strategy, signal_spec=signal_spec, events=event_spec, 
                                  groupby=['Mouse', 'ImagingExperiment', 'roi_label'], agg='mean')

# stats
stats.wilcoxon(enphr_responses['pre'], enphr_responses['post'])

# plot

plot_stim_hist(enphr_psth)
plot_boxplot(enphr_responses)

# ChRmine data

# Calculate
chr_trials = fetch_trials(project_name='aac', mouse_name=CHR_MICE, experimentType='RF_timed')
chr_grp = ExperimentGroup.from_trials(chr_trials, parallelize=True)
chr_psth = chr_grp.apply(psth_strategy, signal_spec=signal_spec, events=event_spec)
chr_responses = chr_grp.apply(responsiveness_strategy, signal_spec=signal_spec, events=event_spec, 
                                  groupby=['Mouse', 'ImagingExperiment', 'roi_label'], agg='mean')

# stats
stats.wilcoxon(chr_responses['pre'], chr_responses['post'])

# plot

plot_stim_hist(chr_psth)
plot_boxplot(chr_responses)
