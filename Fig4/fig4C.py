from aac_analysis import *

palette = {
    'AAC': 'r',
    'PYR': 'g'
}

dfof_spec = {
    'signal_type': 'dfof',
    'label': 'PYR',
}

event_spec = {
    'signal_type': 'behavior',
    'event_type': 'led_context',
    'as_time_events': True
}

tcs = SpatialTuning(nbins=100)
pvc = PopulationVectorCorrelations(nbins=100)

enphr_trials = fetch_trials(project_name='aac', mouse_name=ENPHR_MICE, experimentType='RF_fixedLED')
chr_trials = fetch_trials(project_name='aac', mouse_name=CHR_MICE, experimentType='RF_fixedLED')
ctrl_trials = fetch_trials(project_name='aac', mouse_name=CTR_MICE, experimentType='RF_fixedLED')
ctrl_trials = filter_ctrl_trials(ctrl_trials)

grp1 = ExperimentGroup.from_trials(enphr_trials, parallelize=True, name='eNpHR')
grp2 = ExperimentGroup.from_trials(chr_trials, parallelize=True, name='ChRmine')
grp3 = ExperimentGroup.from_trials(ctrl_trials, parallelize=True, name='ctrl')

cohort = CohortGroup([grp1.to_cohort(), grp2.to_cohort(), grp3.to_cohort()])

tuning_curves = cohort.apply(tcs, signal_spec=dfof_spec, time_filter=IsRunning())

# THIS TAKES A LONG TIME! TODO: cache intermediate results to speed this up, maybe write ._analysis_results.h5?
population_vectors = cohort.apply(pvc, signal_spec=dfof_spec, groupby=['Mouse'], agg='mean', time_filter=IsRunning()) # , "ImagingExperiment", 'roi_label'

print(stats.ttest_ind(population_vectors.level(Cohort='ChRmine')['pre-post'], population_vectors.level(Cohort='ctrl')['pre-post']))
print(stats.ttest_ind(population_vectors.level(Cohort='eNpHR')['pre-post'], population_vectors.level(Cohort='ctrl')['pre-post']))

sns.boxplot(data=population_vectors.reset_index(), x='Cohort')
plt.gcf().savefig(savepath + "fig4c.pdf")
