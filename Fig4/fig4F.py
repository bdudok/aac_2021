from aac_analysis import *

palette = {
    'AAC': 'r',
    'PYR': 'g'
}

dfof_spec = {
    'signal_type': 'dfof',
    'label': 'PYR',
}

event_spec = {
    'signal_type': 'behavior',
    'event_type': 'led_context',
    'as_time_events': True
}

tcs = SpatialTuning(nbins=100)
plfs = PreStimPostPlaceFields(nbins=100)

enphr_trials = fetch_trials(project_name='aac', mouse_name=ENPHR_MICE, experimentType='RF_fixedLED')
chr_trials = fetch_trials(project_name='aac', mouse_name=CHR_MICE, experimentType='RF_fixedLED')

enphrs = ExperimentGroup.from_trials(enphr_trials, parallelize=True, name='eNpHR')
chrs = ExperimentGroup.from_trials(chr_trials, parallelize=True, name='ChRmine')

enphr_result = enphrs.apply(plfs, signal_spec=signal_spec, time_filter=IsRunning())
reduced_df = place_fields2mice(enphr_result)

print(stats.ttest_rel(reduced_df.level(Pre='0', Post='1'), reduced_df.level(Pre='1', Post='0')))

chi = _to_contingency_table(reduced_df)
print(statsmodels.stats.contingency_tables.mcnemar(chi))


plot_boxplot(reduced_df)

chr_result = chrs.apply(plfs, signal_spec=signal_spec, time_filter=IsRunning())
reduced_df = place_fields2mice(chr_result)
print(stats.ttest_rel(reduced_df.level(Pre='1', Post='0'), reduced_df.level(Pre='0', Post='1')))

chi = _to_contingency_table(reduced_df)
print(statsmodels.stats.contingency_tables.mcnemar(chi))

plot_boxplot(reduced_df)
