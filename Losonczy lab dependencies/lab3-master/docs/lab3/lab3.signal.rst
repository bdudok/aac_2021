===============
``lab3.signal``
===============

.. automodule:: lab3.signal

   .. contents::
      :local:


Submodules
==========

.. toctree::

   lab3.signal.base
   lab3.signal.decomposition
   lab3.signal.dfof
   lab3.signal.filters
   lab3.signal.spikes
   lab3.signal.transients

.. currentmodule:: lab3.signal
