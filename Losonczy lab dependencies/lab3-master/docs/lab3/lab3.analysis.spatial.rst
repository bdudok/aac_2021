=========================
``lab3.analysis.spatial``
=========================

.. automodule:: lab3.analysis.spatial

   .. contents::
      :local:


Submodules
==========

.. toctree::

   lab3.analysis.spatial.place_fields
   lab3.analysis.spatial.spatial_tuning

.. currentmodule:: lab3.analysis.spatial
