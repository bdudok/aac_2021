===================
``lab3.extraction``
===================

.. automodule:: lab3.extraction

   .. contents::
      :local:


Submodules
==========

.. toctree::

   lab3.extraction.s2p
   lab3.extraction.s2p_helpers

.. currentmodule:: lab3.extraction
