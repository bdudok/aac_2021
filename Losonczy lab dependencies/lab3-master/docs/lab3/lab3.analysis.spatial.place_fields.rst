======================================
``lab3.analysis.spatial.place_fields``
======================================

.. automodule:: lab3.analysis.spatial.place_fields

   .. contents::
      :local:

.. currentmodule:: lab3.analysis.spatial.place_fields
