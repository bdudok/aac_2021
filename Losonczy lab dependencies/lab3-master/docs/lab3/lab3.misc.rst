=============
``lab3.misc``
=============

.. automodule:: lab3.misc

   .. contents::
      :local:


Submodules
==========

.. toctree::

   lab3.misc.progressbar
   lab3.misc.scratch_work

.. currentmodule:: lab3.misc
