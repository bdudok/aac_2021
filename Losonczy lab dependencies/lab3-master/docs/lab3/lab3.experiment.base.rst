========================
``lab3.experiment.base``
========================

.. inheritance_diagram:: lab3.experiment.base


.. automodule:: lab3.experiment.base

   .. contents::
      :local:

.. currentmodule:: lab3.experiment.base
