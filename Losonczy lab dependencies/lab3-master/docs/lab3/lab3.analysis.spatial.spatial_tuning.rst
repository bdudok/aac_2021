========================================
``lab3.analysis.spatial.spatial_tuning``
========================================

.. automodule:: lab3.analysis.spatial.spatial_tuning

   .. contents::
      :local:

.. currentmodule:: lab3.analysis.spatial.spatial_tuning
