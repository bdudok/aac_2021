========
``lab3``
========

.. automodule:: lab3

   .. contents::
      :local:


Submodules
==========

.. toctree::

   lab3.analysis
   lab3.experiment
   lab3.extraction
   lab3.misc
   lab3.signal

.. currentmodule:: lab3
