=================
``lab3.analysis``
=================

.. automodule:: lab3.analysis

   .. contents::
      :local:


Submodules
==========

.. toctree::

   lab3.analysis.base
   lab3.analysis.spatial

.. currentmodule:: lab3.analysis
