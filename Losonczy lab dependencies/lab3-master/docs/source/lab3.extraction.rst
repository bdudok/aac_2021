lab3.extraction package
=======================

Submodules
----------

lab3.extraction.s2p module
--------------------------

.. automodule:: lab3.extraction.s2p
   :members:
   :undoc-members:
   :show-inheritance:

lab3.extraction.s2p\_helpers module
-----------------------------------

.. automodule:: lab3.extraction.s2p_helpers
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: lab3.extraction
   :members:
   :undoc-members:
   :show-inheritance:
