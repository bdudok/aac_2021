lab3.signal package
===================

Submodules
----------

lab3.signal.base module
-----------------------

.. automodule:: lab3.signal.base
   :members:
   :undoc-members:
   :show-inheritance:

lab3.signal.decomposition module
--------------------------------

.. automodule:: lab3.signal.decomposition
   :members:
   :undoc-members:
   :show-inheritance:

lab3.signal.dfof module
-----------------------

.. automodule:: lab3.signal.dfof
   :members:
   :undoc-members:
   :show-inheritance:

lab3.signal.filters module
--------------------------

.. automodule:: lab3.signal.filters
   :members:
   :undoc-members:
   :show-inheritance:

lab3.signal.spikes module
-------------------------

.. automodule:: lab3.signal.spikes
   :members:
   :undoc-members:
   :show-inheritance:

lab3.signal.transients module
-----------------------------

.. automodule:: lab3.signal.transients
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: lab3.signal
   :members:
   :undoc-members:
   :show-inheritance:
