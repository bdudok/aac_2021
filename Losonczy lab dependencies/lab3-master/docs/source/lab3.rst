lab3 package
============

Subpackages
-----------

.. toctree::

   lab3.analysis
   lab3.experiment
   lab3.extraction
   lab3.misc
   lab3.signal

Module contents
---------------

.. automodule:: lab3
   :members:
   :undoc-members:
   :show-inheritance:
