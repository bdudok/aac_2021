This repo contains analysis scripts used to produce the figures in our study entitled "Recruitment and inhibitory action of hippocampal axo-axonic cells during behavior", 2021. 
Scripts are organized by figures, and custom dependencies are included in separate folders, organized by contributing labs.
The contributor of each analysis is listed in the __contributor file in each folder, please contact the contributor with any questions.

The repo can be cloned from: https://code.stanford.edu/bdudok/aac_2021
